# add shebangs for bash and zsh
#! /usr/bin/bash

for f in /home/trident/spathe/mtt/mtt-student/frontend/*; do
    cd $f
    echo "Removing Git Repo stuff in $f ..."    
    rm -rf .git*
done