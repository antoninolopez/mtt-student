context('Navigate To Input Page', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8000');
  });

  it('Should Render Navigation Links', () => {
    const navLinks = cy.get('.nav-link');

    navLinks.should('have.length', 2);
  });

  it('Should Navigate To Form Page when Form Link is clicked', () => {
    const navLinks = cy.get('.nav-link');

    const formLink = navLinks.eq(1);

    formLink.click();

    cy.url().should('include', '/form');
  });
});

context('Check Input Status', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8000/form');
  });

  it('Should Render Red Outline By Default', () => {
    const inputBox = cy.get('input[type="text"]');

    inputBox.should('have.class', 'is-danger');
  });

  it('Should render green outline after typing 5 chars', () => {
    const inputBox = cy.get('input[type="text"]');

    inputBox.type('hello');

    inputBox.should('have.class', 'is-success');
  });

  it('Should return to red Outline if invalidated', () => {
    const inputBox = cy.get('input[type="text"]');

    inputBox.type('hello');

    inputBox.type('{backspace}{backspace}{backspace}');

    inputBox.should('have.class', 'is-danger');
  });
});
