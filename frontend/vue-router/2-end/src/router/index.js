import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/Home.vue";
import About from "../views/About.vue";
import FirstRoute from "../views/FirstRoute.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/about",
    name: "About",
    component: About,
  },
  {
    path: "/first-route",
    name: "FirstRoute",
    component: FirstRoute,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
