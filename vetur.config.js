// vetur.config.js
/** @type {import('vls').VeturConfig} */
module.exports = {
    // **optional** default: `{}`
    // override vscode settings
    // Notice: It only affects the settings used by Vetur.
    settings: {
      "vetur.useWorkspaceDependencies": true,
      "vetur.experimental.templateInterpolationService": true
    },
    // **optional** default: `[{ root: './' }]`
    // support monorepos
    projects: [
      './mtt-student/frontend/component-communications/1-start',
      './mtt-student/frontend/component-communications/2-end',
      './mtt-student/frontend/cypress-testing/1-start',
      './mtt-student/frontend/cypress-testing/2-end',
      './mtt-student/frontend/intro-to-vue/1-start',
      './mtt-student/frontend/intro-to-vue/2-end',
      './mtt-student/frontend/vue-router/1-start',
      './mtt-student/frontend/vue-router/2-end',
    ]
  }