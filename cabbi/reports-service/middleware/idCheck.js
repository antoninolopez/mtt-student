function _isIDInt(str) {
  if (typeof str !== 'string') return false; // we only process strings!

  if (str.includes('.')) return false;

  return !Number.isNaN(str) && !Number.isNaN(parseInt(str, 10));
}

module.exports = function isIDValid(req, res, next) {
  if (!_isIDInt(req.params.id)) {
    res.status(400).send('ID Needs to be an integer');
  }
  next();
};
