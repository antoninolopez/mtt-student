function checkRoles(keycloak, roles) {
  if (process.env.NODE_ENV !== 'development') {
    if (roles === null) {
      return keycloak.protect();
    }
    return keycloak.protect(roles);
  }

  return (_req, _res, next) => {
    next();
  };
}

module.exports = {
  checkRoles,
};
