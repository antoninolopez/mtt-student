const axios = require('axios');

module.exports = class ReportController {
  constructor() {
    this.irServiceURL = `http://${process.env.INCIDENT_REPORT_HOST}:${process.env.INCIDENT_REPORT_PORT}/ir`;
    this.rideServiceURL = `http://${process.env.RIDE_SERVICE_HOST}:${process.env.RIDE_SERVICE_PORT}/ride`;
  }

  getVehicleReport(headers, targetVehicleID) {
    return new Promise((resolve, reject) => {
      const irURL = `${this.irServiceURL}?vehicle_id=${targetVehicleID}`;
      const rideURL = `${this.rideServiceURL}/reportByVehicleID/${targetVehicleID}`;

      const reportData = [
        axios.get(irURL, { headers: { ...headers } }),
        axios.get(rideURL, { headers: { ...headers } }),
      ];

      // Get Incident Reports for vehicle
      Promise.all(reportData)
        .then((result) => {
          resolve({
            incidentReports: result[0].data,
            rides: result[1].data,
          });
        })
        .catch((err) => {
          reject(err);
        });
    });
  }
};
