const express = require('express');

const isIDValid = require('../middleware/idCheck');
const ReportController = require('../controllers/ReportController.js');
const { checkRoles } = require('../middleware/authCheck');

const router = express.Router();

const reportController = new ReportController();

module.exports = (keycloak) => {
  router.use(keycloak.middleware());

  router.get(
    '/vehicle/:id',
    checkRoles(keycloak, null),
    isIDValid,
    (req, res) => {
      const targetVehicle = req.params.id;

      const forwardHeaders = req.headers;

      reportController
        .getVehicleReport(forwardHeaders, targetVehicle)
        .then((result) => {
          res.send(result);
        })
        .catch((err) => {
          res.send(err);
        });
    },
  );

  return router;
};
