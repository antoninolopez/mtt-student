const axios = require('axios');
const qs = require('qs');
const { exit } = require('process');
const { logError, logSuccess, logInfo } = require('./helpers/logging');
const { runCommand } = require('./command-runner');
const uploadPostgresData = require('./potgresDataUpload');

const args = process.argv;
const command = args[2];

run(command);

async function run(command) {
  switch (command) {
    case 'up':
      try {
        await createDockerKeycloak();

        await waitForKeycloak();

        logInfo('Keycloak Ready');

        const dockerClusterStatus = await createDockerCluster();

        logInfo('Getting Admin Access Token');

        const accessToken = await getAdminAccessToken();

        logInfo('Adding Keycloak Users');

        await addAllUsers(accessToken);

        await uploadPostgresData({
          user: 'postgres',
          password: 'password',
          host: 'localhost',
          port: 5433,
          database: 'CABBI',
        });

        exitProgram(0);
      } catch (err) {
        console.error(err);
        exitProgram(1);
      }

      break;

    case 'down':
      try {
        await destroyDockerCluster();

        exitProgram(0);
      } catch (err) {
        exitProgram(1);
      }

      break;

    default:
      logError(`Unknown command: ${command}.  Values are ${up} or ${down}`);
      break;
  }
}

async function waitForKeycloak() {
  let keycloakStarting = true;

  while (keycloakStarting) {
    const isKeycloakUp = await tryConnectingToKeycloak();

    if (isKeycloakUp) {
      keycloakStarting = false;
      return;
    }
    console.log('Waiting for Keycloak to start');

    await sleep(2500);
  }
}

async function tryConnectingToKeycloak() {
  try {
    const result = await axios.get('http://localhost:8080/auth', {
      timeout: 250,
    });
    return result.data;
  } catch (err) {
    return false;
  }
}

async function createDockerKeycloak() {
  const commandToRun = 'docker-compose';
  const args = ['up', '--build', '-d', 'keycloak'];

  try {
    await runCommand(commandToRun, args);
    return;
  } catch (err) {
    exitProgram(err);
  }
}

async function createDockerCluster() {
  const commandToRun = 'docker-compose';
  const args = ['up', '--build', '-d'];

  try {
    await runCommand(commandToRun, args);
    return;
  } catch (err) {
    exitProgram(err);
  }
}

async function destroyDockerCluster() {
  const commandToRun = 'docker-compose';
  const args = ['down'];

  try {
    await runCommand(commandToRun, args);
    return;
  } catch (err) {
    exitProgram(err);
  }
}

function exitProgram(code) {
  if (code > 0) {
    logError(`Process Exited with errors`);
    exit(code);
  } else {
    logSuccess('Process Exited with no errors');
    exit(code);
  }
}

async function getAdminAccessToken() {
  const data = qs.stringify({
    username: 'admin',
    password: 'admin',
    grant_type: 'password',
    client_id: 'admin-cli',
  });
  const config = {
    method: 'post',
    url:
      'http://localhost:8080/auth/realms/master/protocol/openid-connect/token',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    data,
  };

  try {
    const response = await axios(config);
    return response.data.access_token;
  } catch (err) {
    console.log(err);
  }
}

async function addAllUsers(accessToken) {
  const user1 = JSON.stringify({
    firstName: 'Jane',
    lastName: 'Doe',
    username: 'jane',
    email: 'jane@cabbi.com',
    enabled: 'true',
    credentials: [
      {
        type: 'password',
        value: 'test123',
        temporary: false,
      },
    ],
    realmRoles: ['admin'],
  });

  const user2 = JSON.stringify({
    firstName: 'John',
    lastName: 'Doe',
    username: 'jdoe',
    email: 'jdoe@cabbi.com',
    enabled: 'true',
    credentials: [
      {
        type: 'password',
        value: 'test123',
        temporary: false,
      },
    ],
    realmRoles: ['admin'],
  });

  await addUser(user1, accessToken);
  await addUser(user2, accessToken);
}

async function addUser(data, token) {
  const config = {
    method: 'post',
    url: 'http://localhost:8080/auth/admin/realms/CABBI/users',
    headers: {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'application/json',
    },
    data,
  };

  try {
    await axios(config);
    return true;
  } catch (err) {
    exitProgram(1);
  }
}

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}
