const Promise = require('bluebird');
const fs = require('fs');
const path = require('path');
const { Pool } = require('pg');
const { logInfo, logSuccess, logWarning } = require('./helpers/logging');

let filePrefix = 'CABBI-';
let fileSuffix = '-table_data_current';
let fileSuffixSchema = '_current';

module.exports = async function uploadPostgresData(connectionData) {
  logInfo('Uploading Postgres Data');
  const client = await connectToDB(connectionData);

  await createTables(client);

  const data = await uploadAllTableData(client);

  logSuccess('updated tables');

  let resultMap = getResultCount(data);

  logSuccess('Records updated:');
  data.tableNames.forEach((name) => {
    logSuccess(`\t${name} -> ${resultMap.get(name)} records added`);
  });
  logSuccess('DATA Deployed Successfully');
};

function getResultCount(data) {
  let tableNames = data.tableNames;
  let results = data.result;

  let resultMap = new Map();

  tableNames.forEach((name, index) => {
    let numResults = 0;

    let targetResult = results[index];

    if (Array.isArray(targetResult)) {
      targetResult.forEach((element) => {
        numResults += element.rowCount;
      });
    } else {
      numResults = targetResult.rowCount;
    }

    resultMap.set(name, numResults);
  });

  return resultMap;
}

async function connectToDB(connectionData) {
  const client = new Pool({
    user: connectionData.user,
    host: connectionData.host,
    password: connectionData.password,
    database: connectionData.database,
    port: connectionData.port,
  });

  return client;
}

async function createTables(client) {
  await dropTables(client);

  await dropSequences(client);

  logInfo('Creating Tables');

  const result = await createTablesFromSQL(client);

  return result;
}

function readSQLFile(fileName) {
  let filePath = path.join(__dirname, 'data', fileName);

  return fs.readFileSync(filePath).toString();
}

async function getTableList(client) {
  const response = await client.query(
    "SELECT * FROM pg_catalog.pg_tables WHERE schemaname != 'pg_catalog' AND schemaname != 'information_schema';"
  );
  return response.rows.map((tableObj) => tableObj['tablename']);
}

async function dropTables(client) {
  let tableDrops = [];

  const tableNameList = await getTableList(client);

  logInfo(`Found ${tableNameList.length} Tables`);

  tableNameList.forEach((tableName) => {
    tableDrops.push(dropSingleTable(client, tableName));
  });

  await Promise.all(tableDrops);

  return;
}

function dropSequences(client) {
  return new Promise((resolve, reject) => {
    client
      .query(`SELECT c.relname FROM pg_class c WHERE c.relkind = 'S';`)
      .then((seqRes) => {
        let seqDrop = [];
        seqRes.rows.forEach((row) => {
          let sequenceDropQuery = 'DROP SEQUENCE "' + row.relname + '";';

          logWarning(`Dropping Sequence ${row.relname}`);

          seqDrop.push(client.query(sequenceDropQuery));
        });

        Promise.all(seqDrop)
          .then(() => {
            resolve();
          })
          .catch((dropError) => reject(dropError));
      })
      .catch((seqErr) => reject(seqErr));
  });
}

function dropSingleTable(client, tableName) {
  return new Promise((resolve, reject) => {
    logWarning(`Dropping Table ${tableName}`);
    client
      .query(`DROP TABLE IF EXISTS public."${tableName}" CASCADE;`)
      .then((res) => resolve())
      .catch((err) => reject(err));
  });
}

async function createTablesFromSQL(client) {
  const tableSQL = readSQLFile(
    `${filePrefix}tables-schema${fileSuffixSchema}.sql`
  );

  const res = await client.query(tableSQL);

  return res;
}

async function uploadAllTableData(client) {
  const tableNames = await getTableList(client);
  let dataUploads = [];
  let result = [];

  for (const tableName of tableNames) {
    logInfo(`Executing ${filePrefix}${tableName}${fileSuffix}.sql`);

    let query = readSQLFile(`${filePrefix}${tableName}${fileSuffix}.sql`);

    const uploadData = await client.query(query);

    result.push(uploadData);
  }

  return {
    tableNames,
    result,
  };
}
