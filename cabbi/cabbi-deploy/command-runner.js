const { spawn } = require('child_process');
const { logInfo } = require('./helpers/logging');

module.exports = {
  runCommand(commandToRun, args) {
    return new Promise((resolve, reject) => {
      logInfo(`Running Command: ${commandToRun} ${args.join(' ')}`);

      const commandInProcess = spawn(commandToRun, args);
      commandInProcess.stdout.on('data', (data) => {
        logInfo(`${data}`);
      });

      // Docker Compose writes build logs to stderr which is super cringe-worthy
      commandInProcess.stderr.on('data', (data) => {
        console.log(`Docker Compose Output: ${data}`);
      });

      commandInProcess.on('close', (code) => {
        if (code > 0) {
          reject(code);
        } else {
          resolve(code);
        }
      });
    });
  },
};
