const uploadPostgresData = require('./potgresDataUpload');

uploadPostgresData({
  user: 'postgres',
  password: 'password',
  host: 'localhost',
  port: 5433,
  database: 'CABBI',
})
  .then(() => {
    process.exit(0);
  })
  .catch((err) => {
    console.log(err);
    process.exit(1);
  });
