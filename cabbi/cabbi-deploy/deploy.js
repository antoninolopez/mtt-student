const fs = require('fs');
const path = require('path');
const { exec, spawn } = require('child_process');
const attemptForwardToPod = require('./portForward');
const uploadPostgresData = require('./potgresDataUpload');
const { logInfo, logError, logSuccess } = require('./helpers/logging');
const initKeycloak = require('./keycloak/init-keycloak');

const deploySecrets = require('./secrets');
const deployApp = require('./deployApp');

const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

const args = process.argv;

if (args.includes('--only-secrets') || args.includes('--os')) {
  logInfo('Running Secret Generation...');
  deploySecrets()
    .then((res) => closeTool(0))
    .catch((err) => {
      console.log(err);
      closeTool(1);
    });
}

if (args.includes('--only-data') || args.includes('--od')) {
  runDataDeploy()
    .then((success) => closeTool(0))
    .catch((err) => closeTool(1));
}

if (
  !(
    args.includes('--only-data') ||
    args.includes('--od') ||
    args.includes('--only-secrets') ||
    args.includes('--os')
  )
) {
  getProjectData();
}

// Gets project Data from OC
function getProjectData() {
  exec('oc project', (error, stdout, stderr) => {
    if (error) {
      process.exit(1);
    }

    if (stderr) {
      process.exit(1);
    }

    confirmProject(stdout);
  });
}

// Starts the catch loop for prompting the user
function confirmProject(stringPrompt) {
  logInfo(stringPrompt);
  promptUserForContinue('Do you want to proceed with the deploy? (yes|no) ');
}

// Prompts user to continue until they provide 'yes', 'no', or 'n'
function promptUserForContinue(prompt) {
  rl.question(prompt, function (answer) {
    if (answer.toLowerCase() === 'yes') {
      if (args.includes('--include-secrets') || args.includes('--is')) {
        deploySecrets()
          .then((success) => {
            applicationDeploy();
          })
          .catch((err) => closeTool(1));
      } else {
        applicationDeploy();
      }
      rl.close();
      return;
    }
    if (answer.toLowerCase() === 'no' || answer.toLowerCase() === 'n') {
      rl.close;
      closeTool(1);
    }

    promptUserForContinue(
      "That option is not valid, please answer with 'yes' or 'no'"
    );
  });
}

function applicationDeploy() {
  deployApp(args)
    .then((data) => {
      checkDataDeploy()
        .then(() => {
          closeTool(0);
        })
        .catch((err) => closeTool(1));
    })
    .catch((err) => closeTool(1));
}

function checkDataDeploy() {
  return new Promise((resolve, reject) => {
    if (args.includes('--include-data') || args.includes('--id')) {
      runDataDeploy()
        .then(() => {
          resolve();
        })
        .catch((err) => {
          reject(err);
        });
    } else {
      resolve();
    }
  });
}

function runDataDeploy() {
  return new Promise((resolve, reject) => {
    attemptForwardToPod('postgres-app', 6500, 5432)
      .then(() => {
        uploadPostgresData({
          user: 'postgres',
          password: 'password',
          host: 'localhost',
          port: 6500,
          database: 'CABBI',
        })
          .then(() => {
            resolve();
          })
          .catch((err) => {
            reject(err);
          });
      })
      .catch((err) => {
        reject(err);
      });
  });
}

function initializeKeycloak() {
  logInfo('Initializing Keycloak');
  initKeycloak()
    .then(() => {
      logSuccess('Initialized Keycloak');
      resolve();
    })
    .catch((err) => {
      reject(err);
    });
}

// Closes with exit code
function closeTool(code) {
  if (code === 0) {
    logSuccess('Closing Deploy Tool Without Errors');
  } else {
    logError('Closing Deploy Tool WITH Errors');
  }

  process.exit(code);
}
