const { exec, spawn } = require('child_process');
const {
  logInfo,
  logError,
  logSuccess,
  logWarning,
} = require('./helpers/logging');

let numPortForwardAttempts = 0;
let numWatchAttempts = 0;

module.exports = function attemptForwardToPod(podQuery, localPort, remotePort) {
  return new Promise((resolve, reject) => {
    getPods()
      .then((output) => {
        let numFound = 0;
        const splitOutput = output.split('\n');

        splitOutput.forEach((element) => {
          if (element.includes(podQuery)) {
            const podName = element.split(' ')[0];
            watchPodForReadyStatus(podName)
              .then((success) => {
                startPortForward(podName, localPort, remotePort)
                  .then((success) => resolve())
                  .catch((err) => reject(err));
              })
              .catch((err) => reject(err));

            numFound++;
          }
        });

        if (numFound === 0) {
          attemptForwardToPod(podQuery, localPort, remotePort);
        }
      })
      .catch((err) => {
        console.error(err);
      });
  });
};

function getPods() {
  return new Promise((resolve, reject) => {
    exec('oc get pods', (error, stdout, stderr) => {
      if (error) {
        reject(error);
      }
      if (stderr) {
        reject(stderr);
      }

      resolve(stdout);
    });
  });
}

function watchPodForReadyStatus(podName) {
  return new Promise((resolve, reject) => {
    if (numWatchAttempts > 100) {
      logError('Unable to Start Pod');
      process.exit(1);
    } else {
      setTimeout(() => {
        isPodReady(podName)
          .then((success) => {
            logSuccess('Pod Running');
            resolve();
          })
          .catch((err) => {
            logInfo('Pod Not Found Running');
            watchPodForReadyStatus(podName)
              .then((success) => resolve())
              .catch((err) => reject(err));
          });
        numWatchAttempts++;
      }, 2000);
    }
  });
}

function isPodReady(podName) {
  return new Promise((resolve, reject) => {
    getPods()
      .then((podList) => {
        const splitPods = podList.split('\n');

        splitPods.forEach((podRow) => {
          let splitRow = podRow.split(' ');
          if (splitRow.includes(podName) && splitRow.includes('Running')) {
            resolve();
          }
        });

        reject();
      })
      .catch((err) => reject(err));
  });
}

function startPortForward(podName, localPort, remotePort) {
  return new Promise((resolve, reject) => {
    if (numPortForwardAttempts > 100) {
      logError('Unable to Forward Port');
      process.exit(1);
    } else {
      setTimeout(() => {
        attemptPortForward(podName, localPort, remotePort)
          .then((success) => {
            resolve();
          })
          .catch((err) => {
            startPortForward(podName, localPort, remotePort)
              .then((success) => resolve())
              .catch((err) => reject(err));
          });
        numPortForwardAttempts++;
      }, 2000);
    }
  });
}

function attemptPortForward(podName, localPort, remotePort) {
  return new Promise((resolve, reject) => {
    logInfo(
      `Attempting to forward to pod ${podName} ${localPort}:${remotePort}`
    );
    const portForward = spawn('oc', [
      'port-forward',
      podName,
      `${localPort}:${remotePort}`,
    ]);
    portForward.stdout.on('data', (data) => {
      logInfo(`PORT_FORWARD: ${data}`);

      if (data.toString().includes(`Forwarding from [::1]`)) {
        logSuccess('Port Forward Succeeded');
        resolve();
      }
    });
    portForward.stderr.on('data', (data) => {
      logError(`PORT_FORWARD_ERR: ${data}`);
      reject(data);
    });
    portForward.on('close', (code) => {});
  });
}
