const fs = require('fs');
const { exec } = require('child_process');
const { resolve, reject } = require('bluebird');
const axios = require('axios');
const qs = require('querystring');
const path = require('path');

module.exports = function initializeKeycloak() {
  return new Promise((resolve, reject) => {
    getRoutes()
      .then((routes) => {
        let routeURL = findKeycloakRouteURL(routes);

        getAccessToken(routeURL)
          .then((accessToken) => {
            generateRealm(routeURL, accessToken)
              .then((success) => {
                createUsers(routeURL, accessToken)
                  .then((success) => resolve())
                  .catch((err) => reject(err));
              })
              .catch((err) => reject(err));
          })
          .catch((err) => reject(err));
      })
      .catch((err) => reject(err));
  });
};

function getRoutes() {
  return new Promise((resolve, reject) => {
    exec('oc get routes', (error, stdout, stderr) => {
      if (error) {
        reject(error);
      }
      if (stderr) {
        reject(stderr);
      }

      resolve(stdout);
    });
  });
}

function findKeycloakRouteURL(routes) {
  let splitRoutes = routes.split('\n');

  let keycloakRoute = '';

  splitRoutes.forEach((route) => {
    if (route.includes('keycloak')) {
      keycloakRoute = route;
    }
  });

  let rowArr = keycloakRoute.split(' ').filter((item) => item !== '');

  return rowArr[1];
}

function getAccessToken(baseUrl) {
  return new Promise((resolve, reject) => {
    const headers = { 'Content-Type': 'application/x-www-form-urlencoded' };

    const body = {
      username: 'admin',
      password: 'admin',
      grant_type: 'password',
      client_id: 'admin-cli',
    };

    axios
      .post(
        `http://${baseUrl}/auth/realms/master/protocol/openid-connect/token`,
        qs.stringify(body),
        {
          headers,
        }
      )
      .then((res) => {
        resolve(res.data.access_token);
      })
      .catch((err) => reject(err));
  });
}

function generateRealm(routeURL, accessToken) {
  return new Promise((resolve, reject) => {
    let realmFileData = readJsonFile('realm-export.json');

    const headers = {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${accessToken}`,
    };

    axios
      .post(`http://${routeURL}/auth/admin/realms`, realmFileData, { headers })
      .then((res) => {
        resolve();
      })
      .catch((err) => reject(err));
  });
}

function readJsonFile(fileName) {
  let realmFilePath = path.join(process.cwd(), 'keycloak', fileName);

  let realmFile = fs.readFileSync(realmFilePath, { encoding: 'utf-8' });

  return realmFile;
}

function createUsers(routeURL, accessToken) {
  return new Promise((resolve, reject) => {
    let realmFileData = JSON.parse(readJsonFile('users.json'));

    const headers = {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${accessToken}`,
    };

    let userCreations = [];

    realmFileData.users.forEach((user) => {
      userCreations.push(
        axios.post(
          `http://${routeURL}/auth/admin/realms/CABBI/users`,
          JSON.stringify(user),
          { headers }
        )
      );
    });

    Promise.all(userCreations)
      .then((success) => {
        resolve();
      })
      .catch((err) => reject(err));
  });
}
