const path = require('path');
const fs = require('fs');
const { exec, execSync } = require('child_process');
const { logInfo, logError, logSuccess } = require('./helpers/logging');
const { stdout } = require('process');

const envFile = path.join(process.cwd(), '.env.json');

const safePrefixes = ['builder', 'default', 'deployer'];

module.exports = function deploySecrets() {
  return new Promise((resolve, reject) => {
    deleteSecrets().then(() => {
      createSecrets()
        .then(() => {
          logSuccess('Completed Generating ENV Secrets');
          runSecretGenerationCommand('oc create -f secrets')
            .then(() => {
              logSuccess('Completed Generating TEMPLATE Secrets');
              resolve();
            })
            .catch((err) => reject(err));
        })
        .catch((err) => {
          logError(err);
          reject();
        });
    });
  });
};

function deleteSecrets() {
  return new Promise((resolve, reject) => {
    getSecrets()
      .then((secretList) => {
        let secretDeletePromiseList = [];

        secretList.forEach((secretName) => {
          secretDeletePromiseList.push(deleteSingleSecret(secretName));
        });

        Promise.all(secretDeletePromiseList)
          .then(() => {
            logSuccess(`${secretDeletePromiseList.length} Secret(s) Deleted`);
            resolve();
          })
          .catch((err) => reject(err));
      })
      .catch((err) => logError(err));
  });
}

function getSecrets() {
  return new Promise((resolve, reject) => {
    console.log('running oc get secrets');
    exec('oc get secrets', (error, stdout, stderr) => {
      if (error) {
        reject(error);
      }

      if (stderr) {
        reject(stderr);
      }

      resolve(parseSecrets(stdout));
    });
  });
}

function deleteSingleSecret(secretToDelete) {
  return new Promise((resolve, reject) => {
    exec(`oc delete secret ${secretToDelete}`, (error, stdout, stderr) => {
      if (error) {
        reject(error);
      }
      if (stderr) {
        reject(stderr);
      }

      logInfo(stdout);
      resolve();
    });
  });
}

function parseSecrets(secrets) {
  // Split and remove the header
  let splitSecrets = secrets.split('\n');
  splitSecrets.splice(0, 1);

  let secretsToDelete = [];

  splitSecrets.forEach((el) => {
    let secretName = el.split(' ')[0];
    if (safePrefixes.some((pre) => secretName.indexOf(pre) >= 0)) {
      logInfo(`System secret ${secretName} ignored`);
    } else {
      secretsToDelete.push(secretName);
    }
  });

  return secretsToDelete.filter((secret) => secret !== '');
}

function runSecretGenerationCommand(command) {
  return new Promise((resolve, reject) => {
    exec(command, (error, stdout, stderr) => {
      if (error) {
        reject(error);
      }
      if (stderr) {
        reject(stderr);
      }

      resolve();
    });
  });
}

// Creates the secrets
function createSecrets() {
  const fileData = fs.readFileSync(envFile, {
    encoding: 'utf-8',
  });

  const jsonData = JSON.parse(fileData);

  const envs = jsonData.envs;

  let secretStatus = [];

  envs.forEach((envEl) => {
    let createCommand = '';

    switch (envEl.type) {
      case 'Key/Value':
        createCommand += `oc create secret generic ${envEl.name} `;

        envEl.entries.forEach((credEl) => {
          createCommand += `--from-literal=${credEl} `;
        });

        break;

      case 'Image_Pull_Secret':
        createCommand += `oc create secret docker-registry ${envEl.name} --docker-server=${envEl.data.registryName} --docker-username=${envEl.data.username} --docker-password=${envEl.data.password} --docker-email=${envEl.data.email}`;
        break;

      default:
        logError(`Can\'t Process secret type: ${envEl.type}`);
        break;
    }

    secretStatus.push(runSecretGenerationCommand(createCommand));
  });

  return Promise.all(secretStatus);
}
