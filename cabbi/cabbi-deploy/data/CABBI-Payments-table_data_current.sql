--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5
-- Dumped by pg_dump version 12.5

-- Started on 2021-01-13 11:40:27 EST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2980 (class 0 OID 90360)
-- Dependencies: 206
-- Data for Name: Payments; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Payments" VALUES (1, 'credit_card         ', 'Credit card                   ');
INSERT INTO public."Payments" VALUES (2, 'cash                ', 'Cash                          ');
INSERT INTO public."Payments" VALUES (3, 'no_charge           ', 'No charge                     ');
INSERT INTO public."Payments" VALUES (4, 'dispute             ', 'Dispute                       ');
INSERT INTO public."Payments" VALUES (5, 'unknown             ', 'Unknown                       ');
INSERT INTO public."Payments" VALUES (6, 'voided_trip         ', 'Voided trip                   ');


--
-- TOC entry 2987 (class 0 OID 0)
-- Dependencies: 207
-- Name: Payments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Payments_id_seq"', 7, true);


-- Completed on 2021-01-13 11:40:27 EST

--
-- PostgreSQL database dump complete
--

