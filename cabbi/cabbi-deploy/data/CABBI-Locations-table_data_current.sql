--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5
-- Dumped by pg_dump version 12.5

-- Started on 2021-01-13 11:39:58 EST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2980 (class 0 OID 90355)
-- Dependencies: 204
-- Data for Name: Locations; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Locations" VALUES (1, 'EWR                                               ', 'Newark Airport                                    ', 'EWR                                               ');
INSERT INTO public."Locations" VALUES (2, 'Queens                                            ', 'Jamaica Bay                                       ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (3, 'Bronx                                             ', 'Allerton/Pelham Gardens                           ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (4, 'Manhattan                                         ', 'Alphabet City                                     ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (5, 'Staten Island                                     ', 'Arden Heights                                     ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (6, 'Staten Island                                     ', 'Arrochar/Fort Wadsworth                           ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (7, 'Queens                                            ', 'Astoria                                           ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (8, 'Queens                                            ', 'Astoria Park                                      ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (9, 'Queens                                            ', 'Auburndale                                        ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (10, 'Queens                                            ', 'Baisley Park                                      ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (11, 'Brooklyn                                          ', 'Bath Beach                                        ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (12, 'Manhattan                                         ', 'Battery Park                                      ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (13, 'Manhattan                                         ', 'Battery Park City                                 ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (14, 'Brooklyn                                          ', 'Bay Ridge                                         ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (15, 'Queens                                            ', 'Bay Terrace/Fort Totten                           ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (16, 'Queens                                            ', 'Bayside                                           ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (17, 'Brooklyn                                          ', 'Bedford                                           ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (18, 'Bronx                                             ', 'Bedford Park                                      ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (19, 'Queens                                            ', 'Bellerose                                         ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (20, 'Bronx                                             ', 'Belmont                                           ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (21, 'Brooklyn                                          ', 'Bensonhurst East                                  ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (22, 'Brooklyn                                          ', 'Bensonhurst West                                  ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (23, 'Staten Island                                     ', 'Bloomfield/Emerson Hill                           ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (24, 'Manhattan                                         ', 'Bloomingdale                                      ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (25, 'Brooklyn                                          ', 'Boerum Hill                                       ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (26, 'Brooklyn                                          ', 'Borough Park                                      ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (27, 'Queens                                            ', 'Breezy Point/Fort Tilden/Riis Beach               ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (28, 'Queens                                            ', 'Briarwood/Jamaica Hills                           ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (29, 'Brooklyn                                          ', 'Brighton Beach                                    ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (30, 'Queens                                            ', 'Broad Channel                                     ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (31, 'Bronx                                             ', 'Bronx Park                                        ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (32, 'Bronx                                             ', 'Bronxdale                                         ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (33, 'Brooklyn                                          ', 'Brooklyn Heights                                  ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (34, 'Brooklyn                                          ', 'Brooklyn Navy Yard                                ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (35, 'Brooklyn                                          ', 'Brownsville                                       ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (36, 'Brooklyn                                          ', 'Bushwick North                                    ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (37, 'Brooklyn                                          ', 'Bushwick South                                    ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (38, 'Queens                                            ', 'Cambria Heights                                   ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (39, 'Brooklyn                                          ', 'Canarsie                                          ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (40, 'Brooklyn                                          ', 'Carroll Gardens                                   ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (41, 'Manhattan                                         ', 'Central Harlem                                    ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (42, 'Manhattan                                         ', 'Central Harlem North                              ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (43, 'Manhattan                                         ', 'Central Park                                      ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (44, 'Staten Island                                     ', 'Charleston/Tottenville                            ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (45, 'Manhattan                                         ', 'Chinatown                                         ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (46, 'Bronx                                             ', 'City Island                                       ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (47, 'Bronx                                             ', 'Claremont/Bathgate                                ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (48, 'Manhattan                                         ', 'Clinton East                                      ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (49, 'Brooklyn                                          ', 'Clinton Hill                                      ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (50, 'Manhattan                                         ', 'Clinton West                                      ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (51, 'Bronx                                             ', 'Co-Op City                                        ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (52, 'Brooklyn                                          ', 'Cobble Hill                                       ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (53, 'Queens                                            ', 'College Point                                     ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (54, 'Brooklyn                                          ', 'Columbia Street                                   ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (55, 'Brooklyn                                          ', 'Coney Island                                      ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (56, 'Queens                                            ', 'Corona                                            ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (57, 'Queens                                            ', 'Corona                                            ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (58, 'Bronx                                             ', 'Country Club                                      ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (59, 'Bronx                                             ', 'Crotona Park                                      ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (60, 'Bronx                                             ', 'Crotona Park East                                 ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (61, 'Brooklyn                                          ', 'Crown Heights North                               ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (62, 'Brooklyn                                          ', 'Crown Heights South                               ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (63, 'Brooklyn                                          ', 'Cypress Hills                                     ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (64, 'Queens                                            ', 'Douglaston                                        ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (65, 'Brooklyn                                          ', 'Downtown Brooklyn/MetroTech                       ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (66, 'Brooklyn                                          ', 'DUMBO/Vinegar Hill                                ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (67, 'Brooklyn                                          ', 'Dyker Heights                                     ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (68, 'Manhattan                                         ', 'East Chelsea                                      ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (69, 'Bronx                                             ', 'East Concourse/Concourse Village                  ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (70, 'Queens                                            ', 'East Elmhurst                                     ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (71, 'Brooklyn                                          ', 'East Flatbush/Farragut                            ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (72, 'Brooklyn                                          ', 'East Flatbush/Remsen Village                      ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (73, 'Queens                                            ', 'East Flushing                                     ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (74, 'Manhattan                                         ', 'East Harlem North                                 ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (75, 'Manhattan                                         ', 'East Harlem South                                 ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (76, 'Brooklyn                                          ', 'East New York                                     ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (77, 'Brooklyn                                          ', 'East New York/Pennsylvania Avenue                 ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (78, 'Bronx                                             ', 'East Tremont                                      ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (79, 'Manhattan                                         ', 'East Village                                      ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (80, 'Brooklyn                                          ', 'East Williamsburg                                 ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (81, 'Bronx                                             ', 'Eastchester                                       ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (82, 'Queens                                            ', 'Elmhurst                                          ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (83, 'Queens                                            ', 'Elmhurst/Maspeth                                  ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (84, 'Staten Island                                     ', 'Eltingville/Annadale/Prince''s Bay                 ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (85, 'Brooklyn                                          ', 'Erasmus                                           ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (86, 'Queens                                            ', 'Far Rockaway                                      ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (87, 'Manhattan                                         ', 'Financial District North                          ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (88, 'Manhattan                                         ', 'Financial District South                          ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (89, 'Brooklyn                                          ', 'Flatbush/Ditmas Park                              ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (90, 'Manhattan                                         ', 'Flatiron                                          ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (91, 'Brooklyn                                          ', 'Flatlands                                         ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (92, 'Queens                                            ', 'Flushing                                          ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (93, 'Queens                                            ', 'Flushing Meadows-Corona Park                      ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (94, 'Bronx                                             ', 'Fordham South                                     ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (95, 'Queens                                            ', 'Forest Hills                                      ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (96, 'Queens                                            ', 'Forest Park/Highland Park                         ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (97, 'Brooklyn                                          ', 'Fort Greene                                       ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (98, 'Queens                                            ', 'Fresh Meadows                                     ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (99, 'Staten Island                                     ', 'Freshkills Park                                   ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (100, 'Manhattan                                         ', 'Garment District                                  ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (101, 'Queens                                            ', 'Glen Oaks                                         ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (102, 'Queens                                            ', 'Glendale                                          ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (103, 'Manhattan                                         ', 'Governor''s Island/Ellis Island/Liberty Island     ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (104, 'Manhattan                                         ', 'Governor''s Island/Ellis Island/Liberty Island     ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (105, 'Manhattan                                         ', 'Governor''s Island/Ellis Island/Liberty Island     ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (106, 'Brooklyn                                          ', 'Gowanus                                           ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (107, 'Manhattan                                         ', 'Gramercy                                          ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (108, 'Brooklyn                                          ', 'Gravesend                                         ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (109, 'Staten Island                                     ', 'Great Kills                                       ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (110, 'Staten Island                                     ', 'Great Kills Park                                  ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (111, 'Brooklyn                                          ', 'Green-Wood Cemetery                               ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (112, 'Brooklyn                                          ', 'Greenpoint                                        ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (113, 'Manhattan                                         ', 'Greenwich Village North                           ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (114, 'Manhattan                                         ', 'Greenwich Village South                           ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (115, 'Staten Island                                     ', 'Grymes Hill/Clifton                               ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (116, 'Manhattan                                         ', 'Hamilton Heights                                  ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (117, 'Queens                                            ', 'Hammels/Arverne                                   ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (118, 'Staten Island                                     ', 'Heartland Village/Todt Hill                       ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (119, 'Bronx                                             ', 'Highbridge                                        ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (120, 'Manhattan                                         ', 'Highbridge Park                                   ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (121, 'Queens                                            ', 'Hillcrest/Pomonok                                 ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (122, 'Queens                                            ', 'Hollis                                            ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (123, 'Brooklyn                                          ', 'Homecrest                                         ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (124, 'Queens                                            ', 'Howard Beach                                      ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (125, 'Manhattan                                         ', 'Hudson Sq                                         ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (126, 'Bronx                                             ', 'Hunts Point                                       ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (127, 'Manhattan                                         ', 'Inwood                                            ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (128, 'Manhattan                                         ', 'Inwood Hill Park                                  ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (129, 'Queens                                            ', 'Jackson Heights                                   ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (130, 'Queens                                            ', 'Jamaica                                           ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (131, 'Queens                                            ', 'Jamaica Estates                                   ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (132, 'Queens                                            ', 'JFK Airport                                       ', 'Airports                                          ');
INSERT INTO public."Locations" VALUES (133, 'Brooklyn                                          ', 'Kensington                                        ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (134, 'Queens                                            ', 'Kew Gardens                                       ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (135, 'Queens                                            ', 'Kew Gardens Hills                                 ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (136, 'Bronx                                             ', 'Kingsbridge Heights                               ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (137, 'Manhattan                                         ', 'Kips Bay                                          ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (138, 'Queens                                            ', 'LaGuardia Airport                                 ', 'Airports                                          ');
INSERT INTO public."Locations" VALUES (139, 'Queens                                            ', 'Laurelton                                         ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (140, 'Manhattan                                         ', 'Lenox Hill East                                   ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (141, 'Manhattan                                         ', 'Lenox Hill West                                   ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (142, 'Manhattan                                         ', 'Lincoln Square East                               ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (143, 'Manhattan                                         ', 'Lincoln Square West                               ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (144, 'Manhattan                                         ', 'Little Italy/NoLiTa                               ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (145, 'Queens                                            ', 'Long Island City/Hunters Point                    ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (146, 'Queens                                            ', 'Long Island City/Queens Plaza                     ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (147, 'Bronx                                             ', 'Longwood                                          ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (148, 'Manhattan                                         ', 'Lower East Side                                   ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (149, 'Brooklyn                                          ', 'Madison                                           ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (150, 'Brooklyn                                          ', 'Manhattan Beach                                   ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (151, 'Manhattan                                         ', 'Manhattan Valley                                  ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (152, 'Manhattan                                         ', 'Manhattanville                                    ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (153, 'Manhattan                                         ', 'Marble Hill                                       ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (154, 'Brooklyn                                          ', 'Marine Park/Floyd Bennett Field                   ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (155, 'Brooklyn                                          ', 'Marine Park/Mill Basin                            ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (156, 'Staten Island                                     ', 'Mariners Harbor                                   ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (157, 'Queens                                            ', 'Maspeth                                           ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (158, 'Manhattan                                         ', 'Meatpacking/West Village West                     ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (159, 'Bronx                                             ', 'Melrose South                                     ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (160, 'Queens                                            ', 'Middle Village                                    ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (161, 'Manhattan                                         ', 'Midtown Center                                    ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (162, 'Manhattan                                         ', 'Midtown East                                      ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (163, 'Manhattan                                         ', 'Midtown North                                     ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (164, 'Manhattan                                         ', 'Midtown South                                     ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (165, 'Brooklyn                                          ', 'Midwood                                           ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (166, 'Manhattan                                         ', 'Morningside Heights                               ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (167, 'Bronx                                             ', 'Morrisania/Melrose                                ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (168, 'Bronx                                             ', 'Mott Haven/Port Morris                            ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (169, 'Bronx                                             ', 'Mount Hope                                        ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (170, 'Manhattan                                         ', 'Murray Hill                                       ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (171, 'Queens                                            ', 'Murray Hill-Queens                                ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (172, 'Staten Island                                     ', 'New Dorp/Midland Beach                            ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (173, 'Queens                                            ', 'North Corona                                      ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (174, 'Bronx                                             ', 'Norwood                                           ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (175, 'Queens                                            ', 'Oakland Gardens                                   ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (176, 'Staten Island                                     ', 'Oakwood                                           ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (177, 'Brooklyn                                          ', 'Ocean Hill                                        ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (178, 'Brooklyn                                          ', 'Ocean Parkway South                               ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (179, 'Queens                                            ', 'Old Astoria                                       ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (180, 'Queens                                            ', 'Ozone Park                                        ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (181, 'Brooklyn                                          ', 'Park Slope                                        ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (182, 'Bronx                                             ', 'Parkchester                                       ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (183, 'Bronx                                             ', 'Pelham Bay                                        ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (184, 'Bronx                                             ', 'Pelham Bay Park                                   ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (185, 'Bronx                                             ', 'Pelham Parkway                                    ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (186, 'Manhattan                                         ', 'Penn Station/Madison Sq West                      ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (187, 'Staten Island                                     ', 'Port Richmond                                     ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (188, 'Brooklyn                                          ', 'Prospect-Lefferts Gardens                         ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (189, 'Brooklyn                                          ', 'Prospect Heights                                  ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (190, 'Brooklyn                                          ', 'Prospect Park                                     ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (191, 'Queens                                            ', 'Queens Village                                    ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (192, 'Queens                                            ', 'Queensboro Hill                                   ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (193, 'Queens                                            ', 'Queensbridge/Ravenswood                           ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (194, 'Manhattan                                         ', 'Randalls Island                                   ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (195, 'Brooklyn                                          ', 'Red Hook                                          ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (196, 'Queens                                            ', 'Rego Park                                         ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (197, 'Queens                                            ', 'Richmond Hill                                     ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (198, 'Queens                                            ', 'Ridgewood                                         ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (199, 'Bronx                                             ', 'Rikers Island                                     ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (200, 'Bronx                                             ', 'Riverdale/North Riverdale/Fieldston               ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (201, 'Queens                                            ', 'Rockaway Park                                     ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (202, 'Manhattan                                         ', 'Roosevelt Island                                  ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (203, 'Queens                                            ', 'Rosedale                                          ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (204, 'Staten Island                                     ', 'Rossville/Woodrow                                 ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (205, 'Queens                                            ', 'Saint Albans                                      ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (206, 'Staten Island                                     ', 'Saint George/New Brighton                         ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (207, 'Queens                                            ', 'Saint Michaels Cemetery/Woodside                  ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (208, 'Bronx                                             ', 'Schuylerville/Edgewater Park                      ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (209, 'Manhattan                                         ', 'Seaport                                           ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (210, 'Brooklyn                                          ', 'Sheepshead Bay                                    ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (211, 'Manhattan                                         ', 'SoHo                                              ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (212, 'Bronx                                             ', 'Soundview/Bruckner                                ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (213, 'Bronx                                             ', 'Soundview/Castle Hill                             ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (214, 'Staten Island                                     ', 'South Beach/Dongan Hills                          ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (215, 'Queens                                            ', 'South Jamaica                                     ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (216, 'Queens                                            ', 'South Ozone Park                                  ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (217, 'Brooklyn                                          ', 'South Williamsburg                                ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (218, 'Queens                                            ', 'Springfield Gardens North                         ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (219, 'Queens                                            ', 'Springfield Gardens South                         ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (220, 'Bronx                                             ', 'Spuyten Duyvil/Kingsbridge                        ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (221, 'Staten Island                                     ', 'Stapleton                                         ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (222, 'Brooklyn                                          ', 'Starrett City                                     ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (223, 'Queens                                            ', 'Steinway                                          ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (224, 'Manhattan                                         ', 'Stuy Town/Peter Cooper Village                    ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (225, 'Brooklyn                                          ', 'Stuyvesant Heights                                ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (226, 'Queens                                            ', 'Sunnyside                                         ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (227, 'Brooklyn                                          ', 'Sunset Park East                                  ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (228, 'Brooklyn                                          ', 'Sunset Park West                                  ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (229, 'Manhattan                                         ', 'Sutton Place/Turtle Bay North                     ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (230, 'Manhattan                                         ', 'Times Sq/Theatre District                         ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (231, 'Manhattan                                         ', 'TriBeCa/Civic Center                              ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (232, 'Manhattan                                         ', 'Two Bridges/Seward Park                           ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (233, 'Manhattan                                         ', 'UN/Turtle Bay South                               ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (234, 'Manhattan                                         ', 'Union Sq                                          ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (235, 'Bronx                                             ', 'University Heights/Morris Heights                 ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (236, 'Manhattan                                         ', 'Upper East Side North                             ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (237, 'Manhattan                                         ', 'Upper East Side South                             ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (238, 'Manhattan                                         ', 'Upper West Side North                             ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (239, 'Manhattan                                         ', 'Upper West Side South                             ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (240, 'Bronx                                             ', 'Van Cortlandt Park                                ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (241, 'Bronx                                             ', 'Van Cortlandt Village                             ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (242, 'Bronx                                             ', 'Van Nest/Morris Park                              ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (243, 'Manhattan                                         ', 'Washington Heights North                          ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (244, 'Manhattan                                         ', 'Washington Heights South                          ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (245, 'Staten Island                                     ', 'West Brighton                                     ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (246, 'Manhattan                                         ', 'West Chelsea/Hudson Yards                         ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (247, 'Bronx                                             ', 'West Concourse                                    ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (248, 'Bronx                                             ', 'West Farms/Bronx River                            ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (249, 'Manhattan                                         ', 'West Village                                      ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (250, 'Bronx                                             ', 'Westchester Village/Unionport                     ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (251, 'Staten Island                                     ', 'Westerleigh                                       ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (252, 'Queens                                            ', 'Whitestone                                        ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (253, 'Queens                                            ', 'Willets Point                                     ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (254, 'Bronx                                             ', 'Williamsbridge/Olinville                          ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (255, 'Brooklyn                                          ', 'Williamsburg (North Side)                         ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (256, 'Brooklyn                                          ', 'Williamsburg (South Side)                         ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (257, 'Brooklyn                                          ', 'Windsor Terrace                                   ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (258, 'Queens                                            ', 'Woodhaven                                         ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (259, 'Bronx                                             ', 'Woodlawn/Wakefield                                ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (260, 'Queens                                            ', 'Woodside                                          ', 'Boro Zone                                         ');
INSERT INTO public."Locations" VALUES (261, 'Manhattan                                         ', 'World Trade Center                                ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (262, 'Manhattan                                         ', 'Yorkville East                                    ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (263, 'Manhattan                                         ', 'Yorkville West                                    ', 'Yellow Zone                                       ');
INSERT INTO public."Locations" VALUES (264, 'Unknown                                           ', 'NV                                                ', 'N/A                                               ');
INSERT INTO public."Locations" VALUES (265, 'Unknown                                           ', 'NA                                                ', 'N/A                                               ');


--
-- TOC entry 2987 (class 0 OID 0)
-- Dependencies: 205
-- Name: Locations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Locations_id_seq"', 266, true);


-- Completed on 2021-01-13 11:39:58 EST

--
-- PostgreSQL database dump complete
--

