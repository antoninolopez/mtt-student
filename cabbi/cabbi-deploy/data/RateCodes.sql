-- Adminer 4.7.8 PostgreSQL dump

INSERT INTO "RateCodes" ("id", "description") VALUES
(1,	'Standard rate'),
(2,	'JFK'),
(3,	'Newark'),
(4,	'Nassau or Westchester'),
(5,	'Negotiated fare'),
(6,	'Group ride'),
(99,	NULL),
(100,	NULL);

-- 2020-12-30 18:48:58.166592+00
