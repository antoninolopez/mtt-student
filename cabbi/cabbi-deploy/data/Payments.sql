-- Adminer 4.7.8 PostgreSQL dump

INSERT INTO "Payments" ("id", "type", "name") VALUES
(1,	'credit_card         ',	'Credit card                   '),
(2,	'cash                ',	'Cash                          '),
(3,	'no_charge           ',	'No charge                     '),
(4,	'dispute             ',	'Dispute                       '),
(5,	'unknown             ',	'Unknown                       '),
(6,	'voided_trip         ',	'Voided trip                   ');

-- 2020-12-30 18:48:51.039075+00
