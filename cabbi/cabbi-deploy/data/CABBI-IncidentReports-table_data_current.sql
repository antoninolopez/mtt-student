--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5
-- Dumped by pg_dump version 12.5

-- Started on 2021-01-13 11:39:32 EST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2980 (class 0 OID 90347)
-- Dependencies: 202
-- Data for Name: IncidentReports; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."IncidentReports" VALUES (1, '5fd1203c15f125280f06b731', 3, 265, 'Pedestrian was jaywalking between parked cars and walked in front of driver at the same moment driver let off the brake during a fare PU', 'pedestrian          ', 'MV-104A-XYZ-20200101-000000                       ', true, '2020-01-31 08:37:47');
INSERT INTO public."IncidentReports" VALUES (2, '5fd1203c15f125280f06b115', 5, 1, 'Dog got off leash and driver swerved to miss the animal and the owner chasing after it during the rain and driver slid into parked box truck rear bumper to avoid striking dog and owner. Side view mirror was struck and mirror glass shattered but avoided striking dog and pedestrian. Box truck driver declined to file police report due to negligible damage to box truck bumper.', 'pedestrian          ', NULL, false, '2020-01-31 07:51:11');
INSERT INTO public."IncidentReports" VALUES (3, '5fd1203c15f125280f06b76f', 6, 132, 'Drunken passenger fare quietly vomited in gap between the backseat and rear driver side door and did not inform the driver before paying the fare in cash and exiting the vehicle at the DU location.', 'passenger fare      ', NULL, NULL, '2020-01-31 07:42:44');
INSERT INTO public."IncidentReports" VALUES (4, '5fd1203c15f125280f06a0b2', 2, 11, 'Driver resigned from his job post on the spot and left company vehicle running in the middle of the street with keys locked inside during peak standstill rush hour congestion time. The vehicle was towed 3 hours later to nearest vendor garage. No property damage or personal injury was recorded as a result of this incident.', 'driver              ', NULL, NULL, '2020-01-31 07:48:36');
INSERT INTO public."IncidentReports" VALUES (5, '5fd1203c15f125280f06b869', 823, 148, NULL, 'pedestrian          ', 'MV-104A-XYZ-20200102-501930                       ', true, '2020-01-31 07:45:47');
INSERT INTO public."IncidentReports" VALUES (6, '5fd1203c15f125280f06a6b9', 687, 47, NULL, 'pedestrian          ', 'MV-104A-XYZ-20200102-67178                        ', true, '2020-01-31 07:50:30');
INSERT INTO public."IncidentReports" VALUES (7, '5fd1203c15f125280f06b2d1', 650, 252, NULL, 'pedestrian          ', 'MV-104A-XYZ-20200102-929541                       ', true, '2020-01-31 07:51:15');
INSERT INTO public."IncidentReports" VALUES (8, '5fd1203c15f125280f06a1a3', 307, 163, NULL, 'pedestrian          ', 'MV-104A-XYZ-20200102-517191                       ', true, '2020-01-31 07:50:57');
INSERT INTO public."IncidentReports" VALUES (9, '5fd1203c15f125280f06a4b7', 663, 154, NULL, 'pedestrian          ', 'MV-104A-XYZ-20200102-120965                       ', true, '2020-01-31 08:07:17');
INSERT INTO public."IncidentReports" VALUES (10, '5fd1203c15f125280f069b10', 824, 259, NULL, 'pedestrian          ', 'MV-104A-XYZ-20200102-105145                       ', true, '2020-01-31 07:43:25');
INSERT INTO public."IncidentReports" VALUES (11, '5fd1203c15f125280f06ae7e', 364, 257, NULL, 'motorist            ', 'MV-104A-XYZ-20200102-873830                       ', true, '2020-01-31 07:58:04');
INSERT INTO public."IncidentReports" VALUES (12, '5fd1203c15f125280f06a30d', 127, 212, NULL, 'motorist            ', 'MV-104A-XYZ-20200102-982387                       ', true, '2020-01-31 07:45:54');
INSERT INTO public."IncidentReports" VALUES (13, '5fd1203c15f125280f06a896', 360, 231, NULL, 'motorist            ', 'MV-104A-XYZ-20200102-876621                       ', true, '2020-01-31 08:01:23');
INSERT INTO public."IncidentReports" VALUES (14, '5fd1203c15f125280f06a7ef', 433, 175, NULL, 'motorist            ', 'MV-104A-XYZ-20200102-920037                       ', true, '2020-01-31 07:50:21');
INSERT INTO public."IncidentReports" VALUES (15, '5fd1203c15f125280f06a4a8', 370, 114, NULL, 'motorist            ', 'MV-104A-XYZ-20200102-757281                       ', true, '2020-01-31 08:09:19');
INSERT INTO public."IncidentReports" VALUES (16, '5fd1203c15f125280f06aa73', 258, 173, NULL, 'passenger fare      ', 'MV-104A-XYZ-20200102-594236                       ', true, '2020-01-31 07:56:23');
INSERT INTO public."IncidentReports" VALUES (17, '5fd1203c15f125280f06a5a9', 837, 149, NULL, 'passenger fare      ', 'MV-104A-XYZ-20200102-193422                       ', true, '2020-01-31 07:56:02');
INSERT INTO public."IncidentReports" VALUES (18, '5fd1203c15f125280f06b47a', 130, 176, NULL, 'passenger fare      ', 'MV-104A-XYZ-20200102-233883                       ', true, '2020-01-31 07:51:01');
INSERT INTO public."IncidentReports" VALUES (19, '5fd1203c15f125280f069a3c', 145, 45, NULL, 'passenger fare      ', 'MV-104A-XYZ-20200102-585233                       ', true, '2020-01-31 07:47:32');
INSERT INTO public."IncidentReports" VALUES (20, '5fd1203c15f125280f06b034', 577, 14, NULL, 'passenger fare      ', 'MV-104A-XYZ-20200102-914287                       ', true, '2020-01-31 07:48:06');
INSERT INTO public."IncidentReports" VALUES (21, '5fd1203c15f125280f06ab8a', 308, 187, NULL, 'driver              ', 'MV-104A-XYZ-20200102-101928                       ', true, '2020-01-31 07:49:50');
INSERT INTO public."IncidentReports" VALUES (22, '5fd1203c15f125280f06a65a', 9, 215, NULL, 'driver              ', 'MV-104A-XYZ-20200102-283368                       ', true, '2020-01-31 07:47:18');
INSERT INTO public."IncidentReports" VALUES (23, '5fd1203c15f125280f06ba65', 729, 89, NULL, 'driver              ', 'MV-104A-XYZ-20200102-687208                       ', true, '2020-01-31 07:47:22');
INSERT INTO public."IncidentReports" VALUES (24, '5fd1203c15f125280f06963d', 15, 56, NULL, 'driver              ', 'MV-104A-XYZ-20200102-196178                       ', true, '2020-01-31 07:48:18');
INSERT INTO public."IncidentReports" VALUES (25, '5fd1203d15f125280f06dd93', 785, 68, NULL, 'driver              ', 'MV-104A-XYZ-20200102-721599                       ', true, '2020-01-31 09:03:01');
INSERT INTO public."IncidentReports" VALUES (26, '5fd1203c15f125280f06bb14', 826, 193, NULL, 'motorist            ', 'MV-104A-XYZ-20200102-426593                       ', true, '2020-01-31 07:53:39');
INSERT INTO public."IncidentReports" VALUES (27, '5fd1203c15f125280f06a5bf', 493, 124, NULL, 'motorist            ', 'MV-104A-XYZ-20200102-293195                       ', true, '2020-01-31 07:56:45');
INSERT INTO public."IncidentReports" VALUES (28, '5fd1203c15f125280f06994f', 278, 84, NULL, 'motorist            ', 'MV-104A-XYZ-20200102-889738                       ', true, '2020-01-31 07:58:40');
INSERT INTO public."IncidentReports" VALUES (29, '5fd1203c15f125280f06b1e2', 488, 214, NULL, 'motorist            ', 'MV-104A-XYZ-20200102-731275                       ', true, '2020-01-31 07:57:28');
INSERT INTO public."IncidentReports" VALUES (30, '5fd1203d15f125280f06d48c', 746, 208, NULL, 'motorist            ', 'MV-104A-XYZ-20200102-752673                       ', true, '2020-01-31 07:53:08');
INSERT INTO public."IncidentReports" VALUES (31, '5fd1203c15f125280f06a148', 470, 155, NULL, 'bicycle             ', 'MV-104A-XYZ-20200102-937005                       ', true, '2020-01-31 07:53:55');
INSERT INTO public."IncidentReports" VALUES (32, '5fd1203c15f125280f06abf7', 320, 222, NULL, 'bicycle             ', 'MV-104A-XYZ-20200102-507053                       ', true, '2020-01-31 07:57:03');
INSERT INTO public."IncidentReports" VALUES (33, '5fd1203d15f125280f06dc0f', 756, 66, NULL, 'bicycle             ', 'MV-104A-XYZ-20200102-632294                       ', true, '2020-01-31 08:08:23');
INSERT INTO public."IncidentReports" VALUES (34, '5fd1203c15f125280f06a2af', 709, 254, NULL, 'bicycle             ', 'MV-104A-XYZ-20200102-805107                       ', true, '2020-01-31 07:58:03');
INSERT INTO public."IncidentReports" VALUES (35, '5fd1203c15f125280f06983d', 659, 27, NULL, 'bicycle             ', 'MV-104A-XYZ-20200102-766155                       ', true, '2020-01-31 07:57:17');
INSERT INTO public."IncidentReports" VALUES (36, '5fd1203c15f125280f06c591', 641, 53, NULL, 'bicycle             ', 'MV-104A-XYZ-20200102-619007                       ', true, '2020-01-31 08:08:29');
INSERT INTO public."IncidentReports" VALUES (37, '5fd1203c15f125280f06b336', 58, 137, NULL, 'bicycle             ', 'MV-104A-XYZ-20200102-967246                       ', true, '2020-01-31 08:15:59');
INSERT INTO public."IncidentReports" VALUES (38, '5fd1203c15f125280f06b224', 219, 22, NULL, 'bicycle             ', 'MV-104A-XYZ-20200102-73732                        ', true, '2020-01-31 07:59:58');
INSERT INTO public."IncidentReports" VALUES (39, '5fd1203c15f125280f069971', 82, 11, NULL, 'bicycle             ', 'MV-104A-XYZ-20200102-111950                       ', true, '2020-01-31 07:42:56');
INSERT INTO public."IncidentReports" VALUES (40, '5fd1203c15f125280f06b9f5', 193, 230, NULL, 'bicycle             ', 'MV-104A-XYZ-20200102-124883                       ', true, '2020-01-31 08:20:47');
INSERT INTO public."IncidentReports" VALUES (41, '5fd1203c15f125280f069dd7', 213, 102, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-789279                       ', true, '2020-01-31 07:57:21');
INSERT INTO public."IncidentReports" VALUES (42, '5fd1203c15f125280f06a7ef', 433, 2, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-105390                       ', true, '2020-01-31 07:50:21');
INSERT INTO public."IncidentReports" VALUES (43, '5fd1203c15f125280f069fab', 92, 219, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-687085                       ', true, '2020-01-31 07:52:45');
INSERT INTO public."IncidentReports" VALUES (44, '5fd1203c15f125280f06a13e', 246, 188, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-48839                        ', true, '2020-01-31 07:55:23');
INSERT INTO public."IncidentReports" VALUES (45, '5fd1203c15f125280f06ac25', 243, 39, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-698350                       ', true, '2020-01-31 07:59:32');
INSERT INTO public."IncidentReports" VALUES (46, '5fd1203c15f125280f06b1e2', 488, 260, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-612232                       ', true, '2020-01-31 07:57:28');
INSERT INTO public."IncidentReports" VALUES (47, '5fd1203c15f125280f06a522', 684, 60, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-852175                       ', true, '2020-01-31 07:57:33');
INSERT INTO public."IncidentReports" VALUES (48, '5fd1203c15f125280f06ac51', 409, 66, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-367239                       ', true, '2020-01-31 07:58:29');
INSERT INTO public."IncidentReports" VALUES (49, '5fd1203c15f125280f06abe0', 388, 121, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-10844                        ', true, '2020-01-31 07:52:53');
INSERT INTO public."IncidentReports" VALUES (50, '5fd1203c15f125280f069675', 46, 197, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-206538                       ', true, '2020-01-31 07:55:07');
INSERT INTO public."IncidentReports" VALUES (51, '5fd1203c15f125280f06a43a', 740, 45, NULL, 'criminal            ', 'MV-104A-XYZ-20200102-642564                       ', true, '2020-01-31 08:11:28');
INSERT INTO public."IncidentReports" VALUES (52, '5fd1203c15f125280f0699e6', 718, 48, NULL, 'criminal            ', 'MV-104A-XYZ-20200102-620691                       ', true, '2020-01-31 07:43:58');
INSERT INTO public."IncidentReports" VALUES (53, '5fd1203d15f125280f06cbc5', 523, 204, NULL, 'criminal            ', 'MV-104A-XYZ-20200102-878953                       ', true, '2020-01-31 08:29:36');
INSERT INTO public."IncidentReports" VALUES (54, '5fd1203c15f125280f06ae9d', 265, 258, NULL, 'criminal            ', 'MV-104A-XYZ-20200102-640949                       ', true, '2020-01-31 08:00:28');
INSERT INTO public."IncidentReports" VALUES (55, '5fd1203d15f125280f06dfff', 764, 191, NULL, 'criminal            ', 'MV-104A-XYZ-20200102-496872                       ', true, '2020-01-31 09:05:16');
INSERT INTO public."IncidentReports" VALUES (56, '5fd1203c15f125280f06a332', 442, 122, NULL, 'criminal            ', 'MV-104A-XYZ-20200102-314736                       ', true, '2020-01-31 07:43:16');
INSERT INTO public."IncidentReports" VALUES (57, '5fd1203c15f125280f06a264', 1, 28, NULL, 'criminal            ', 'MV-104A-XYZ-20200102-426256                       ', true, '2020-01-31 08:02:40');
INSERT INTO public."IncidentReports" VALUES (58, '5fd1203c15f125280f06a8ba', 671, 106, NULL, 'criminal            ', 'MV-104A-XYZ-20200102-683079                       ', true, '2020-01-31 07:50:57');
INSERT INTO public."IncidentReports" VALUES (59, '5fd1203c15f125280f069db4', 190, 100, NULL, 'criminal            ', 'MV-104A-XYZ-20200102-878679                       ', true, '2020-01-31 08:02:45');
INSERT INTO public."IncidentReports" VALUES (60, '5fd1203c15f125280f06b39b', 401, 153, NULL, 'criminal            ', 'MV-104A-XYZ-20200102-363060                       ', true, '2020-01-31 07:47:08');
INSERT INTO public."IncidentReports" VALUES (61, '5fd1203c15f125280f069ad7', 325, 261, NULL, 'other               ', 'MV-104A-XYZ-20200102-215834                       ', true, '2020-01-31 07:47:22');
INSERT INTO public."IncidentReports" VALUES (62, '5fd1203c15f125280f069aad', 498, 135, NULL, 'other               ', 'MV-104A-XYZ-20200102-3335                         ', true, '2020-01-31 07:50:17');
INSERT INTO public."IncidentReports" VALUES (63, '5fd1203c15f125280f069962', 791, 79, NULL, 'other               ', 'MV-104A-XYZ-20200102-232730                       ', true, '2020-01-31 08:02:45');
INSERT INTO public."IncidentReports" VALUES (64, '5fd1203c15f125280f06b589', 697, 252, NULL, 'other               ', 'MV-104A-XYZ-20200102-77174                        ', true, '2020-01-31 07:53:11');
INSERT INTO public."IncidentReports" VALUES (65, '5fd1203c15f125280f06b14b', 519, 6, NULL, 'other               ', 'MV-104A-XYZ-20200102-617413                       ', true, '2020-01-31 08:03:23');
INSERT INTO public."IncidentReports" VALUES (66, '5fd1203c15f125280f06a694', 691, 175, NULL, 'other               ', 'MV-104A-XYZ-20200102-704896                       ', true, '2020-01-31 07:47:18');
INSERT INTO public."IncidentReports" VALUES (67, '5fd1203c15f125280f06a8fd', 322, 103, NULL, 'other               ', 'MV-104A-XYZ-20200102-967073                       ', true, '2020-01-31 07:52:39');
INSERT INTO public."IncidentReports" VALUES (68, '5fd1203c15f125280f06a25d', 234, 164, NULL, 'other               ', 'MV-104A-XYZ-20200102-456435                       ', true, '2020-01-31 08:09:07');
INSERT INTO public."IncidentReports" VALUES (69, '5fd1203c15f125280f069bbb', 405, 50, NULL, 'other               ', 'MV-104A-XYZ-20200102-889884                       ', true, '2020-01-31 08:06:11');
INSERT INTO public."IncidentReports" VALUES (70, '5fd1203c15f125280f06ba67', 485, 46, NULL, 'other               ', 'MV-104A-XYZ-20200102-374777                       ', true, '2020-01-31 07:52:10');
INSERT INTO public."IncidentReports" VALUES (71, '5fd1203c15f125280f069bbb', 405, 135, NULL, 'passenger fare      ', 'MV-104A-XYZ-20200102-782333                       ', true, '2020-01-31 08:06:11');
INSERT INTO public."IncidentReports" VALUES (72, '5fd1203c15f125280f06a496', 416, 253, NULL, 'passenger fare      ', 'MV-104A-XYZ-20200102-45619                        ', true, '2020-01-31 08:08:21');
INSERT INTO public."IncidentReports" VALUES (73, '5fd1203c15f125280f06a369', 268, 128, NULL, 'passenger fare      ', 'MV-104A-XYZ-20200102-784808                       ', true, '2020-01-31 07:58:53');
INSERT INTO public."IncidentReports" VALUES (74, '5fd1203c15f125280f06ae61', 33, 33, NULL, 'passenger fare      ', 'MV-104A-XYZ-20200102-881140                       ', true, '2020-01-31 07:44:57');
INSERT INTO public."IncidentReports" VALUES (75, '5fd1203c15f125280f06c796', 467, 23, NULL, 'passenger fare      ', 'MV-104A-XYZ-20200102-770941                       ', true, '2020-01-31 08:30:55');
INSERT INTO public."IncidentReports" VALUES (76, '5fd1203c15f125280f069903', 150, 234, NULL, 'passenger fare      ', 'MV-104A-XYZ-20200102-812989                       ', true, '2020-01-31 07:48:02');
INSERT INTO public."IncidentReports" VALUES (77, '5fd1203c15f125280f06b0cf', 8, 187, NULL, 'passenger fare      ', 'MV-104A-XYZ-20200102-621531                       ', true, '2020-01-31 07:49:13');
INSERT INTO public."IncidentReports" VALUES (78, '5fd1203c15f125280f06a8bd', 582, 56, NULL, 'passenger fare      ', 'MV-104A-XYZ-20200102-474146                       ', true, '2020-01-31 08:53:53');
INSERT INTO public."IncidentReports" VALUES (79, '5fd1203c15f125280f069753', 806, 34, NULL, 'passenger fare      ', 'MV-104A-XYZ-20200102-787440                       ', true, '2020-01-31 08:20:52');
INSERT INTO public."IncidentReports" VALUES (80, '5fd1203d15f125280f06ea65', 646, 11, NULL, 'passenger fare      ', 'MV-104A-XYZ-20200102-263524                       ', true, '2020-01-31 08:11:15');
INSERT INTO public."IncidentReports" VALUES (81, '5fd1203c15f125280f06b9c4', 391, 193, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-169039                       ', true, '2020-01-31 08:03:39');
INSERT INTO public."IncidentReports" VALUES (82, '5fd1203c15f125280f06984c', 634, 237, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-613274                       ', true, '2020-01-31 07:42:56');
INSERT INTO public."IncidentReports" VALUES (83, '5fd1203c15f125280f06b6b1', 799, 223, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-412238                       ', true, '2020-01-31 07:57:35');
INSERT INTO public."IncidentReports" VALUES (84, '5fd1203c15f125280f06b562', 40, 238, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-304064                       ', true, '2020-01-31 07:54:04');
INSERT INTO public."IncidentReports" VALUES (85, '5fd1203c15f125280f06a37c', 658, 161, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-416954                       ', true, '2020-01-31 07:58:20');
INSERT INTO public."IncidentReports" VALUES (86, '5fd1203c15f125280f06aa5f', 461, 86, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-26370                        ', true, '2020-01-31 08:03:56');
INSERT INTO public."IncidentReports" VALUES (87, '5fd1203c15f125280f069f1d', 713, 16, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-996107                       ', true, '2020-01-31 07:47:48');
INSERT INTO public."IncidentReports" VALUES (88, '5fd1203c15f125280f069a06', 571, 115, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-997375                       ', true, '2020-01-31 08:00:51');
INSERT INTO public."IncidentReports" VALUES (89, '5fd1203c15f125280f069ad9', 63, 188, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-251787                       ', true, '2020-01-31 07:52:53');
INSERT INTO public."IncidentReports" VALUES (90, '5fd1203c15f125280f06b168', 257, 96, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-100934                       ', true, '2020-01-31 07:57:12');
INSERT INTO public."IncidentReports" VALUES (91, '5fd1203c15f125280f069794', 597, 222, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-503579                       ', true, '2020-01-31 08:02:20');
INSERT INTO public."IncidentReports" VALUES (92, '5fd1203c15f125280f06b79e', 820, 83, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-79260                        ', true, '2020-01-31 07:52:45');
INSERT INTO public."IncidentReports" VALUES (93, '5fd1203c15f125280f0697e8', 375, 88, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-422272                       ', true, '2020-01-31 07:50:51');
INSERT INTO public."IncidentReports" VALUES (94, '5fd1203c15f125280f06b814', 321, 60, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-611531                       ', true, '2020-01-31 08:00:12');
INSERT INTO public."IncidentReports" VALUES (95, '5fd1203c15f125280f06b0cf', 8, 22, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-538801                       ', true, '2020-01-31 07:49:13');
INSERT INTO public."IncidentReports" VALUES (96, '5fd1203c15f125280f06a3e4', 761, 63, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-410710                       ', true, '2020-01-31 08:04:49');
INSERT INTO public."IncidentReports" VALUES (97, '5fd1203c15f125280f06b7ab', 326, 167, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-479120                       ', true, '2020-01-31 07:58:19');
INSERT INTO public."IncidentReports" VALUES (98, '5fd1203c15f125280f06a424', 624, 226, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-291264                       ', true, '2020-01-31 07:53:45');
INSERT INTO public."IncidentReports" VALUES (99, '5fd1203c15f125280f06bb7a', 453, 149, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-987490                       ', true, '2020-01-31 07:49:42');
INSERT INTO public."IncidentReports" VALUES (100, '5fd1203c15f125280f06b143', 126, 204, NULL, 'traffic             ', 'MV-104A-XYZ-20200102-13029                        ', true, '2020-01-31 07:49:48');
INSERT INTO public."IncidentReports" VALUES (101, '5fd1203c15f125280f06b057', 101, 156, NULL, 'bicycle             ', 'MV-104A-XYZ-20200102-989973                       ', true, '2020-01-31 08:04:18');
INSERT INTO public."IncidentReports" VALUES (102, '5fd1203d15f125280f06d963', 561, 242, NULL, 'pedestrian          ', 'MV-104A-XYZ-20200102-201801                       ', true, '2020-01-31 08:47:12');
INSERT INTO public."IncidentReports" VALUES (103, '5fd1203c15f125280f06b74a', 450, 255, NULL, 'passenger fare      ', 'MV-104A-XYZ-20200102-701675                       ', true, '2020-01-31 07:52:57');
INSERT INTO public."IncidentReports" VALUES (104, '5fd1203c15f125280f06b344', 618, 170, NULL, 'passenger fare      ', 'MV-104A-XYZ-20200102-268851                       ', true, '2020-01-31 08:07:56');
INSERT INTO public."IncidentReports" VALUES (105, '5fd1203c15f125280f06b071', 766, 129, NULL, 'motorist            ', 'MV-104A-XYZ-20200102-247129                       ', true, '2020-01-31 08:19:39');
INSERT INTO public."IncidentReports" VALUES (106, '5fd1203c15f125280f069fa0', 334, 135, NULL, 'passenger fare      ', 'MV-104A-XYZ-20200102-173741                       ', true, '2020-01-31 07:50:08');
INSERT INTO public."IncidentReports" VALUES (107, '5fd1203c15f125280f06bff9', 524, 138, NULL, 'driver              ', 'MV-104A-XYZ-20200102-422933                       ', true, '2020-01-31 08:30:33');
INSERT INTO public."IncidentReports" VALUES (108, '5fd1203c15f125280f06a87d', 414, 208, NULL, 'bicycle             ', 'MV-104A-XYZ-20200102-574328                       ', true, '2020-01-31 08:11:00');
INSERT INTO public."IncidentReports" VALUES (109, '5fd1203c15f125280f06ad67', 825, 191, NULL, 'motorist            ', 'MV-104A-XYZ-20200102-104588                       ', true, '2020-01-31 07:53:37');
INSERT INTO public."IncidentReports" VALUES (110, '5fd1203c15f125280f06b356', 212, 237, NULL, 'pedestrian          ', 'MV-104A-XYZ-20200102-756209                       ', true, '2020-01-31 07:53:03');


--
-- TOC entry 2987 (class 0 OID 0)
-- Dependencies: 203
-- Name: IncidentReports_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."IncidentReports_id_seq"', 111, true);


-- Completed on 2021-01-13 11:39:33 EST

--
-- PostgreSQL database dump complete
--

