--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5
-- Dumped by pg_dump version 12.5

-- Started on 2021-01-13 11:42:15 EST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2980 (class 0 OID 90381)
-- Dependencies: 213
-- Data for Name: Vendors; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."Vendors" VALUES (1, 'Creative Mobile Technologies, LLC', NULL, true);
INSERT INTO public."Vendors" VALUES (2, 'VeriFone Incorporated', NULL, true);
INSERT INTO public."Vendors" VALUES (32767, NULL, NULL, NULL);


--
-- TOC entry 2987 (class 0 OID 0)
-- Dependencies: 214
-- Name: Vendors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Vendors_id_seq"', 3, true);


-- Completed on 2021-01-13 11:42:15 EST

--
-- PostgreSQL database dump complete
--

