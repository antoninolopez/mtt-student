--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5
-- Dumped by pg_dump version 12.5

-- Started on 2021-01-13 11:30:37 EST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 202 (class 1259 OID 90347)
-- Name: IncidentReports; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."IncidentReports" (
    id bigint NOT NULL,
    ride_id character varying(24) NOT NULL,
    vehicle_id integer NOT NULL,
    location_id integer NOT NULL,
    description text,
    type character varying(20),
    police_report character varying(50),
    status boolean,
    incident_datetime timestamp without time zone
);


ALTER TABLE public."IncidentReports" OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 90353)
-- Name: IncidentReports_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."IncidentReports_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."IncidentReports_id_seq" OWNER TO postgres;

--
-- TOC entry 3013 (class 0 OID 0)
-- Dependencies: 203
-- Name: IncidentReports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."IncidentReports_id_seq" OWNED BY public."IncidentReports".id;


--
-- TOC entry 204 (class 1259 OID 90355)
-- Name: Locations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Locations" (
    id integer NOT NULL,
    borough character varying(50) NOT NULL,
    zone character varying(50) NOT NULL,
    service_zone character varying(50) NOT NULL
);


ALTER TABLE public."Locations" OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 90358)
-- Name: Locations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Locations_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Locations_id_seq" OWNER TO postgres;

--
-- TOC entry 3014 (class 0 OID 0)
-- Dependencies: 205
-- Name: Locations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Locations_id_seq" OWNED BY public."Locations".id;


--
-- TOC entry 206 (class 1259 OID 90360)
-- Name: Payments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Payments" (
    id smallint NOT NULL,
    type character varying(20),
    name character varying(30)
);


ALTER TABLE public."Payments" OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 90363)
-- Name: Payments_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Payments_id_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Payments_id_seq" OWNER TO postgres;

--
-- TOC entry 3015 (class 0 OID 0)
-- Dependencies: 207
-- Name: Payments_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Payments_id_seq" OWNED BY public."Payments".id;


--
-- TOC entry 208 (class 1259 OID 90365)
-- Name: RateCodes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."RateCodes" (
    id smallint NOT NULL,
    description character varying(50)
);


ALTER TABLE public."RateCodes" OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 90368)
-- Name: RateCodes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."RateCodes_id_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."RateCodes_id_seq" OWNER TO postgres;

--
-- TOC entry 3016 (class 0 OID 0)
-- Dependencies: 209
-- Name: RateCodes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."RateCodes_id_seq" OWNED BY public."RateCodes".id;


--
-- TOC entry 210 (class 1259 OID 90370)
-- Name: Rides; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Rides" (
    _id character varying(24) NOT NULL,
    "VendorID" smallint,
    tpep_pickup_datetime timestamp without time zone,
    tpep_dropoff_datetime timestamp without time zone,
    passenger_count smallint,
    trip_distance numeric,
    "RatecodeID" integer,
    store_and_fwd_flag character varying(1),
    "PULocationID" integer,
    "DOLocationID" integer,
    payment_type smallint,
    fare_amount integer,
    extra integer,
    mta_tax integer,
    tip_amount integer,
    tolls_amount integer,
    improvement_surcharge integer,
    total_amount integer,
    congestion_surcharge integer,
    vehicle_id integer
);


ALTER TABLE public."Rides" OWNER TO postgres;

--
-- TOC entry 211 (class 1259 OID 90376)
-- Name: Vehicles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Vehicles" (
    id integer NOT NULL,
    vendor_id integer,
    year integer,
    make character varying(50),
    model character varying(50),
    license_plate character varying(20),
    max_passengers smallint,
    status boolean
);


ALTER TABLE public."Vehicles" OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 90379)
-- Name: Vehicles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Vehicles_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Vehicles_id_seq" OWNER TO postgres;

--
-- TOC entry 3017 (class 0 OID 0)
-- Dependencies: 212
-- Name: Vehicles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Vehicles_id_seq" OWNED BY public."Vehicles".id;


--
-- TOC entry 213 (class 1259 OID 90381)
-- Name: Vendors; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."Vendors" (
    id smallint NOT NULL,
    name character varying(50),
    contract_id character varying(20),
    status boolean
);


ALTER TABLE public."Vendors" OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 90384)
-- Name: Vendors_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."Vendors_id_seq"
    AS smallint
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."Vendors_id_seq" OWNER TO postgres;

--
-- TOC entry 3018 (class 0 OID 0)
-- Dependencies: 214
-- Name: Vendors_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."Vendors_id_seq" OWNED BY public."Vendors".id;


--
-- TOC entry 215 (class 1259 OID 90386)
-- Name: WorkOrders; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."WorkOrders" (
    id bigint NOT NULL,
    incident_id integer NOT NULL,
    work_description text,
    material_cost numeric,
    labor_cost numeric,
    final_total_costs numeric,
    start_date date NOT NULL,
    end_date date NOT NULL,
    additional_details text,
    approved_by uuid,
    approval_status character varying
);


ALTER TABLE public."WorkOrders" OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 90392)
-- Name: WorkOrders_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public."WorkOrders_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."WorkOrders_id_seq" OWNER TO postgres;

--
-- TOC entry 3019 (class 0 OID 0)
-- Dependencies: 216
-- Name: WorkOrders_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public."WorkOrders_id_seq" OWNED BY public."WorkOrders".id;


--
-- TOC entry 2859 (class 2604 OID 90394)
-- Name: IncidentReports id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."IncidentReports" ALTER COLUMN id SET DEFAULT nextval('public."IncidentReports_id_seq"'::regclass);


--
-- TOC entry 2860 (class 2604 OID 90395)
-- Name: Locations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Locations" ALTER COLUMN id SET DEFAULT nextval('public."Locations_id_seq"'::regclass);


--
-- TOC entry 2861 (class 2604 OID 90396)
-- Name: Payments id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Payments" ALTER COLUMN id SET DEFAULT nextval('public."Payments_id_seq"'::regclass);


--
-- TOC entry 2862 (class 2604 OID 90397)
-- Name: RateCodes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."RateCodes" ALTER COLUMN id SET DEFAULT nextval('public."RateCodes_id_seq"'::regclass);


--
-- TOC entry 2863 (class 2604 OID 90398)
-- Name: Vehicles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Vehicles" ALTER COLUMN id SET DEFAULT nextval('public."Vehicles_id_seq"'::regclass);


--
-- TOC entry 2864 (class 2604 OID 90399)
-- Name: Vendors id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Vendors" ALTER COLUMN id SET DEFAULT nextval('public."Vendors_id_seq"'::regclass);


--
-- TOC entry 2865 (class 2604 OID 90400)
-- Name: WorkOrders id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."WorkOrders" ALTER COLUMN id SET DEFAULT nextval('public."WorkOrders_id_seq"'::regclass);


--
-- TOC entry 2867 (class 2606 OID 90402)
-- Name: IncidentReports IncidentReports_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."IncidentReports"
    ADD CONSTRAINT "IncidentReports_pkey" PRIMARY KEY (id);


--
-- TOC entry 2869 (class 2606 OID 90404)
-- Name: Locations Locations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Locations"
    ADD CONSTRAINT "Locations_pkey" PRIMARY KEY (id);


--
-- TOC entry 2871 (class 2606 OID 90406)
-- Name: Payments Payments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Payments"
    ADD CONSTRAINT "Payments_pkey" PRIMARY KEY (id);


--
-- TOC entry 2873 (class 2606 OID 90408)
-- Name: RateCodes RateCodes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."RateCodes"
    ADD CONSTRAINT "RateCodes_pkey" PRIMARY KEY (id);


--
-- TOC entry 2875 (class 2606 OID 90410)
-- Name: Rides Rides_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Rides"
    ADD CONSTRAINT "Rides_pkey" PRIMARY KEY (_id);


--
-- TOC entry 2877 (class 2606 OID 90412)
-- Name: Vehicles Vehicles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Vehicles"
    ADD CONSTRAINT "Vehicles_pkey" PRIMARY KEY (id);


--
-- TOC entry 2879 (class 2606 OID 90414)
-- Name: Vendors Vendors_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."Vendors"
    ADD CONSTRAINT "Vendors_pkey" PRIMARY KEY (id);


--
-- TOC entry 2881 (class 2606 OID 90416)
-- Name: WorkOrders WorkOrders_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."WorkOrders"
    ADD CONSTRAINT "WorkOrders_pkey" PRIMARY KEY (id);


-- Completed on 2021-01-13 11:30:37 EST

--
-- PostgreSQL database dump complete
--

