-- Adminer 4.7.8 PostgreSQL dump

CREATE SEQUENCE "IncidentReports_id_seq";

CREATE TABLE "public"."IncidentReports" (
    "id" bigint DEFAULT nextval('"IncidentReports_id_seq"') NOT NULL,
    "ride_id" character varying(24) NOT NULL,
    "vehicle_id" integer NOT NULL,
    "location_id" integer NOT NULL,
    "description" text,
    "type" character varying(20),
    "police_report" character varying(50),
    "status" boolean,
    "incident_datetime" timestamp,
    CONSTRAINT "IncidentReports_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


CREATE SEQUENCE "Locations_id_seq";

CREATE TABLE "public"."Locations" (
    "id" integer DEFAULT nextval('"Locations_id_seq"') NOT NULL,
    "borough" character varying(50) NOT NULL,
    "zone" character varying(50) NOT NULL,
    "service_zone" character varying(50) NOT NULL,
    CONSTRAINT "Locations_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


CREATE SEQUENCE "Payments_id_seq";

CREATE TABLE "public"."Payments" (
    "id" smallint DEFAULT nextval('"Payments_id_seq"') NOT NULL,
    "type" character varying(20),
    "name" character varying(30),
    CONSTRAINT "Payments_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


CREATE SEQUENCE "RateCodes_id_seq";

CREATE TABLE "public"."RateCodes" (
    "id" smallint DEFAULT nextval('"RateCodes_id_seq"') NOT NULL,
    "description" character varying(50),
    CONSTRAINT "RateCodes_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


CREATE TABLE "public"."Rides" (
    "_id" character varying(24) NOT NULL,
    "VendorID" smallint,
    "tpep_pickup_datetime" timestamp,
    "tpep_dropoff_datetime" timestamp,
    "passenger_count" smallint,
    "trip_distance" numeric,
    "RatecodeID" integer,
    "store_and_fwd_flag" character varying(1),
    "PULocationID" integer,
    "DOLocationID" integer,
    "payment_type" smallint,
    "fare_amount" integer,
    "extra" integer,
    "mta_tax" integer,
    "tip_amount" integer,
    "tolls_amount" integer,
    "improvement_surcharge" integer,
    "total_amount" integer,
    "congestion_surcharge" integer,
    "vehicle_id" integer,
    CONSTRAINT "Rides_pkey" PRIMARY KEY ("_id")
) WITH (oids = false);


CREATE SEQUENCE "Vehicles_id_seq";

CREATE TABLE "public"."Vehicles" (
    "id" integer DEFAULT nextval('"Vehicles_id_seq"') NOT NULL,
    "vendor_id" integer,
    "year" integer,
    "make" character varying(50),
    "model" character varying(50),
    "license_plate" character varying(20),
    "max_passengers" smallint,
    "status" boolean,
    CONSTRAINT "Vehicles_pkey1" PRIMARY KEY ("id")
) WITH (oids = false);


CREATE SEQUENCE "Vendors_id_seq";

CREATE TABLE "public"."Vendors" (
    "id" smallint DEFAULT nextval('"Vendors_id_seq"') NOT NULL,
    "name" character varying(50),
    "contract_id" character varying(20),
    "status" boolean,
    CONSTRAINT "Vendors_pkey" PRIMARY KEY ("id")
) WITH (oids = false);


CREATE SEQUENCE "WorkOrders_id_seq";

CREATE TABLE "public"."WorkOrders" (
    "id" bigint DEFAULT nextval('"WorkOrders_id_seq"') NOT NULL,
    "incident_id" integer NOT NULL,
    "work_description" text,
    "material_cost" numeric,
    "labor_cost" numeric,
    "final_total_costs" numeric,
    "start_date" date NOT NULL,
    "end_date" date NOT NULL,
    "additional_details" text,
    "status" boolean,
    CONSTRAINT "WorkOrders_pkey1" PRIMARY KEY ("id")
) WITH (oids = false);


-- 2020-12-30 18:46:51.853065+00
