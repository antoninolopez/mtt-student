--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5
-- Dumped by pg_dump version 12.5

-- Started on 2021-01-13 11:40:50 EST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2980 (class 0 OID 90365)
-- Dependencies: 208
-- Data for Name: RateCodes; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."RateCodes" VALUES (1, 'Standard rate');
INSERT INTO public."RateCodes" VALUES (2, 'JFK');
INSERT INTO public."RateCodes" VALUES (3, 'Newark');
INSERT INTO public."RateCodes" VALUES (4, 'Nassau or Westchester');
INSERT INTO public."RateCodes" VALUES (5, 'Negotiated fare');
INSERT INTO public."RateCodes" VALUES (6, 'Group ride');
INSERT INTO public."RateCodes" VALUES (32767, NULL);
INSERT INTO public."RateCodes" VALUES (99, 'Unknown');


--
-- TOC entry 2987 (class 0 OID 0)
-- Dependencies: 209
-- Name: RateCodes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."RateCodes_id_seq"', 7, true);


-- Completed on 2021-01-13 11:40:50 EST

--
-- PostgreSQL database dump complete
--

