--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5
-- Dumped by pg_dump version 12.5

-- Started on 2021-01-13 11:42:59 EST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 2980 (class 0 OID 90386)
-- Dependencies: 215
-- Data for Name: WorkOrders; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."WorkOrders" VALUES (1, 1, 'Repairs made to vehicle in vendor garage with 1 hour labor from compatible salvage of decommissioned vehicle parts (vehicle_id = 999)', 50.00, 50.00, 100.00, '2020-01-01', '2020-01-01', 'Minor repair completed in vendor garage_id = 1, same day.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (2, 2, 'Estimate for replacement parts are $700 new, $300 salvaged. Salvage parts available same day. Labor repairs are $500 for dent removal and resurface of damaged paint and body or $100 for only primer and paint of new parts surfaces. Awaiting approval decision since 2020-01-01 13:00:00', 700.00, 500.00, NULL, '2020-01-01', '2020-01-01', 'Awaiting approval decision for new or used repair parts. Maximum estimate cost is approximately $1200. Decision pending since 2020-01-01', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (3, 3, 'No repairs necessary. Damage is negligible. No loss of use.', 0.00, 0.00, 0.00, '2020-01-01', '2020-01-01', 'No repairs necessary. Damage is negligible. No loss of use.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (4, 4, 'Front Passenger seat ripped by fare by objects in pants pockets', 50.00, 50.00, 100.00, '2020-01-01', '2020-01-02', 'Next day repair, after driver shift, in house (vendor garage). Customer charged voluntarily for repair.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (5, 5, 'Front Passenger seat ripped by fare by objects in pants pockets', 50.00, 50.00, 100.00, '2020-01-01', '2020-01-02', 'Next day repair, after driver shift, in house (vendor garage). Customer charged voluntarily for repair.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (6, 6, 'Front Passenger seat ripped by fare by objects in pants pockets', 50.00, 50.00, 100.00, '2020-01-01', '2020-01-02', 'Next day repair, after driver shift, in house (vendor garage). Customer charged voluntarily for repair.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (7, 7, 'Front Passenger seat ripped by fare by objects in pants pockets', 50.00, 50.00, 100.00, '2020-01-01', '2020-01-02', 'Next day repair, after driver shift, in house (vendor garage). Customer charged voluntarily for repair.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (8, 8, 'Rear bumper and trunk latch damaged from negligent motorist.', 987.00, 500.00, 1487.00, '2020-01-02', '2020-01-05', 'Rear bumper replaced and trunk door salvages with repair. New lock installed with identical key pattern. Legal action completed. Offender insurance (Farmers Auto) completed settlement payment for full amount.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (9, 9, 'Rear bumper and trunk latch damaged from negligent motorist.', 987.00, 500.00, 1487.00, '2020-01-02', '2020-01-05', 'Rear bumper replaced and trunk door salvages with repair. New lock installed with identical key pattern. Legal action completed. Offender insurance (Farmers Auto) completed settlement payment for full amount.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (10, 10, 'Rear bumper and trunk latch damaged from negligent motorist.', 987.00, 500.00, 1487.00, '2020-01-02', '2020-01-05', 'Rear bumper replaced and trunk door salvages with repair. New lock installed with identical key pattern. Legal action completed. Offender insurance (Farmers Auto) completed settlement payment for full amount.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (11, 11, 'Minor rear passenger side door damage (dent)', 0.00, 50.00, 50.00, '2020-01-02', '2020-01-03', 'Minor rear passenger side door damage from fare customer kicking door closed with hands full. No police report, incident settled by fare custom charge for dent repair. $100 payment of charged to customer cc voluntarily.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (12, 12, 'Minor rear driver side door damage (dent)', 0.00, 50.00, 50.00, '2020-01-02', '2020-01-03', 'Minor rear driver side door damage from fare customer kicking door closed with hands full. No police report, incident settled by fare custom charge for dent repair. $100 payment of charged to customer cc voluntarily.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (13, 13, 'Minor front passenger side door damage (dent)', 0.00, 50.00, 50.00, '2020-01-02', '2020-01-03', 'Minor rear driver side door damage from fare customer kicking door closed with hands full. No police report, incident settled by fare custom charge for dent repair. $100 payment of charged to customer cc voluntarily.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (14, 14, 'Rear Passenger seat ripped by fare by sharp objects in cargo', 50.00, 50.00, 100.00, '2020-01-04', '2020-01-04', 'Next day repair, after driver shift, in house (vendor garage). Customer disputes charges. Legal action pending for payment of seat repair.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (15, 15, 'Rear Passenger seat ripped by fare by sharp objects in cargo', 50.00, 50.00, 100.00, '2020-01-04', '2020-01-04', 'Next day repair, after driver shift, in house (vendor garage). Customer disputes charges. Legal action pending for payment of seat repair.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (16, 16, 'Rear Passenger seat ripped by fare by sharp objects in cargo', 50.00, 50.00, 100.00, '2020-01-04', '2020-01-04', 'Next day repair, after driver shift, in house (vendor garage). Customer disputes charges. Legal action pending for payment of seat repair.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (17, 17, 'Rear Passenger seat ripped by fare by sharp objects in cargo', 50.00, 50.00, 100.00, '2020-01-04', '2020-01-04', 'Next day repair, after driver shift, in house (vendor garage). Customer disputes charges. Legal action pending for payment of seat repair.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (18, 18, 'Passenger side view mirror cracked by vandal on bicycle.', 50.00, 25.00, 75.00, '2020-01-04', '2020-01-05', 'Next day repair, after driver shift, in house (vendor garage). Legal action pending for payment of repair.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (19, 19, 'Passenger side view mirror cracked by vandal on bicycle.', 50.00, 25.00, 75.00, '2020-01-04', '2020-01-05', 'Next day repair, after driver shift, in house (vendor garage). Legal action pending for payment of repair.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (20, 20, 'Passenger side view mirror cracked by vandal on bicycle.', 50.00, 25.00, 75.00, '2020-01-04', '2020-01-05', 'Next day repair, after driver shift, in house (vendor garage). Legal action pending for payment of repair.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (21, 21, 'Driver side collision from drunk driver failing to stop at a red light', 1521.00, 2500.00, 4021.00, '2020-01-05', '2020-01-12', 'Driver side front and rear door panels replaced and pillar repaired with frame straightening. Legal action completed. Offender insurance (State Farm Auto) completed settlement payment for full amount.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (22, 22, 'Passenger side collision from wreckless driver speeding.', 1241.00, 2500.00, 3741.00, '2020-01-05', '2020-01-08', 'Passenger side front and rear door panels replaced, no pillar damage. Legal action completed. Offender insurance (Geico Auto) completed settlement payment for full amount.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (23, 23, 'Passenger side collision from wreckless driver speeding.', 1441.00, 2500.00, 3941.00, '2020-01-05', '2020-01-08', 'Passenger side front and rear door panels replaced, no pillar damage. Legal action completed. Offender insurance (Geico Auto) completed settlement payment for full amount.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (24, 24, 'Drunken passenger vomited in cab. (Hazmat cleanup).', 50.00, 150.00, 200.00, '2020-01-06', '2020-01-07', 'Customer Fare vomit cleaning, charged standard cleanup fee of $300 to cc to avoid civil action.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (25, 25, 'Drunken passenger vomited in cab. (Hazmat cleanup).', 50.00, 150.00, 200.00, '2020-01-06', '2020-01-07', 'Customer Fare vomit cleaning, charged standard cleanup fee of $300 to cc to avoid civil action.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (26, 26, 'Drunken passenger vomited in cab. (Hazmat cleanup).', 50.00, 150.00, 200.00, '2020-01-06', '2020-01-07', 'Customer Fare vomit cleaning, charged standard cleanup fee of $300 to cc to avoid civil action.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (27, 27, 'Drunken passenger vomited in cab. (Hazmat cleanup).', 50.00, 150.00, 200.00, '2020-01-06', '2020-01-07', 'Customer Fare vomit cleaning, charged standard cleanup fee of $300 to cc to avoid civil action.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (28, 28, 'Drunken passenger vomited in cab. (Hazmat cleanup).', 50.00, 150.00, 200.00, '2020-01-07', '2020-01-08', 'Customer Fare vomit cleaning, charged standard cleanup fee of $300 to cc to avoid civil action.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (29, 29, 'Drunken passenger vomited in cab. (Hazmat cleanup).', 50.00, 150.00, 200.00, '2020-01-07', '2020-01-08', 'Customer Fare vomit cleaning, charged standard cleanup fee of $300 to cc to avoid civil action.', NULL, NULL);
INSERT INTO public."WorkOrders" VALUES (30, 30, 'Drunken passenger vomited in cab. (Hazmat cleanup).', 50.00, 150.00, 200.00, '2020-01-07', '2020-01-08', 'Customer Fare vomit cleaning, charged standard cleanup fee of $300 to cc to avoid civil action.', NULL, NULL);


--
-- TOC entry 2987 (class 0 OID 0)
-- Dependencies: 216
-- Name: WorkOrders_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."WorkOrders_id_seq"', 31, true);


-- Completed on 2021-01-13 11:42:59 EST

--
-- PostgreSQL database dump complete
--

