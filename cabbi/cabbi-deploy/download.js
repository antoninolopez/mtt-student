const path = require('path');
const { execSync } = require('child_process');

const cabbiDir = getParentDir();

const repos = [
  'ride-service',
  'api-gateway',
  'cabbi-graphql',
  'incident-report-service',
  'metadata-service',
  'reports-service',
  'ui',
  'vehicle-service',
  'work-order-service',
];

repos.forEach((repoName) => {
  cloneRepo(repoName, cabbiDir);
});

function getParentDir() {
  const splitDir = process.cwd().split(path.sep);
  splitDir.pop();
  return splitDir.join(path.sep);
}

function cloneRepo(repoName, cabbiDir) {
  execSync(
    `git clone git@gitlab.com:spathelab/cabbi-app/${repoName}.git ${cabbiDir}/${repoName}`,
    { encoding: 'utf-8' }
  );
}
