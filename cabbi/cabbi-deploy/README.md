# CABBI Deploy Tool

## Setup

Run `npm install` to install dependencies

## Base Command

The base command is:

`node deploy`

## Flags

`--include-secrets` - Includes secret deployment

`--only-secrets` - Only deploys the secrets

`--include-data` - Includes data deploy in automated pipeline

`--only-data` - Only deploys data - based on config in `.env` file
