const path = require('path');
const { spawn } = require('child_process');
const { logInfo, logError, logSuccess } = require('./helpers/logging');

module.exports = function deployApp(args) {
  return new Promise((resolve, reject) => {
    const targetDir = path.join(process.cwd(), 'k8s');

    const ocApply = spawn('oc', ['apply', '-f', targetDir, '--recursive']);
    ocApply.stdout.on('data', (data) => {
      logInfo(`${data}`);
    });
    ocApply.stderr.on('data', (data) => {
      logError(data.toString());
      reject(data);
    });
    ocApply.on('close', (code) => {
      logSuccess('Deploy Complete\n');
      resolve();
    });
  });
};
