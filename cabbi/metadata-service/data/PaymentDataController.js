/* eslint-disable no-restricted-syntax */
/* eslint-disable class-methods-use-this */
const { Pool } = require('pg');
const isInt = require('../helpers/isInt');
const { logError } = require('../logging/logger');

module.exports = class DataController {
  constructor() {
    this.connectionPool = new Pool({
      max: 25,
    });
  }

  getPaymentByID(id) {
    return new Promise((resolve, reject) => {
      this._executeQuery(`SELECT * FROM public."Payments" where id = $1;`, [id])
        .then((data) => {
          resolve(this._trimData(data.rows));
        })
        .catch(() => reject({ code: 500, message: 'Unable To Run Query' }));
    });
  }

  getAllPayments() {
    return new Promise((resolve, reject) => {
      const sqlQuery = `SELECT * FROM public."Payments";`;

      this._executeQuery(sqlQuery, [])
        .then((data) => {
          resolve(this._trimData(data.rows));
        })
        .catch(() => {
          reject({ code: 500, message: 'Unable to run Query' });
        });
    });
  }

  _executeQuery(query, parameterArr) {
    return new Promise((resolve, reject) => {
      this.connectionPool.connect().then((client) => {
        client
          .query(query, parameterArr)
          .then((response) => {
            resolve(response);
            client.release();
          })
          .catch((err) => {
            logError(err);
            reject(err);
            client.release();
          });
      });
    });
  }

  _trimData(data) {
    const trimmedData = [];

    data.forEach((element) => {
      const trimmedObject = {};

      Object.keys(element).forEach((key) => {
        if (typeof element[key] === 'string') {
          trimmedObject[key] = element[key].trim();
        } else {
          trimmedObject[key] = element[key];
        }
      });

      trimmedData.push(trimmedObject);
    });

    return trimmedData;
  }

  _convertQueryToSelectSQL(query, type) {
    let sqlQuery = '';

    let keyCount = 0;

    const parameterArr = [];

    for (const [key, value] of Object.entries(query)) {
      if (keyCount > 0) {
        if (type === 'SELECT') {
          sqlQuery += ' AND ';
        }

        if (type === 'UPDATE') {
          sqlQuery += ', ';
        }
      }

      // if (typeof value === 'string') {
      // TODO(TUCKER) - Clean up the logic in here
      // if (isInt(value)) {
      //   sqlQuery += `${key}=$${keyCount + 1}`;
      // } else {
      //     sqlQuery += `${key}='$${keyCount + 1}'`;
      //   }
      // } else {
      sqlQuery += `${key}=$${keyCount + 1}`;
      // }

      if (isInt(value)) {
        parameterArr.push(parseInt(value, 10));
      } else {
        parameterArr.push(value);
      }

      keyCount++;
    }

    return {
      sqlQuery,
      parameterArr,
      keyCount,
    };
  }

  _buildSQLInsert(data) {
    let keys = '';
    let values = '';

    let keyCount = 0;

    const parameterArr = [];

    for (const [key, value] of Object.entries(data)) {
      if (keyCount > 0) {
        keys += ', ';
        values += ', ';
      }

      keys += key;

      values += `$${keyCount + 1}`;

      if (isInt(value)) {
        parameterArr.push(parseInt(value, 10));
      } else {
        parameterArr.push(value);
      }

      keyCount++;
    }

    const sqlQuery = `(${keys}) VALUES (${values})`;

    return {
      sqlQuery,
      parameterArr,
      keyCount,
    };
  }
};
