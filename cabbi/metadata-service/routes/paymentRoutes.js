const express = require('express');
const PaymentController = require('../controller/PaymentController.js');
const { checkRoles } = require('../middleware/authCheck.js');

const isIDValid = require('../middleware/idCheck');

const router = express.Router();

module.exports = (keycloak) => {
  router.use(keycloak.middleware());

  const payCtl = new PaymentController();

  router.get('/:id', checkRoles(keycloak, null), isIDValid, (req, res) => {
    payCtl
      .getByID(req.params.id)
      .then((data) => {
        res.json(data);
      })
      .catch((err) => res.status(err.code).send(err.message));
  });

  router.get('/', checkRoles(keycloak, null), (req, res) => {
    payCtl
      .getAll()
      .then((data) => {
        res.json(data);
      })
      .catch((err) => res.status(err.code).send(err.message));
  });

  return router;
};
