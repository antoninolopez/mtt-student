const express = require('express');
const RateCodeController = require('../controller/RateCodeController.js');
const { checkRoles } = require('../middleware/authCheck.js');

const isIDValid = require('../middleware/idCheck');

const router = express.Router();

module.exports = (keycloak) => {
  router.use(keycloak.middleware());

  const rcCtl = new RateCodeController();

  router.get('/:id', checkRoles(keycloak, null), isIDValid, (req, res) => {
    rcCtl
      .getByID(req.params.id)
      .then((data) => {
        res.json(data);
      })
      .catch((err) => res.status(err.code).send(err.message));
  });

  router.get('/', checkRoles(keycloak, null), (req, res) => {
    rcCtl
      .getAll()
      .then((data) => {
        res.json(data);
      })
      .catch((err) => res.status(err.code).send(err.message));
  });

  return router;
};
