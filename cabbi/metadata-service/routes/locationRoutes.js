const express = require('express');
const LocationController = require('../controller/LocationController.js');

const { checkRoles } = require('../middleware/authCheck');

const isIDValid = require('../middleware/idCheck');

const router = express.Router();

module.exports = (keycloak) => {
  router.use(keycloak.middleware());

  const locCtrl = new LocationController();

  router.get('/:id', checkRoles(keycloak, null), isIDValid, (req, res) => {
    locCtrl
      .getByID(req.params.id)
      .then((data) => {
        res.json(data);
      })
      .catch((err) => res.status(err.code).send(err.message));
  });

  router.get('/', checkRoles(keycloak, null), (req, res) => {
    locCtrl
      .getAll()
      .then((data) => {
        res.json(data);
      })
      .catch((err) => res.status(err.code).send(err.message));
  });

  return router;
};
