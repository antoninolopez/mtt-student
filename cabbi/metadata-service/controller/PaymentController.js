/* eslint-disable no-restricted-syntax */
const DataController = require('../data/PaymentDataController');

module.exports = class PaymentController {
  constructor() {
    this.db = new DataController();

    this.validQueryKeys = ['id', 'borough', 'zone', 'service_zone'];
  }

  getByID(id) {
    return new Promise((resolve, reject) => {
      this.db
        .getPaymentByID(id)
        .then((data) => resolve(data))
        .catch((err) => reject(err));
    });
  }

  getAll() {
    return new Promise((resolve, reject) => {
      this.db
        .getAllPayments()
        .then((data) => {
          resolve(data);
        })
        .catch((err) => reject(err));
    });
  }

  _isQueryValid(query, allRequired) {
    let keyLength = 0;

    for (const [key] of Object.entries(query)) {
      if (!this.validQueryKeys.includes(key)) {
        return false;
      }

      keyLength++;
    }

    if (allRequired) {
      if (keyLength !== this.validQueryKeys.length) {
        return false;
      }
    }

    return true;
  }
};
