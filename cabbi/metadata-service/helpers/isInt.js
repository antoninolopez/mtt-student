module.exports = function isInt(str) {
  if (typeof str !== 'string') return false; // we only process strings!

  if (str.includes('.')) return false;

  return !Number.isNaN(str) && !Number.isNaN(parseInt(str, 10));
};
