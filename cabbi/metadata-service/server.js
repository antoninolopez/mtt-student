// Node Imports
const os = require('os');
const cluster = require('cluster');

// Express Imports
const express = require('express');

const app = express();

// Middleware
const morgan = require('morgan');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const cors = require('cors');
const { logError, logInfo } = require('./logging/logger');

// Server Data
const PORT = process.env.PORT || 4550;

// Routes
const locationRoutes = require('./routes/locationRoutes');
const paymentRoutes = require('./routes/paymentRoutes');
const rateCodeRoutes = require('./routes/rateCodeRoutes');

function registerEnvironmentVariables() {
  if (process.env.NODE_ENV !== 'production') {
    // eslint-disable-next-line global-require
    require('dotenv').config();
  }
}

registerEnvironmentVariables();

// Init Keycloak
require('./config/keycloak-config.js')
  .initKeycloak()
  .then((keycloak) => {
    function registerMiddleware() {
      app.use(helmet());
      app.use(express.json());
      app.use(express.urlencoded({ extended: false }));
      app.use(cors());
    }

    function registerRoutes() {
      app.get('/metadata/status', (_, res) => {
        res.status(200).json({ service: 'metadata-service', status: 'green' });
      });

      app.use('/metadata/locations', locationRoutes(keycloak));
      app.use('/metadata/payments', paymentRoutes(keycloak));
      app.use('/metadata/ratecodes', rateCodeRoutes(keycloak));
    }

    function getNumWorkers() {
      return os.cpus().length;
    }

    function forkWorkers() {
      for (let i = 0; i < getNumWorkers(); i++) {
        cluster.fork();
      }

      cluster.on('exit', (worker) => {
        logInfo(`Worker ${worker.id} has exited`);
      });
    }

    function startWorker() {
      registerRoutes(app);

      app.listen(PORT, () => {
        logInfo(`Worker ${process.pid} \tListening on port ${PORT}`);
      });
    }

    function isRunningMasterProcess() {
      if (cluster.isMaster) {
        forkWorkers();
      } else {
        startWorker();
      }
    }

    function runApp() {
      if (getNumWorkers() > 1) {
        isRunningMasterProcess();
      } else {
        startWorker();
      }
    }

    function startLogging() {
      if (process.env.NODE_ENV !== 'production') {
        app.use(
          morgan((tokens, req, res) => {
            const loggingOptions = [
              tokens.method(req, res),
              tokens.url(req, res),
              tokens.status(req, res),
              tokens.res(req, res, 'content-length'),
              '-',
              tokens['response-time'](req, res),
              'ms',
              '-',
              process.pid,
            ].join(' ');
            return loggingOptions;
          }),
        );
      } else {
        app.use(morgan('combined'));
      }
    }

    function setProcessWatchers() {
      process.on('SIGTERM', () => {
        process.exit();
      });

      process.on('SIGTERM', () => {
        process.exit();
      });
    }

    // Start Server Sequence
    setProcessWatchers();

    startLogging();

    registerMiddleware();

    runApp();
  })
  .catch((err) => {
    logError('Cannot Start Server');
    logError(err);
  });
