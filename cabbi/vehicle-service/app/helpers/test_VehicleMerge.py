from helpers.VehicleMerge import merge_vehicle_data

OLD_DATA= {
    'id': 50,
    'vendor_id': 1,
    'year': 2016,
    'make': 'Chevrolet',
    'model': 'Impala',
    'license_plate': 'NY-B17342',
    'max_passengers': 4,
    'status': False
    }

NEW_DATA = {
    'status': True
}

    
# Tests if status updates when provided in new_data
def test_status_update():
    merged_data = merge_vehicle_data(OLD_DATA, NEW_DATA)

    assert merged_data['status'] == True

# This test fails
# def test_no_args():
#     merged_data = merge_vehicle_data(OLD_DATA, 'Im a string')

#     assert merged_data == None
    
