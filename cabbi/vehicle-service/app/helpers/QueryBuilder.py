# from helpers.Formatter import format_query_param
from flask import abort

acceptable_query_params = [
  'id',
  'vendor_id',
  'year',
  'make',
  'model',
  'license_plate',
  'max_passengers',
  'status',
]

def build_query(query_string):
  query = {}

  split_query = query_string.decode('utf-8').split('&')

  for element in split_query:

    split_element = element.split('=')

    if split_element[0] in acceptable_query_params:

      if split_element[0] == 'make' or split_element[0] == 'model':
        query[split_element[0]] = split_element[1].title()

      elif split_element[0] == 'license_plate':
        query[split_element[0]] = split_element[1].upper()
        
      else:
        query[split_element[0]] = split_element[1]

    
    else:
      abort(400, 'Key {} not allowed in query'.format(split_element[0]))

  return query