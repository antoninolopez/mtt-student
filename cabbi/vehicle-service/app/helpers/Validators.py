def validate_new_data(new_data, require_all_to_be_present=False):
  valid_keys = ['vendor_id',
                'year',
                'make',
                'model',
                'license_plate',
                'max_passengers',
                'status'
                ]

  valid_key_check = valid_keys

  is_data_valid = True

  for key in new_data:
    if key not in valid_keys:
      return False
    valid_key_check.remove(key)

  if require_all_to_be_present:
    if not valid_key_check:
      is_data_valid = True
    else:
      return False

  return is_data_valid

def validate_id(id):
  if type(id) is int:
    return True
  else:
    return False
