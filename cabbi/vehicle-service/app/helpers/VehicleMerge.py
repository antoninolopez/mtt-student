def merge_vehicle_data(old_data, new_data):

  merged_data = old_data

  for key in new_data:
    merged_data[key] = new_data[key]

  return merged_data
