import os
import requests
from flask import abort, request
import jwt


def authenticate():
  if os.getenv('FLASK_ENV') == 'development':
    try:
      bearer_token = request.headers['Authorization'].split(' ')[1]

      if (os.getenv('FLASK_ENV') == 'development'):
        request_URL='http://{0}:{1}/auth/realms/CABBI'.format(os.getenv('KEYCLOAK_HOST'), os.getenv('KEYCLOAK_PORT'))
      else:
        request_URL='https://{0}/auth/realms/CABBI'.format(os.getenv('KEYCLOAK_HOST'))

      pk_request = requests.get(request_URL, verify=False)

      public_key = "-----BEGIN PUBLIC KEY-----\n{}\n-----END PUBLIC KEY-----".format(pk_request.json()['public_key'])

      decoded_token =jwt.decode(bearer_token, public_key, algorithms=["RS256"], flush=True, audience=["work-order-service", "production", "incident-report-service", "account"])

      return decoded_token

    except:
      abort(401, 'Token Not Valid')