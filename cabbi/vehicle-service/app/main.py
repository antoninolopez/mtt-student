import logging
from flask import Flask, jsonify, abort, request
from flask.wrappers import Response
from flask_sqlalchemy import SQLAlchemy
import json
import uuid
import os
import requests
from flask_cors import CORS
from flask_restful import Api

from resources.Vehicle import Vehicle
from resources.VehicleQuery import VehicleQuery
from db import db

if os.getenv('FLASK_ENV') == 'development':
  from dotenv import load_dotenv
  load_dotenv()

app = Flask(__name__)

# app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv('DATABASE_URL')
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://{0}:{1}@{2}:{3}/{4}'.format(os.getenv('PG_USERNAME'), os.getenv('PG_PASSWORD'), os.getenv('PG_HOST'), os.getenv('PG_PORT'), os.getenv('PG_DATABASE'))
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

logging.basicConfig(level=logging.DEBUG)


db.init_app(app)

CORS(app)

api = Api(app)

@app.route('/vehicle/status')
def heartbeat():
  return {'service': 'vehicle-service', 'status': 'green'}

api.add_resource(VehicleQuery, '/vehicle')
api.add_resource(Vehicle, '/vehicle/<int:id>')

if __name__ == "__main__":
  app.run(host="0.0.0.0")