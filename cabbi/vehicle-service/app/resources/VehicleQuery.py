from models.Vehicle import VehicleModel
from flask_restful import Resource
from flask import request, abort
from authentication.Authentication import authenticate
from helpers.QueryBuilder import build_query
from helpers.Validators import validate_new_data


class VehicleQuery(Resource):

  def __init__(self):
    self.user = authenticate()
    pass

  def get(self):
    query = build_query(request.query_string)

    query_return = VehicleModel.find_by_query(query)

    return query_return, 200

  def post(self):
    data = request.json

    is_data_valid = validate_new_data(data, require_all_to_be_present=True)

    if not is_data_valid:
      abort(422, 'Data Not Valid')

    data['id'] = None
    
    vehicle = VehicleModel(**data)

    try:
      db_result = vehicle.save_to_db()
    except:
      return {'message': 'Unable to Save To DB'}, 500

    return vehicle.json()