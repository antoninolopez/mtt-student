from models.Vehicle import VehicleModel
from helpers.VehicleMerge import merge_vehicle_data
from helpers.Validators import validate_id, validate_new_data
from flask_restful import Resource
from flask import request, abort
from authentication.Authentication import authenticate
class Vehicle(Resource):

  def __init__(self):
    self.user = authenticate()
    pass

  def get(self, id):
    is_id_valid = validate_id(id)

    if not is_id_valid:
      abort(422,'Data Not Formatted Correctly')

    vehicle = VehicleModel.find_by_id(id)

    if vehicle != None:
      return vehicle.json(), 200
    else:
      return {'message': 'Unable to Find Vehicle {}'.format(id)}, 404
  
  def put(self, id):
    update_data = request.json

    is_data_valid = validate_new_data(update_data)

    if not is_data_valid:
      abort(422, 'Data Not Formatted Correctly')

    vehicle = VehicleModel.find_by_id(id)

    if vehicle:
      updated_vehicle = VehicleModel(**vehicle.json())

      merged_data = merge_vehicle_data(updated_vehicle.json(), update_data)

      updated_vehicle.update_entry(merged_data)

      return merged_data

    else:
      return { 'message': 'vehicle not found' }, 404


  def delete(self, id):
    is_id_valid = validate_id(id)

    if not is_id_valid:
      abort(422,'Data Not Formatted Correctly')

    vehicle = VehicleModel.find_by_id(id)

    if vehicle:
      vehicle.delete_from_db()
      return {'message': 'vehicle {} deleted'.format(id)}

    else:
      return {'message': 'vehicle not found'}, 404