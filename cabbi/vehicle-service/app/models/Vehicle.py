from db import db
from sqlalchemy import Sequence

class VehicleModel(db.Model):
    __tablename__ = 'Vehicles'

    id = db.Column(db.Integer, primary_key=True)
    vendor_id = db.Column(db.Integer)
    year = db.Column(db.Integer)
    make = db.Column(db.String(50))
    model = db.Column(db.String(50))
    license_plate = db.Column(db.String(50))
    max_passengers = db.Column(db.Integer)
    status = db.Column(db.Boolean)

    def __init__(self, id, vendor_id, year, make, model, license_plate, max_passengers, status):

        # TODO(TUCKER) - Make sure the db id here can't be stolen by a concurrent write
        if id == None:
            self.id = VehicleModel.get_next_id()
        else:
            self.id = id

        self.vendor_id = vendor_id
        self.year = year
        self.make = make.title()
        self.model = model.title()
        self.license_plate = license_plate.upper()
        self.max_passengers = max_passengers
        self.status = status

    def __repr__(self):
        return '<id {0}>'.format(self.id)

    def json(self):
        return {
            'id': self.id,
            'vendor_id': self.vendor_id,
            'year': self.year,
            'make': self.make.strip(),
            'model': self.model.strip(),
            'license_plate': self.license_plate.strip(),
            'max_passengers': self.max_passengers,
            'status': self.status
        }

    @classmethod
    def get_next_id(cls):
        db_session = db.session.execute('SELECT MAX(id) FROM "Vehicles";')
        db_last_id_json = {'result': [dict(row) for row in db_session]}

        db_last_id = int(db_last_id_json['result'][0]['max'])

        return db_last_id + 1

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

    @classmethod
    def find_by_query(cls, query):

        return_data = {'data': []}
        for entry in cls.query.filter_by(**query).all():
            return_data['data'].append(entry.json())

        return return_data

    def save_to_db(self):
        db.session.add(self)
        return db.session.commit()

    def update_entry(self, new_data):
        VehicleModel.query.filter_by(id=self.id).update(new_data)
        return db.session.commit()

    def delete_from_db(self):
        db.session.delete(self)
        return db.session.commit()



