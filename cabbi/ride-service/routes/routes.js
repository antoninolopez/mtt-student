const { Router } = require('express');

const router = Router();

const RideController = require('../controllers/RideController');
const { checkRoles } = require('../middleware/authCheck');

function _getFilters(req) {
  const returnData = req.query;

  // TODO(TUCKER) - Add more of these in there
  if (returnData.PULocationID) {
    returnData.PULocationID = parseInt(returnData.PULocationID, 10);
  }

  return returnData;
}

module.exports = (keycloak) => {
  router.use(keycloak.middleware());

  const rideController = new RideController();

  router.get(
    '/reportByVehicleID/:vehicleID',
    checkRoles(keycloak, null),
    (req, res) => {
      rideController
        .getRidesByVehicleID(req.params.vehicleID)
        .then((data) => {
          res.json(data);
        })
        .catch((rejectData) => {
          res.status(rejectData.code).json({ message: rejectData.message });
        });
    },
  );

  router.get('/', checkRoles(keycloak, null), (req, res) => {
    const filters = _getFilters(req);

    rideController
      .getRides(filters)
      .then((data) => {
        res.status(200).json({ message: 'Found Rides', data });
      })
      .catch((rejectData) => {
        res.status(rejectData.code).json({ message: rejectData.message });
      });
  });

  router.post('/', checkRoles(keycloak, null), (req, res) => {
    const rideData = req.body;

    rideController
      .createRide(rideData)
      .then((data) => {
        res.status(201).json({ message: 'Ride Created', data });
      })
      .catch((rejectData) => {
        res.status(rejectData.code).json({ message: rejectData.message });
      });
  });

  router.get('/:id', checkRoles(keycloak, null), (req, res) => {
    rideController
      .getRideByID(req.params.id)
      .then((data) => {
        res.status(200).json({ message: 'Found Ride', data });
      })
      .catch((rejectData) => {
        res.status(rejectData.code).json({ message: rejectData.message });
      });
  });

  router.put('/:id', checkRoles(keycloak, null), (req, res) => {
    rideController
      .updateRide(req.params.id, req.body)
      .then((data) => {
        res.status(200).json({ message: 'Ride Updated', data });
      })
      .catch((rejectData) => {
        res.status(rejectData.code).json({ message: rejectData.message });
      });
  });

  router.delete('/:id', checkRoles(keycloak, null), (req, res) => {
    rideController
      .deleteRide(req.params.id)
      .then(() => {
        res.json({
          code: 200,
          message: 'Ride Deleted',
          _id: req.params.id,
        });
      })
      .catch((rejectData) => {
        res.status(rejectData.code).json({ message: rejectData.message });
      });
  });

  return router;
};
