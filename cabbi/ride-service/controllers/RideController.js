/* eslint-disable class-methods-use-this */
/* eslint-disable no-restricted-syntax */
const DataController = require('./DataController');

module.exports = class RideController {
  constructor() {
    if (process.env.NODE_ENV !== 'test') {
      this.dataController = new DataController();
    }

    this.dataKeys = [
      'VendorID',
      'tpep_pickup_datetime',
      'tpep_dropoff_datetime',
      'passenger_count',
      'trip_distance',
      'RatecodeID',
      'store_and_fwd_flag',
      'PULocationID',
      'DOLocationID',
      'payment_type',
      'fare_amount',
      'extra',
      'mta_tax',
      'tip_amount',
      'tolls_amount',
      'improvement_surcharge',
      'total_amount',
      'congestion_surcharge',
    ];

    this.intKeys = [
      'VendorID',
      'passenger_count',
      'trip_distance',
      'RatecodeID',
      'PULocationID',
      'DOLocationID',
      'payment_type',
    ];

    this.moneyKeys = [
      'fare_amount',
      'extra',
      'mta_tax',
      'tip_amount',
      'tolls_amount',
      'improvement_surcharge',
      'total_amount',
      'congestion_surcharge',
    ];
  }

  /**
   * Searches DB for target ID
   * @param {string} targetID - ID to search DB for
   */
  getRideByID(targetID) {
    return new Promise((resolve, reject) => {
      const idValid = this._isIdValid(targetID);

      if (!idValid) {
        reject({ code: 405, message: 'Invalid ID format' });
      }

      this.dataController
        .getRideByID(targetID)
        .then((record) => {
          resolve(record);
        })
        .catch((rejectData) => {
          reject(rejectData);
        });
    });
  }

  /**
   * Gets rides based on filter set
   * @param {Object} filters - Filters for data
   */
  getRides(filters) {
    return new Promise((resolve, reject) => {
      const filtersValid = this._areFiltersValid();

      if (!filtersValid) {
        reject({ code: 405, message: 'Invalid Filters' });
      }

      this.dataController
        .getRides(filters)
        .then((record) => {
          resolve(record);
        })
        .catch((rejectData) => {
          reject(rejectData);
        });
    });
  }

  getRidesByVehicleID(vehicleID) {
    return new Promise((resolve, reject) => {
      this.dataController
        .getRidesByVehicleID(vehicleID)
        .then((records) => resolve(records))
        .catch((err) => reject(err));
    });
  }

  /**
   * Puts valid ride data int db
   * @param {Ride} rideData - Ride data to put into database
   */
  createRide(rideData) {
    return new Promise((resolve, reject) => {
      const dataValid = this._hasValidObjectKeys(rideData);

      if (!dataValid) {
        reject({ code: 405, message: 'Invalid POST data' });
      }

      const formattedRideData = this._formatNewRideData(rideData);

      this.dataController
        .createRide(formattedRideData)
        .then((record) => {
          resolve(record);
        })
        .catch((rejectData) => {
          reject(rejectData);
        });
    });
  }

  /**
   * Updates ride data by id
   * @param {Ride} updateData - All or part of the Ride Data
   */
  updateRide(id, rideUpdate) {
    return new Promise((resolve, reject) => {
      const dataValid = this._areUpdateFieldsValid(rideUpdate);

      if (!dataValid) {
        reject({ code: 405, message: 'Invalid POST data' });
      }

      const formattedRideData = this._formatNewRideData(rideUpdate);

      this.dataController
        .updateRide(id, formattedRideData)
        .then((record) => {
          resolve(record);
        })
        .catch((rejectData) => {
          reject(rejectData);
        });
    });
  }

  /**
   * Delete Record ID
   * @param {string} id - Record ID to delete
   */
  deleteRide(targetID) {
    return new Promise((resolve, reject) => {
      const idValid = this._isIdValid(targetID);

      if (!idValid) {
        reject({ code: 405, message: 'Invalid ID Formats' });
      }

      this.dataController
        .deleteRide(targetID)
        .then((deleteData) => {
          resolve(deleteData);
        })
        .catch((rejectData) => {
          reject(rejectData);
        });
    });
  }

  /**
   * Returns boolean for validity of ID
   * @param {string} targetID - ID To Find in DB
   */
  _isIdValid(targetID) {
    if (typeof targetID !== 'string') {
      return false;
    }

    if (targetID.length !== 24) {
      return false;
    }

    // Needs the {24} since we have to match the whole string, not just chunks of it
    const re = /[0-9A-Fa-f]{24}/g;

    const hexTest = re.test(targetID);

    if (!hexTest) {
      return false;
    }

    return true;
  }

  /**
   * Determines if values provided are valid
   * @param {Object} filters - Object containing filters
   */
  _areFiltersValid() {
    // TODO(TUCKER) - Filter these values
    return true;
  }

  /**
   *
   * @param {Object} rideData - Data to create ride
   */
  _formatNewRideData(rideData) {
    const formattedRideData = {};

    for (const [key, value] of Object.entries(rideData)) {
      if (this.intKeys.includes(key)) {
        formattedRideData[key] = parseInt(value, 10);
      } else if (this.moneyKeys.includes(key)) {
        formattedRideData[key] = parseFloat(value) * 100;
      } else {
        formattedRideData[key] = value;
      }
    }

    return formattedRideData;
  }

  /**
   * Checks that all required parameters are present, and there are no extra parameters
   * @param {Object} rideData - object containing ride data
   */
  _hasValidObjectKeys(rideData) {
    const isInList = this._checkDataFieldsAreInList(rideData);

    // TODO (TUCKER) - Right now this checks one way, make it check both ways before returning true
    return isInList;
  }

  _areUpdateFieldsValid(updateData) {
    const isInList = this._checkDataFieldsAreInList(updateData);

    return isInList;
  }

  _checkDataFieldsAreInList(dataToValidate) {
    for (const [key, value] of Object.entries(dataToValidate)) {
      if (!this.dataKeys.includes(key)) {
        return false;
      }

      if (value === null || value === '' || value === undefined) {
        return false;
      }
    }

    return true;
  }
};
