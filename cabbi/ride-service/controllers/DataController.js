/* eslint-disable no-param-reassign */
const { MongoClient, ObjectID } = require('mongodb');
const { logError } = require('../logging/logger');

module.exports = class DataController {
  constructor() {
    try {
      new MongoClient(
        `mongodb+srv://${process.env.MONGO_USERNAME}:${process.env.MONGO_PASSWORD}@${process.env.MONGO_HOST}`,
        {
          useUnifiedTopology: true,
        },
      ).connect((err, client) => {
        if (err) throw err;

        this.db = client
          .db(process.env.MONGO_DB)
          .collection(process.env.MONGO_COLLECTION);
      });
    } catch (err) {
      logError(err);
    }
  }

  /**
   * Searches DB for targetID
   * @param {String} targetID - ID to search db for
   */
  getRideByID(targetID) {
    return new Promise((resolve, reject) => {
      this.db
        .findOne({ _id: ObjectID(targetID) })
        .then((record) => {
          if (record === null) {
            reject({ code: 404, message: 'Unable to find Record' });
          }
          resolve(record);
        })
        .catch(() => {
          reject({ code: 500, message: 'Unable to Get Records' });
        });
    });
  }

  /**
   * Queries Database based on filters applied
   * @param {Object} filterList - List of filters to apply to the query
   */
  getRides(filterList) {
    return new Promise((resolve, reject) => {
      try {
        if (filterList.limitCount > 100) {
          filterList.limitCount = 100;
        }

        if (filterList.limitCount < 0) {
          filterList.limitCount = 0;
        }

        const targetPage = parseInt(filterList.page, 10);
        const limitCount = parseInt(filterList.limitCount, 10);

        delete filterList.page;
        delete filterList.limitCount;

        const records = this.db
          .find(filterList)
          .sort({ _id: 1 })
          .skip(targetPage * limitCount)
          .limit(limitCount);
        resolve(records.toArray());
      } catch (rejectData) {
        reject(rejectData);
      }
    });
  }

  getRidesByVehicleID(vehicleID) {
    return new Promise((resolve, reject) => {
      try {
        const records = this.db.find({ vehicle_id: parseInt(vehicleID, 10) });
        resolve(records.toArray());
      } catch (rejectData) {
        reject(rejectData);
      }
    });
  }

  /**
   *
   * @param {Ride} newRide - Data For New Ride
   */
  createRide(rideData) {
    return new Promise((resolve, reject) => {
      this.db
        .insertOne(rideData)
        .then((data) => {
          resolve({
            ...rideData,
            _id: data.insertedId,
          });
        })
        .catch(() => {
          reject({ code: 500, message: 'Unable To Create Record' });
        });
    });
  }

  /**
   *
   * @param {Ride} newRide - Data For New Ride
   */
  updateRide(id, updateData) {
    return new Promise((resolve, reject) => {
      const filter = { _id: ObjectID(id) };

      const update = {
        $set: updateData,
      };

      this.db
        .updateOne(filter, update)
        .then(() => {
          this.getRideByID(id)
            .then((data) => {
              resolve(data);
            })
            .catch(() => {
              reject({
                code: 500,
                message: `Unable to validate update - unable to get record ${id}`,
              });
            });
        })
        .catch(() => {
          reject({ code: 500, message: `Unable To Update Record ${id}` });
        });
    });
  }

  deleteRide(targetID) {
    return new Promise((resolve, reject) => {
      this.db
        .deleteOne({ _id: ObjectID(targetID) })
        .then((data) => {
          if (data.deletedCount > 0) {
            resolve(data);
          } else {
            reject({ code: 404, message: `Unable to Find Record ${targetID}` });
          }
        })
        .catch(() => {
          reject({ code: 500, message: `Unable To Delete Record ${targetID}` });
        });
    });
  }
};
