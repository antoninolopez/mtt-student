// NODE IMPORTS
const os = require('os');
const cluster = require('cluster');

// EXPRESS IMPORTS
const express = require('express');

const app = express();

// MIDDLEWARE
const bodyParser = require('body-parser');
const morgan = require('morgan');
const helmet = require('helmet');
const cors = require('cors');
const { logError, logInfo } = require('./logging/logger');

// ROUTES
const routes = require('./routes/routes');

// Server Data
const PORT = process.env.PORT || 3000;

function registerEnvironmentVariables() {
  if (process.env.NODE_ENV !== 'production') {
    // eslint-disable-next-line global-require
    require('dotenv').config();
  }
}

registerEnvironmentVariables();

require('./config/keycloak-config.js')
  .initKeycloak()
  .then((keycloak) => {
    /**
     * Helper Functions
     */
    function registerMiddleware() {
      app.use(helmet());
      app.use(bodyParser.json());
      app.use(bodyParser.urlencoded({ extended: false }));
      app.use(cors());
    }

    function getNumWorkers() {
      return os.cpus().length;
    }

    function registerRoutes() {
      app.use('/ride/status', (req, res) => {
        res.json({ service: 'ride-service', status: 'green-status' });
      });

      app.use('/ride', routes(keycloak));
    }

    function startWorker() {
      registerRoutes(app);

      app.listen(PORT, () => {
        logInfo(`Worker ${process.pid} \tListening on port ${PORT}`);
      });
    }

    function forkWorkers() {
      for (let i = 0; i < getNumWorkers(); i++) {
        cluster.fork();
      }

      cluster.on('exit', (worker) => {
        logError(`Worker ${worker.id} has exited`);
      });
    }

    function isRunningMasterProcess() {
      if (cluster.isMaster) {
        forkWorkers();
      } else {
        startWorker();
      }
    }

    function runApp() {
      if (getNumWorkers() > 1) {
        isRunningMasterProcess();
      } else {
        startWorker();
      }
    }

    function startLogging() {
      if (process.env.NODE_ENV !== 'production') {
        app.use(
          morgan((tokens, req, res) => {
            const loggingOptions = [
              tokens.method(req, res),
              tokens.url(req, res),
              tokens.status(req, res),
              tokens.res(req, res, 'content-length'),
              '-',
              tokens['response-time'](req, res),
              'ms',
              '-',
              process.pid,
            ].join(' ');
            return loggingOptions;
          }),
        );
      } else {
        app.use(morgan('combined'));
      }
    }

    function setProcessWatchers() {
      process.on('SIGTERM', () => {
        process.exit();
      });

      process.on('SIGTERM', () => {
        process.exit();
      });
    }

    setProcessWatchers();

    startLogging();

    registerMiddleware();

    runApp();
  })
  .catch((err) => {
    logError('Cannot Start Server');
    logError(err);
  });
