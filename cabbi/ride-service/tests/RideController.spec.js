const { describe, expect, test } = require('@jest/globals');
const RideController = require('../controllers/RideController');
require('dotenv').config();

describe('new RideController()', () => {
  test('Ride controller Is Created', () => {
    const rideController = new RideController();
    expect(typeof rideController).toBe('object');
  });
});

describe('RideController -> _isIdValid()', () => {
  test('Should return false if not a string', () => {
    const rideController = new RideController();
    expect(rideController._isIdValid(1)).toBe(false);
    expect(rideController._isIdValid({ example: 'example' })).toBe(false);
    expect(rideController._isIdValid(true)).toBe(false);
  });

  test('Should return false if not 24 characters long', () => {
    const rideController = new RideController();
    expect(rideController._isIdValid('asdf')).toBe(false);
    expect(
      rideController._isIdValid('asdfasdfasdfasdfasdfasdfsadfasdfsdaf'),
    ).toBe(false);
  });

  test('Should return false if not hexadecimal', () => {
    const rideController = new RideController();
    expect(rideController._isIdValid('5fb66d780860a7944321239z')).toBe(false);
  });

  test('Should succeed if given a 24 character long hex string', () => {
    const rideController = new RideController();
    expect(rideController._isIdValid('5fb66d780860a79443212392')).toBe(true);
  });
});

describe('RideController -> _areFiltersValid()', () => {
  expect(true).toBe(true);
});

describe('RideController -> _formatNewRideData()', () => {});
describe('RideController -> _hasValidObjectKeys()', () => {});
