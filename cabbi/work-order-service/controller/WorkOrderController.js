const DataController = require('../data/DataController');
const { logError } = require('../logging/logger');

module.exports = class WorkOrderController {
  constructor() {
    this.db = new DataController();

    this.validQueryKeys = [
      'incident_id',
      'work_description',
      'material_cost',
      'labor_cost',
      'final_total_costs',
      'start_date',
      'end_date',
      'additional_details',
      'approved_by',
      'approval_status',
    ];
  }

  create(newData) {
    return new Promise((resolve, reject) => {
      const queryValid = this.isQueryValid(newData, true);

      if (!queryValid) {
        reject({ code: 400, message: 'Data Not Valid' });
      }

      this.db
        .createWorkOrder(newData)
        .then((data) => {
          if (data.rowCount > 0) {
            resolve('Created Work Order');
          } else {
            reject({ code: 500, message: 'Unable to create Work Order' });
          }
        })
        .catch((err) => reject(err));
    });
  }

  approve(id, user) {
    return this.db.approveWorkOrder(id, user);
  }

  getByID(id) {
    return new Promise((resolve, reject) => {
      this.db
        .getWorkOrderByID(id)
        .then((data) => resolve(data))
        .catch((err) => reject(err));
    });
  }

  getByQuery(query) {
    return new Promise((resolve, reject) => {
      const queryValid = this.isQueryValid(query, false);

      if (!queryValid) {
        reject({ code: 400, message: 'Data Not Valid' });
      }

      this.db
        .getWorkOrdersByQuery(query)
        .then((data) => {
          resolve(data);
        })
        .catch((err) => reject(err));
    });
  }

  getOpenWOs() {
    return new Promise((resolve, reject) => {
      this.db
        .getOpenWorkOrders()
        .then((data) => {
          resolve(data.rows);
        })
        .catch((err) => {
          logError(err);
          reject(err);
        });
    });
  }

  deleteByID(id) {
    return new Promise((resolve, reject) => {
      this.db
        .deleteWorkOrderByID(id)
        .then((data) => {
          if (data.rowCount === 0) {
            reject({ code: 404, message: 'Unable to Find Resource To Delete' });
          } else {
            resolve(`Deleted ${data.rowCount} entity.`);
          }
        })
        .catch((err) => reject(err));
    });
  }

  updateByID(id, updateData) {
    return new Promise((resolve, reject) => {
      this.db
        .updateWorkOrderByID(id, updateData)
        .then((data) => {
          if (data.rowCount > 0) {
            resolve(`Updated ${data.rowCount} row(s)`);
          } else {
            reject({
              code: 404,
              message: `Unable To Find Resource with id ${id}`,
            });
          }
        })
        .catch((err) => reject(err));
    });
  }

  isQueryValid(query, allRequired) {
    let keyLength = 0;

    // eslint-disable-next-line no-restricted-syntax
    for (const [key] of Object.entries(query)) {
      if (!this.validQueryKeys.includes(key)) {
        return false;
      }

      keyLength++;
    }

    if (allRequired) {
      if (keyLength !== this.validQueryKeys.length) {
        return false;
      }
    }

    return true;
  }
};
