module.exports = function isInt(str) {
  if (typeof str !== 'string') return false; // we only process strings!

  if (str.includes('.')) return false;

  return (
    // use type coercion to parse the entire (`parseFloat` alone does not do this)
    !Number.isNaN(str) && !Number.isNaN(parseInt(str, 10))
  );
};
