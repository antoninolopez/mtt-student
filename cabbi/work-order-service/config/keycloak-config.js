const Keycloak = require('keycloak-connect');
const { logInfo, logWarning, logError } = require('../logging/logger');
const getPublicKey = require('./getPublicKey');

let keycloak;

function initKeycloak() {
  return new Promise((resolve, reject) => {
    getPublicKey()
      .then((pubKey) => {
        let authURL;

        if (process.env.NODE_ENV === 'development') {
          authURL = `http://${process.env.KEYCLOAK_HOST}:${process.env.KEYCLOAK_PORT}/auth`;
        } else {
          authURL = `https://${process.env.KEYCLOAK_HOST}/auth`;
        }

        const keycloakConfig = {
          clientId: process.env.KEYCLOAK_CLIENT,
          bearerOnly: true,
          serverUrl: authURL,
          realm: process.env.KEYCLOAK_REALM,
          realmPublicKey: pubKey,
        };

        if (keycloak) {
          logWarning('Trying to init Keycloak again!');
          resolve(keycloak);
        }
        logInfo(`Initializing Keycloak for pid ${process.pid}`);
        keycloak = new Keycloak({}, keycloakConfig);
        resolve(keycloak);
      })
      .catch((err) => reject(err));
  });
}

function getKeycloak() {
  if (!keycloak) {
    logError('Keycloak has not been initialized. Please called init first.');
  }
  return keycloak;
}

module.exports = {
  initKeycloak,
  getKeycloak,
};
