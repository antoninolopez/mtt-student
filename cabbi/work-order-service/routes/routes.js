const express = require('express');
const WorkOrderController = require('../controller/WorkOrderController.js');
const { logInfo, logError } = require('../logging/logger.js');
const { checkRoles } = require('../middleware/authCheck.js');

const isIDValid = require('../middleware/idCheck');

const router = express.Router();

module.exports = (keycloak) => {
  router.use(keycloak.middleware());

  const woCtl = new WorkOrderController();

  router.post(
    '/:id/approve',
    checkRoles(keycloak, ['realm:admin', 'realm:approver']),
    isIDValid,
    (req, res) => {
      woCtl
        .approve(req.params.id, req.body)
        .then((updated) => {
          res.send(updated);
        })
        .catch((err) => {
          if (err === 'not found') {
            res.status(404).send('Resource Not Found');
          } else {
            res.status(500).send('And Error occured');
          }
        });
    },
  );

  router.get('/open', checkRoles(keycloak, null), (req, res) => {
    woCtl
      .getOpenWOs()
      .then((data) => {
        res.json(data);
      })
      .catch((err) => {
        logError(err);
        res.status(err.code).send(err.message);
      });
  });

  router.get('/:id', checkRoles(keycloak, null), isIDValid, (req, res) => {
    woCtl
      .getByID(req.params.id)
      .then((data) => {
        res.json(data);
      })
      .catch((err) => res.status(err.code).send(err.message));
  });

  router.put(
    '/:id',
    checkRoles(keycloak, 'realm:admin'),
    isIDValid,
    (req, res) => {
      woCtl
        .updateByID(req.params.id, req.body)
        .then((data) => {
          res.json(data);
        })
        .catch((err) => res.status(err.code).send(err.message));
    },
  );

  router.delete(
    '/:id',
    checkRoles(keycloak, 'realm:admin'),
    isIDValid,
    (req, res) => {
      woCtl
        .deleteByID(req.params.id)
        .then((data) => {
          res.send(data);
        })
        .catch((err) => res.status(err.code).send(err.message));
    },
  );

  router.post('/', checkRoles(keycloak, null), (req, res) => {
    woCtl
      .create(req.body)
      .then((data) => {
        res.status(201).send(data);
      })
      .catch((err) => res.status(err.code).send(err.message));
  });

  router.get('/', checkRoles(keycloak, null), (req, res) => {
    woCtl
      .getByQuery(req.query)
      .then((data) => {
        res.json(data);
      })
      .catch((err) => res.status(err.code).send(err.message));
  });

  return router;
};
