/* eslint-disable no-restricted-syntax */
const DataController = require('../data/DataController');

module.exports = class IncidentReportController {
  constructor() {
    this.db = new DataController();

    this.validQueryKeys = [
      'ride_id',
      'vehicle_id',
      'location_id',
      'description',
      'type',
      'police_report',
      'status',
      'incident_datetime',
    ];
  }

  create(newData) {
    return new Promise((resolve, reject) => {
      const queryValid = this._isQueryValid(newData, true);

      if (!queryValid) {
        reject({ code: 400, message: 'Data Not Valid' });
      }

      this.db
        .createIncidentReport(newData)
        .then((data) => {
          if (data.rowCount > 0) {
            resolve('Created Incident Report');
          } else {
            reject({ code: 500, message: 'Unable to create Incident Report' });
          }
        })
        .catch((err) => reject(err));
    });
  }

  getByID(id) {
    return new Promise((resolve, reject) => {
      this.db
        .getIncidentReportByID(id)
        .then((data) => resolve(data))
        .catch((err) => reject(err));
    });
  }

  getReport(id) {
    return new Promise((resolve, reject) => {
      this.db
        .getReport(id)
        .then((data) => resolve(data))
        .catch((err) => reject(err));
    });
  }

  getByQuery(query) {
    return new Promise((resolve, reject) => {
      const queryValid = this._isQueryValid(query, false);

      if (!queryValid) {
        reject({ code: 400, message: 'Data Not Valid' });
      }

      this.db
        .getIncidentReportsByQuery(query)
        .then((data) => {
          resolve(data);
        })
        .catch((err) => reject(err));
    });
  }

  deleteByID(id) {
    return new Promise((resolve, reject) => {
      this.db
        .deleteIncidentReportByID(id)
        .then((data) => {
          if (data.rowCount === 0) {
            reject({ code: 404, message: 'Unable to Find Resource To Delete' });
          } else {
            resolve(`Deleted ${data.rowCount} entity.`);
          }
        })
        .catch((err) => reject(err));
    });
  }

  updateByID(id, updateData) {
    return new Promise((resolve, reject) => {
      let formattedUpdateData;

      if (updateData.incident_datetime) {
        const rawDateTime = new Date(
          updateData.incident_datetime.replace('/', '-'),
        );
        const newDateTime = `${rawDateTime.getFullYear()}-${
          rawDateTime.getUTCMonth() + 1
        }-${rawDateTime.getUTCDate()} ${rawDateTime.getHours()}:${rawDateTime.getMinutes()}:${rawDateTime.getSeconds()}-${rawDateTime.getMilliseconds()}`;
        formattedUpdateData = {
          ...updateData,
          incident_datetime: newDateTime.toString(),
        };
      } else {
        formattedUpdateData = updateData;
      }

      this.db
        .updateIncidentReportByID(id, formattedUpdateData)
        .then((data) => {
          if (data.rowCount > 0) {
            resolve(`Updated ${data.rowCount} row(s)`);
          } else {
            reject({
              code: 404,
              message: `Unable To Find Resource with id ${id}`,
            });
          }
        })
        .catch((err) => reject(err));
    });
  }

  _isQueryValid(query, allRequired) {
    let keyLength = 0;

    for (const [key] of Object.entries(query)) {
      if (!this.validQueryKeys.includes(key)) {
        return false;
      }

      keyLength++;
    }

    if (allRequired) {
      if (keyLength !== this.validQueryKeys.length) {
        return false;
      }
    }

    return true;
  }
};
