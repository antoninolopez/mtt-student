const express = require('express');
const IncidentReportController = require('../controller/IncidentReportController.js');
const { checkRoles } = require('../middleware/authCheck.js');
const isIDValid = require('../middleware/idCheck');

const router = express.Router();

module.exports = function routes(keycloak) {
  router.use(keycloak.middleware());

  const irc = new IncidentReportController();

  router.get(
    '/:id/report',
    checkRoles(keycloak, null),
    isIDValid,
    (req, res) => {
      irc
        .getByID(req.params.id)
        .then((data) => {
          res.json(data);
        })
        .catch((err) => res.status(err.code).send(err.message));
    },
  );

  router.get('/:id', checkRoles(keycloak, null), isIDValid, (req, res) => {
    irc
      .getByID(req.params.id)
      .then((data) => {
        res.json(data);
      })
      .catch((err) => res.status(err.code).send(err.message));
  });

  router.put('/:id', checkRoles(keycloak, null), isIDValid, (req, res) => {
    irc
      .updateByID(req.params.id, req.body)
      .then((data) => {
        res.json(data);
      })
      .catch((err) => res.status(err.code).send(err.message));
  });

  router.delete('/:id', checkRoles(keycloak, null), isIDValid, (req, res) => {
    irc
      .deleteByID(req.params.id)
      .then((data) => {
        res.send(data);
      })
      .catch((err) => res.status(err.code).send(err.message));
  });

  router.post('/', checkRoles(keycloak, null), (req, res) => {
    irc
      .create(req.body)
      .then((data) => {
        res.status(201).send(data);
      })
      .catch((err) => res.status(err.code).send(err.message));
  });

  router.get('/', checkRoles(keycloak, null), (req, res) => {
    irc
      .getByQuery(req.query)
      .then((data) => {
        res.json(data);
      })
      .catch((err) => res.status(err.code).send(err.message));
  });

  return router;
};
