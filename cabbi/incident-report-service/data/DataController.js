const { Pool } = require('pg');

const {
  trimData,
  convertQueryToSelectSQL,
  buildSQLInsert,
} = require('./dataFunctions');
const { logError } = require('../logging/logger');

module.exports = class DataController {
  constructor() {
    this.connectionPool = new Pool({
      max: 25,
    });
  }

  getIncidentReportByID(id) {
    return new Promise((resolve, reject) => {
      this._executeQuery(
        `SELECT * FROM public."IncidentReports" where id = $1;`,
        [id],
      )
        .then((data) => {
          resolve(trimData(data.rows));
        })
        .catch(() => reject({ code: 500, message: 'Unable To Run Query' }));
    });
  }

  getReport(id) {
    return new Promise((resolve, reject) => {
      this._executeQuery(
        `SELECT * FROM public."Vehicles_IncidentReport_v" where id = $1;`,
        [id],
      )
        .then((data) => {
          resolve(trimData(data.rows));
        })
        .catch(() => reject({ code: 500, message: 'Unable To Run Query' }));
    });
  }

  createIncidentReport(newIR) {
    return new Promise((resolve, reject) => {
      const addSQL = buildSQLInsert(newIR);

      const sqlQuery = `INSERT INTO public."IncidentReports" ${addSQL.sqlQuery};`;

      this._executeQuery(sqlQuery, addSQL.parameterArr)
        .then((data) => {
          resolve(data);
        })
        .catch(() => {
          reject({ code: 500, message: 'Unable to run Query' });
        });
    });
  }

  getIncidentReportsByQuery(query) {
    return new Promise((resolve, reject) => {
      const selectSQL = convertQueryToSelectSQL(query, 'SELECT');

      const sqlQuery = `SELECT * FROM public."IncidentReports" WHERE ${selectSQL.sqlQuery};`;

      this._executeQuery(sqlQuery, selectSQL.parameterArr)
        .then((data) => {
          resolve(trimData(data.rows));
        })
        .catch(() => {
          reject({ code: 500, message: 'Unable to run Query' });
        });
    });
  }

  updateIncidentReportByID(id, updateData) {
    return new Promise((resolve, reject) => {
      const updateSQL = convertQueryToSelectSQL(updateData, 'UPDATE');

      const sqlQuery = `UPDATE public."IncidentReports" SET ${
        updateSQL.sqlQuery
      } WHERE id=$${updateSQL.keyCount + 1};`;

      const parameterArr = [...updateSQL.parameterArr, id];

      this._executeQuery(sqlQuery, parameterArr)
        .then((data) => {
          resolve(data);
        })
        .catch(() => {
          reject({ code: 500, message: 'Unable to run Query' });
        });
    });
  }

  deleteIncidentReportByID(id) {
    return new Promise((resolve, reject) => {
      this._executeQuery(`DELETE FROM public."IncidentReports" WHERE id=$1;`, [
        id,
      ])
        .then((data) => {
          resolve(data);
        })
        .catch(() => reject({ code: 500, message: 'Unable To Run Query' }));
    });
  }

  _executeQuery(query, parameterArr) {
    return new Promise((resolve, reject) => {
      this.connectionPool.connect().then((client) => {
        client
          .query(query, parameterArr)
          .then((response) => {
            resolve(response);
            client.release();
          })
          .catch((err) => {
            logError(err);
            reject(err);
            client.release();
          });
      });
    });
  }
};
