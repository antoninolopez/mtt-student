/* eslint-disable no-restricted-syntax */
const isInt = require('../helpers/isInt');

function trimData(data) {
  const trimmedData = [];

  data.forEach((element) => {
    const trimmedObject = {};

    Object.keys(element).forEach((key) => {
      if (typeof element[key] === 'string') {
        trimmedObject[key] = element[key].trim();
      } else {
        trimmedObject[key] = element[key];
      }
    });

    trimmedData.push(trimmedObject);
  });

  return trimmedData;
}

function convertQueryToSelectSQL(query, type) {
  let sqlQuery = '';

  let keyCount = 0;

  const parameterArr = [];

  for (const [key, value] of Object.entries(query)) {
    if (keyCount > 0) {
      if (type === 'SELECT') {
        sqlQuery += ' AND ';
      }

      if (type === 'UPDATE') {
        sqlQuery += ', ';
      }
    }

    sqlQuery += `${key}=$${keyCount + 1}`;

    if (isInt(value)) {
      parameterArr.push(parseInt(value, 10));
    } else {
      parameterArr.push(value);
    }

    keyCount++;
  }

  return {
    sqlQuery,
    parameterArr,
    keyCount,
  };
}

function buildSQLInsert(data) {
  let keys = '';
  let values = '';

  let keyCount = 0;

  const parameterArr = [];

  for (const [key, value] of Object.entries(data)) {
    if (keyCount > 0) {
      keys += ', ';
      values += ', ';
    }

    keys += key;

    values += `$${keyCount + 1}`;

    if (isInt(value)) {
      parameterArr.push(parseInt(value, 10));
    } else {
      parameterArr.push(value);
    }

    keyCount++;
  }

  const sqlQuery = `(${keys}) VALUES (${values})`;

  return {
    sqlQuery,
    parameterArr,
    keyCount,
  };
}

module.exports = {
  buildSQLInsert,
  convertQueryToSelectSQL,
  trimData,
};
