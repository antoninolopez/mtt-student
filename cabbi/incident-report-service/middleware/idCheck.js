const isInt = require('../helpers/isInt');

module.exports = function isIDValid(req, res, next) {
  if (!isInt(req.params.id)) {
    res.status(400).send('ID Needs to be an integer');
  }
  next();
};
