module.exports = function isInt(str) {
  if (typeof str !== 'string') return false; // we only process strings!

  if (str.includes('.')) return false;

  const hexRegex = new RegExp('[a-z]');

  if (hexRegex.test(str)) {
    return false;
  }

  const dateRegex = new RegExp('[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}');

  if (dateRegex.test(str)) {
    return false;
  }

  return (
    !Number.isNaN(str) && // use type coercion to parse the entire string
    !Number.isNaN(parseInt(str, 10))
  ); // ...and ensure strings of whitespace fail
};
