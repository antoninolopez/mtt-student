const WorkOrder = {
  incident(parent, args, { dataSources }) {
    return new Promise((resolve, reject) => {
      dataSources.db
        .allIRs()
        .then((res) => {
          const results = res.find(
            (ir) => parseInt(ir.id, 10) === parseInt(parent.incident_id, 10),
          );
          resolve(results);
        })
        .catch((err) => reject(err));
    });
  },
};

module.exports = WorkOrder;
