const IncidentReport = {
  location(parent, args, { dataSources }) {
    return new Promise((resolve, reject) => {
      dataSources.db
        .allLocations()
        .then((res) => {
          const results = res.find(
            (loc) => parseInt(loc.id, 10) === parseInt(parent.location_id, 10),
          );
          resolve(results);
        })
        .catch((err) => reject(err));
    });
  },

  vehicle(parent, args, { dataSources }) {
    return new Promise((resolve, reject) => {
      dataSources.db
        .allVehicles()
        .then((res) => {
          const results = res.find(
            // prettier-ignore
            (vehicle) => parseInt(vehicle.id, 10) === parseInt(parent.vehicle_id, 10),
          );
          resolve(results);
        })
        .catch((err) => reject(err));
    });
  },
};

module.exports = IncidentReport;
