const Query = {
  vehicles: (parent, args, { dataSources }) => {
    const { db } = dataSources;

    if (args.id) {
      return db.allVehicles().whereIn('id', args.id);
    }

    return db.allVehicles();
  },
  vendors: (parent, args, { dataSources }) => {
    const { db } = dataSources;

    if (args.id) {
      return db.allVendors().whereIn('id', args.id);
    }

    return db.allVendors();
  },
  rateCodes: (parent, args, { dataSources }) => {
    const { db } = dataSources;

    if (args.id) {
      return db.allRateCodes().whereIn('id', args.id);
    }

    return db.allRateCodes();
  },
  payments: (parent, args, { dataSources }) => {
    const { db } = dataSources;

    if (args.id) {
      return db.allPayments().whereIn('id', args.id);
    }

    return db.allPayments();
  },
  locations: (parent, args, { dataSources }) => {
    const { db } = dataSources;

    if (args.id) {
      return db.allLocations().whereIn('id', args.id);
    }

    return db.allLocations();
  },
  incidentReports: (parent, args, { dataSources }) => {
    const { db } = dataSources;

    if (args.id) {
      return db.allIRs().whereIn('id', args.id);
    }

    return db.allIRs();
  },
  workOrders: (parent, args, { dataSources }) => {
    const { db } = dataSources;

    if (args.id) {
      return db.allWOs().whereIn('id', args.id);
    }

    return db.allWOs();
  },
};

module.exports = Query;
