const Vehicle = {
  vendor(parent, args, { dataSources }) {
    return new Promise((resolve, reject) => {
      dataSources.db
        .allVendors()
        .then((res) => {
          const results = res.find(
            // prettier-ignore
            (vendor) => parseInt(vendor.id, 10) === parseInt(parent.vendor_id, 10),
          );
          resolve(results);
        })
        .catch((err) => reject(err));
    });
  },
};

module.exports = Vehicle;
