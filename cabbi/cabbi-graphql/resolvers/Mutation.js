const { logError } = require('../logging/logger');

const Mutation = {
  async addVehicle(parent, args, { dataSources }) {
    const dataStatus = await dataSources.db
      .addVehicle(args)
      .catch((err) => logError(err));

    return {
      id: dataStatus[0],
      ...args,
    };
  },
  async addIncidentReport(parent, args, { dataSources }) {
    const dataStatus = await dataSources.db
      .addIR(args)
      .catch((err) => logError(err));

    return {
      id: dataStatus[0],
      ...args,
    };
  },
  async addWorkOrder(parent, args, { dataSources }) {
    const dataStatus = await dataSources.db
      .addWO(args)
      .catch((err) => logError(err));

    return {
      id: dataStatus[0],
      ...args,
    };
  },

  async approveWorkOrder(parent, args, { dataSources }) {
    const dataStatus = await dataSources.db
      .approveWorkOrder(args)
      .catch((err) => logError(err));

    return {
      id: dataStatus[0],
      ...args,
    };
  },

  async deleteVehicle(parent, args, { dataSources }) {
    const deleteStatus = await dataSources.db.deleteVehicle(args.id);

    return deleteStatus;
  },

  async deleteIncidentReport(parent, args, { dataSources }) {
    const deleteStatus = await dataSources.db.deleteIncidentReport(args.id);

    return deleteStatus;
  },

  async deleteWorkOrder(parent, args, { dataSources }) {
    const deleteStatus = await dataSources.db.deleteWorkOrder(args.id);

    return deleteStatus;
  },
};

module.exports = Mutation;
