const express = require('express');
const { ApolloServer } = require('apollo-server-express');
const { RedisCache } = require('apollo-server-cache-redis');
const { readFileSync } = require('fs');
const {
  KeycloakContext,
  KeycloakTypeDefs,
  KeycloakSchemaDirectives,
} = require('keycloak-connect-graphql');
const Query = require('./resolvers/Query');
const Mutation = require('./resolvers/Mutation');
const IncidentReport = require('./resolvers/IncidentReport');
const Vehicle = require('./resolvers/Vehicle');
const WorkOrder = require('./resolvers/WorkOrder');
const { logInfo } = require('./logging/logger');
const CABBIDB = require('./data/cabbiSql');

if (process.env.NODE_ENV !== 'production') {
  // eslint-disable-next-line global-require
  require('dotenv').config();
}

const PORT = process.env.PORT || 4000;

require('./config/keycloak-config.js')
  .initKeycloak()
  .then((keycloak) => {
    const typeDefs = readFileSync('./schema/schema.graphql', {
      encoding: 'utf-8',
    });

    const resolvers = {
      Query,
      Mutation,
      IncidentReport,
      Vehicle,
      WorkOrder,
    };

    const cabbiConfig = {
      client: 'pg',
      connection: `postgresql://${process.env.PGUSER}:${process.env.PGPASSWORD}@${process.env.PGHOST}:${process.env.PGPORT}/${process.env.PGDATABASE}`,
    };

    const server = new ApolloServer({
      typeDefs: [KeycloakTypeDefs, typeDefs],
      // typeDefs,
      schemaDirectives: KeycloakSchemaDirectives,
      resolvers,
      cache: new RedisCache({
        host: process.env.REDIS_HOST,
      }),
      context: ({ req }) => ({
        kauth: new KeycloakContext({ req }, keycloak),
      }),
      dataSources: () => ({ db: new CABBIDB(cabbiConfig) }),
    });

    const app = express();

    app.use('/', keycloak.middleware());

    server.applyMiddleware({ app });

    // The `listen` method launches a web server.
    app.listen({ port: PORT }, () => {
      logInfo(`🚀  Server ready at http://localhost:${PORT}`);
    });
  });
