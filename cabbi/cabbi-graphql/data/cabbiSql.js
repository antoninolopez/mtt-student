const { SQLDataSource } = require('datasource-sql');

const MINUTE = 60;

class CABBIDB extends SQLDataSource {
  // QUERIES
  allVehicles() {
    return this.knex.select('*').from('Vehicles').cache(MINUTE);
  }

  allVendors() {
    return this.knex.select('*').from('Vendors').cache(MINUTE);
  }

  allPayments() {
    return this.knex.select('*').from('Payments').cache(MINUTE);
  }

  allLocations() {
    return this.knex.select('*').from('Locations').cache(MINUTE);
  }

  allRateCodes() {
    return this.knex.select('*').from('RateCodes').cache(MINUTE);
  }

  allIRs() {
    return this.knex.select('*').from('IncidentReports');
  }

  allWOs() {
    return this.knex.select('*').from('WorkOrders');
  }

  // MUTATIONS
  addVehicle(vehicleData) {
    return this.knex('Vehicles').insert(vehicleData).returning('id');
  }

  addIR(incidentReport) {
    return this.knex('IncidentReports').insert(incidentReport).returning('id');
  }

  addWO(workOrder) {
    return this.knex('WorkOrders').insert(workOrder).returning('id');
  }

  approveWorkOrder(approvalData) {
    return this.knex('WorkOrders')
      .update({
        approved_by: approvalData.approver,
        approval_status: 'approved',
      })
      .where({ id: approvalData.id })
      .returning('*');
  }

  // DELETIONS
  deleteVehicle(id) {
    return this.knex('Vehicles').where({ id }).del();
  }

  deleteIncidentReport(id) {
    return this.knex('IncidentReports').where({ id }).del();
  }

  deleteWorkOrder(id) {
    return this.knex('WorkOrders').where({ id }).del();
  }
}

module.exports = CABBIDB;
