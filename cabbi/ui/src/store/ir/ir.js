import axios from 'axios';
import IncidentReportModel from '../../models/IncidentReport';
import formatData from '../dataTransforms/formatData';
import { sortByID } from '../dataTransforms/sorting';

const state = {
  incidentReports: [],
  incidentReportUpdates: []
};

const getters = {
  incidentReports: state => (page, pageSize) => {
    return state.incidentReports.sort(sortByID).slice(page, pageSize);
  },
  incidentReportByID: state => irID => {
    let ir = state.incidentReports.filter(
      ir => parseInt(ir.id) === parseInt(irID)
    );

    if (ir.length > 0) {
      return ir[0];
    } else {
      return [];
    }
  },
  incidentReportByRideID: state => rideID => {
    return state.incidentReports.filter(ir => ir.ride_id === rideID);
  },
  incidentReportChanges: state => irID => {
    return state.incidentReportUpdates.filter(ir => ir.id === irID);
  }
};

const mutations = {
  /**
   * Overrides all IRs in state
   * @param {*} state - State Snapshot
   * @param {*} irs - New Incident Reports
   */
  setIncidentReports(state, irs) {
    state.incidentReports = irs;
  },

  /**
   * Merges new incident reports with current one after receiving HTTP req with an
   * additional IR
   * @param {*} state - State Snapshot
   * @param {*} irs - New Incident Reports
   */
  mergeNewIncidentReports(state, irs) {
    let oldIncidentReports = state.incidentReports;

    let irIDList = irs.map(ir => parseInt(ir.id));

    let newIrs = [];

    oldIncidentReports.forEach(ir => {
      if (!irIDList.includes(parseInt(ir.id))) {
        newIrs.push(ir);
      }
    });

    newIrs.push(...irs);

    state.incidentReports = newIrs;
  },

  /**
   * Add Change to Update Array, if update already exists it merges the new update into the existing update
   * @param {*} state - State Snapshot
   * @param {*} change - Change To Add to update array
   */
  addIncidentReportUpdate(state, change) {
    let targetIndex = state.incidentReportUpdates.findIndex(
      ir => ir.id === change.id
    );

    let newChange = {};

    newChange['id'] = change.id;
    newChange[change.slug] = change.val;

    if (targetIndex >= 0) {
      // Update Existing IR
      state.incidentReportUpdates[targetIndex][change.slug] = change.val;
    } else {
      // Add New IR
      state.incidentReportUpdates = [...state.incidentReportUpdates, newChange];
    }
  },

  /**
   * Removes id from update array
   * @param {*} state - State Snapshot
   * @param {*} id - ID To Remove from update Array
   */
  removeUpdate(state, id) {
    state.incidentReportUpdates = state.incidentReportUpdates.filter(
      update => parseInt(update.id) !== parseInt(id)
    );
  },

  /**
   * Removes an incident report from the IR array
   * @param {*} state - State Snapshot
   * @param {*} id - ID to Remove from IR array
   */
  deleteIncidentReport(state, id) {
    state.incidentReports = state.incidentReports.filter(
      ir => parseInt(ir.id) !== parseInt(id)
    );
  }
};

const actions = {
  /**
   * Gets list of all IRs from server
   * @param {*} param0 - CTX
   */
  async updateIncidentReports({ commit }) {
    await axios
      .get(`${process.env.VUE_APP_API_LOCATION}/ir?status=1`)
      .then(res => {
        let rawData = res.data;

        commit('setIncidentReports', formatData(rawData, IncidentReportModel));
      })
      .catch(err => console.error(err));
  },

  /**
   * Gets single IR by id from server, triggers merge to merge new data into store
   * @param {*} param0 - CTX
   * @param {*} irID - ID of incident report to delete
   */
  async getIncidentReportByID({ commit }, irID) {
    await axios
      .get(`${process.env.VUE_APP_API_LOCATION}/ir/${irID}`)
      .then(res => {
        let rawData = res.data;

        commit(
          'mergeNewIncidentReports',
          formatData(rawData, IncidentReportModel)
        );
      })
      .catch(err => console.error(err));
  },

  /**
   * Checks if update is new change, passes to addIncidentReportUpdate mutation if new
   * @param {*} param0 - CTX
   * @param {*} data - Update delta
   */
  async updateIncidentReport({ commit }, data) {
    let targetIncidentReport = getters.incidentReportByID(data.id);

    let oldVal = targetIncidentReport[data.slug];
    let newVal = data.val;

    if (oldVal !== newVal) {
      commit('addIncidentReportUpdate', data);
    }
  },

  /**
   * Sends update to server from IRUpdateArray based on id
   * @param {*} param0 - CTX
   * @param {*} id - id to update
   */
  syncIR({ state, commit }, id) {
    return new Promise((resolve, reject) => {
      // Get update to send
      let targetIndex = state.incidentReportUpdates.findIndex(
        ir => ir.id === id
      );
      let targetIR = state.incidentReportUpdates[targetIndex];

      delete targetIR.id;

      axios
        .put(`${process.env.VUE_APP_API_LOCATION}/ir/${id}`, targetIR)
        .then(data => {
          commit('removeUpdate', id);
          resolve(data);
        })
        .catch(err => reject(err));
    });
  },

  /**
   * Deletes incident report from server, then triggers delete store mutation
   * @param {*} param0 - CTX
   * @param {*} id - IR ID
   */
  async deleteIR({ commit }, id) {
    return new Promise((resolve, reject) => {
      axios
        .delete(`${process.env.VUE_APP_API_LOCATION}/ir/${id}`)
        .then(data => {
          commit('deleteIncidentReport', id);
          resolve(data);
        })
        .catch(err => reject(err));
    });
  }
};

export default {
  state,
  mutations,
  actions,
  getters
};
