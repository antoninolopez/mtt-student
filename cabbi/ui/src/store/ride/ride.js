import axios from 'axios';
import formatData from '../dataTransforms/formatData';
import RideModel from '../../models/Ride';

const state = {
  rides: []
};

const getters = {
  rides: state => (page, pageSize) => {
    return state.rides.slice(page, pageSize);
  },
  rideByID: state => id => {
    let matchingRides = state.rides.filter(ride => ride._id === id);

    if (matchingRides.length > 0) {
      return matchingRides[0];
    } else {
      return [];
    }
  }
};

const mutations = {
  setRides(state, rides) {
    state.rides = rides;
  },
  mergeRides(state, rides) {
    let oldRides = state.rides;

    let rideIDList = rides.map(ride => ride._id);

    let newRides = [];

    oldRides.forEach(ride => {
      if (!rideIDList.includes(parseInt(ride.id))) {
        newRides.push(ride);
      }
    });

    newRides.push(...rides);

    state.rides = newRides;
  }
};

const actions = {
  async updateRides({ commit }) {
    await axios
      .get(`${process.env.VUE_APP_API_LOCATION}/ride?page=1&limitCount=100`)
      .then(res => {
        let newRideData = res.data.data;

        commit('setRides', formatData(newRideData, RideModel));
      })
      .catch(err => console.error(err));
  },
  async findRideByID({ commit }, targetRideID) {
    await axios
      .get(`${process.env.VUE_APP_API_LOCATION}/ride/${targetRideID}`)
      .then(res => {
        let newRideData = res.data.data;

        commit('mergeRides', formatData([newRideData], RideModel));
      })
      .catch(err => console.error(err));
  }
};

export default {
  state,
  mutations,
  actions,
  getters
};
