import axios from 'axios';

const state = {
  vehicles: []
};

const getters = {
  vehicles: state => (page, pageSize) => {
    return state.vehicles.slice(page, pageSize);
  },
  vehicleByID: state => id => {
    let matchingVehicles = state.vehicles.filter(
      vehicle => parseInt(vehicle.id) === parseInt(id)
    );

    if (matchingVehicles.length > 0) {
      return matchingVehicles[0];
    } else {
      return [];
    }
  }
};

const mutations = {
  setVehicles(state, vehicles) {
    state.vehicles = vehicles;
  }
};

const actions = {
  async updateVehicles({ commit }) {
    await axios
      .get(`${process.env.VUE_APP_API_LOCATION}/vehicle?status=1`)
      .then(res => {
        commit('setVehicles', res.data.data);
      })
      .catch(err => console.error(err));
  }
};

export default {
  state,
  mutations,
  actions,
  getters
};
