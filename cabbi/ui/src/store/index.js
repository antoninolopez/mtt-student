import Vue from 'vue';
import Vuex from 'vuex';

import auth from './auth/auth';
import ir from './ir/ir';
import wo from './wo/wo';
import ride from './ride/ride';
import vehicle from './vehicle/vehicle';
import metadata from './metadata/metadata';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {},
  getters: {},
  mutations: {},
  actions: {},
  modules: {
    auth,
    ir,
    wo,
    ride,
    vehicle,
    metadata
  }
});

export default store;
