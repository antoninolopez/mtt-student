import axios from 'axios';
import formatData from '../dataTransforms/formatData';
import WorkOrderModel from '../../models/WorkOrder';
import { sortByID } from '../dataTransforms/sorting';

const state = {
  workOrders: [],
  workOrderUpdates: []
};

const getters = {
  workOrders: state => (page, pageSize) => {
    return state.workOrders.sort(sortByID).slice(page, pageSize);
  },
  workOrderByID: state => woID => {
    let matchingWOs = state.workOrders.filter(wo => wo.id === woID);

    if (matchingWOs.length > 0) {
      return matchingWOs[0];
    } else {
      return [];
    }
  },
  workOrdersByIncidentReport: state => irID => {
    return state.workOrders.filter(
      wo => parseInt(wo.incident_id) === parseInt(irID)
    );
  },
  workOrderChanges: state => woID => {
    return state.workOrderUpdates.filter(wo => wo.id === woID);
  },
  pendingWOUpdates(state) {
    return state.workOrderUpdates;
  }
};

const mutations = {
  setWorkOrders(state, wos) {
    state.workOrders = wos;
  },
  mergeNewWorkOrders(state, wos) {
    let oldWorkOrders = state.workOrders;

    let woIDList = wos.map(wo => parseInt(wo.id));

    let newWos = [];

    oldWorkOrders.forEach(wo => {
      if (!woIDList.includes(parseInt(wo.id))) {
        newWos.push(wo);
      }
    });

    newWos.push(...wos);

    state.workOrders = newWos;
  },
  addWorkOrderUpdate(state, change) {
    let targetIndex = state.workOrderUpdates.findIndex(
      wo => wo.id === change.id
    );

    let newChange = {};

    newChange['id'] = change.id;
    newChange[change.slug] = change.val;

    if (targetIndex >= 0) {
      // Update Existing Work Order
      state.workOrderUpdates[targetIndex][change.slug] = change.val;
    } else {
      // Add New Work Order
      state.workOrderUpdates = [...state.workOrderUpdates, newChange];
    }
  },
  updateTotalCost(state, id) {
    let targetIndex = state.workOrderUpdates.findIndex(wo => wo.id === id);
    let targetIndexWOState = state.workOrders.findIndex(wo => wo.id === id);

    let targetWO = state.workOrderUpdates[targetIndex];
    let targetWOState = state.workOrders[targetIndexWOState];

    let matCost, labCost;

    if (targetWO.material_cost) {
      matCost = targetWO.material_cost;
    } else {
      matCost = targetWOState.material_cost;
    }
    if (targetWO.labor_cost) {
      labCost = targetWO.labor_cost;
    } else {
      labCost = targetWOState.labor_cost;
    }

    state.workOrderUpdates[targetIndex]['final_total_costs'] =
      parseFloat(matCost) + parseFloat(labCost);
  },
  removeUpdate(state, id) {
    state.workOrderUpdates = state.workOrderUpdates.filter(
      update => parseInt(update.id) !== parseInt(id)
    );
  },
  deleteWorkOrder(state, id) {
    state.workOrders = state.workOrders.filter(
      wo => parseInt(wo.id) !== parseInt(id)
    );
  }
};

const actions = {
  async updateWorkOrders({ commit }) {
    await axios
      .get(`${process.env.VUE_APP_API_LOCATION}/wo/open`)
      .then(res => {
        let rawData = res.data;

        commit('setWorkOrders', formatData(rawData, WorkOrderModel));
      })
      .catch(err => console.error(err));
  },
  async getWorkOrdersByIncidentID({ commit }, irID) {
    await axios
      .get(`${process.env.VUE_APP_API_LOCATION}/wo?incident_id=${irID}`)
      .then(res => {
        let rawData = res.data;

        commit('mergeNewWorkOrders', formatData(rawData, WorkOrderModel));
      })
      .catch(err => console.error(err));
  },
  async updateWorkOrder({ getters, commit }, data) {
    let targetWorkOrder = getters.workOrderByID(data.id);

    let oldVal, newVal;

    if (data.slug === 'start_date' || data.slug === 'end_date') {
      oldVal = new Date(targetWorkOrder[data.slug]).getTime();
      newVal = new Date(data.val).getTime();
    } else if (data.slug === 'material_cost' || data.slug === 'labor_cost') {
      oldVal = parseFloat(targetWorkOrder[data.slug]);
      newVal = parseFloat(data.val);
    } else {
      oldVal = targetWorkOrder[data.slug];
      newVal = data.val;
    }

    // Check if store val and new val are equal
    if (oldVal !== newVal) {
      commit('addWorkOrderUpdate', data);

      if (data.slug === 'material_cost' || data.slug === 'labor_cost') {
        commit('updateTotalCost', data.id);
      }
    }
  },
  async syncWO({ state, commit }, id) {
    return new Promise((resolve, reject) => {
      // Get update to send
      let targetIndex = state.workOrderUpdates.findIndex(wo => wo.id === id);
      let targetWO = state.workOrderUpdates[targetIndex];

      delete targetWO.id;

      axios
        .put(`${process.env.VUE_APP_API_LOCATION}/wo/${id}`, targetWO)
        .then(data => {
          commit('removeUpdate', id);
          resolve(data);
        })
        .catch(err => reject(err));
    });
  },
  async deleteWO({ commit }, id) {
    return new Promise((resolve, reject) => {
      axios
        .delete(`${process.env.VUE_APP_API_LOCATION}/wo/${id}`)
        .then(data => {
          commit('deleteWorkOrder', id);
          resolve(data);
        })
        .catch(err => reject(err));
    });
  },
  async approveWorkOrder(ctx, details) {
    return new Promise((resolve, reject) => {
      console.log(ctx);
      if (
        details.userRoles.includes('admin') ||
        details.userRoles.includes('approver')
      ) {
        // Send Request

        const approvalURI = `${process.env.VUE_APP_API_LOCATION}/wo/${details.id}/approve`;

        delete details.id;

        axios
          .post(approvalURI, details)
          .then(data => {
            console.log(data.data);
            ctx.dispatch('updateWorkOrders');
          })
          .catch(err => {
            reject(err);
          });
      } else {
        reject('unauthorized');
      }
      resolve();
    });
  }
};

export default {
  state,
  mutations,
  actions,
  getters
};
