export default function(rawAmount) {
  if (typeof rawAmount === 'string') {
    return parseFloat(rawAmount);
  } else {
    return rawAmount / 100;
  }
}
