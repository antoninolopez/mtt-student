import formatDate from './date';
import formatMoney from './money';

export default function(rawData, model) {
  let formattedData = [];

  rawData.forEach(ride => {
    let newRide = ride;

    Object.keys(ride).forEach(key => {
      switch (getKeyType(key, model)) {
        case 'money':
          newRide[key] = formatMoney(ride[key]);

          break;
        case 'datetime':
          newRide[key] = formatDate(ride[key], 'datetime');

          break;

        case 'date':
          newRide[key] = formatDate(ride[key], 'date');

          break;

        case 'time':
          newRide[key] = formatDate(ride[key], 'time');

          break;

        default:
          break;
      }
    });

    formattedData.push(newRide);
  });

  return formattedData;
}

function getKeyType(key, model) {
  return model.filter(entry => entry.name === key)[0].type;
}
