export default function(rawDate, type) {
  let formattedDate = new Date(rawDate);

  switch (type) {
    case 'date':
      return `${formattedDate.toLocaleDateString('en-US')}`;
    case 'time':
      return ` ${formattedDate.toLocaleTimeString('en-US')}`;
    case 'datetime':
      return `${formattedDate.toLocaleDateString(
        'en-US'
      )} ${formattedDate.toLocaleTimeString('en-US')}`;
  }
}
