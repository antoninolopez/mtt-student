const state = {
  userProfile: {}
};

const getters = {
  getFullName(state) {
    return state.userProfile.name;
  },

  getUser(state) {
    return state.userProfile;
  },

  isAdmin() {
    return state.userProfile.realm_access.roles.includes('admin');
  },

  isApprover() {
    return state.userProfile.realm_access.roles.includes('approver');
  },

  userID() {
    return state.userProfile.sub;
  },

  userRoles() {
    return state.userProfile.realm_access.roles;
  }
};

const mutations = {
  setUserProfile(state, val) {
    state.userProfile = val;
  }
};

const actions = {
  async setUserProfile({ commit }, user) {
    let userData = user;

    commit('setUserProfile', userData);
  }
};

export default {
  state,
  mutations,
  actions,
  getters
};
