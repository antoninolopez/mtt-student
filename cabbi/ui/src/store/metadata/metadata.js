import axios from 'axios';

const state = {
  locations: [],
  rateCodes: [],
  payments: []
};

const getters = {
  locations: state => {
    return state.locations;
  },
  locationByID: state => id => {
    let matchingLocations = state.locations.filter(
      ride => parseInt(ride.id) === parseInt(id)
    );

    if (matchingLocations.length > 0) {
      return matchingLocations[0];
    } else {
      return [];
    }
  },
  rateCodes: state => {
    return state.rateCodes;
  },
  rateCodeByID: state => id => {
    return state.rateCodes.filter(ride => ride.id === id);
  },
  payments: state => {
    return state.payments;
  },
  paymentByID: state => id => {
    let matchingPayment = state.payments.filter(payment => payment.id === id);

    if (matchingPayment.length > 0) {
      return matchingPayment[0];
    } else {
      return [];
    }
  }
};

const mutations = {
  setLocations(state, locations) {
    state.locations = locations;
  },
  setRateCodes(state, rateCodes) {
    state.rateCodes = rateCodes;
  },
  setPayments(state, payments) {
    state.payments = payments;
  }
};

const actions = {
  async updateLocations({ commit }) {
    await axios
      .get(`${process.env.VUE_APP_API_LOCATION}/metadata/locations`)
      .then(res => {
        let newLocationData = res.data;

        commit('setLocations', newLocationData);
      })
      .catch(err => console.error(err));
  },
  async updateRateCodes({ commit }) {
    await axios
      .get(`${process.env.VUE_APP_API_LOCATION}/metadata/ratecodes`)
      .then(res => {
        let newRateCodes = res.data;

        commit('setRateCodes', newRateCodes);
      })
      .catch(err => console.error(err));
  },
  async updatePayments({ commit }) {
    await axios
      .get(`${process.env.VUE_APP_API_LOCATION}/metadata/payments`)
      .then(res => {
        let payments = res.data;

        commit('setPayments', payments);
      })
      .catch(err => console.error(err));
  }
};

export default {
  state,
  mutations,
  actions,
  getters
};
