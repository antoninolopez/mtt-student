/**
 * Filter to capitalize every word in a sentence
 * @param {string} phrase String passed automatically to the filter
 */
function capitalizeFilter(phrase) {
  if (!phrase) return '';
  return phrase
    .toLowerCase()
    .split(' ')
    .map(word => word.charAt(0).toUpperCase() + word.slice(1))
    .join(' ');
}

function headingFilter(phrase) {
  if (!phrase) return '';
  let splitUnderscore = phrase.split('_');
  let splitDash = phrase.split('-');
  let splitSpace = phrase.split(' ');
  let camelCase = phrase.split(/(?=[A-Z])/).map(s => s.toLowerCase());

  if (splitUnderscore.length > 1) {
    return splitUnderscore
      .map(word => word.charAt(0).toUpperCase() + word.slice(1))
      .join(' ');
  }
  if (splitDash.length > 1) {
    return splitDash
      .map(word => word.charAt(0).toUpperCase() + word.slice(1))
      .join(' ');
  }
  if (splitSpace.length > 1) {
    return splitSpace
      .map(word => word.charAt(0).toUpperCase() + word.slice(1))
      .join(' ');
  }

  if (camelCase.length > 1) {
    return camelCase
      .map(word => word.charAt(0).toUpperCase() + word.slice(1))
      .join(' ');
  }

  return `${phrase.charAt(0).toUpperCase()}${phrase.slice(1)}`;
}

module.exports = {
  capitalizeFilter,
  headingFilter
};
