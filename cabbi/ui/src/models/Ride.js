export default [
  {
    name: '_id',
    label: 'ID',
    type: 'string',
    key: true,
    show: false,
    order: 0
  },
  {
    name: 'PULocationID',
    label: 'Pickup Location ID',
    type: 'id',
    key: false,
    show: true,
    order: 1
  },
  {
    name: 'DOLocationID',
    label: 'Dropoff Location ID',
    type: 'id',
    key: false,
    show: true,
    order: 2
  },
  {
    name: 'vehicle_id',
    label: 'Vehicle ID',
    type: 'id',
    key: false,
    show: true,
    order: 3
  },
  {
    name: 'tpep_pickup_datetime',
    label: 'Pickup Date',
    type: 'datetime',
    key: false,
    show: true,
    order: 4
  },
  {
    name: 'tpep_dropoff_datetime',
    label: 'Dropoff Datetime',
    type: 'datetime',
    key: false,
    show: true,
    order: 5
  },
  {
    name: 'trip_distance',
    label: 'Trip Distance',
    type: 'number',
    key: false,
    show: true,
    order: 6
  },
  {
    name: 'total_amount',
    label: 'Total Amount',
    type: 'money',
    key: false,
    show: true,
    order: 7
  },
  {
    name: 'tolls_amount',
    label: 'Toll Amount',
    type: 'money',
    key: false,
    show: false,
    order: 6
  },
  {
    name: 'tip_amount',
    label: 'Tip Amount',
    type: 'money',
    key: false,
    show: true,
    order: 7
  },
  {
    name: 'store_and_fwd_flag',
    label: 'Store And Forward',
    type: 'string',
    key: false,
    show: false,
    order: 8
  },
  {
    name: 'payment_type',
    label: 'Payment Type',
    type: 'number',
    key: false,
    show: false,
    order: 9
  },
  {
    name: 'mta_tax',
    label: 'MTA Tax',
    type: 'money',
    key: false,
    show: false,
    order: 10
  },
  {
    name: 'improvement_surcharge',
    label: 'Improvement Surcharge',
    type: 'money',
    key: false,
    show: false,
    order: 11
  },
  {
    name: 'fare_amount',
    label: 'Fare Amount',
    type: 'money',
    key: false,
    show: false,
    order: 12
  },
  {
    name: 'extra',
    label: 'Extra',
    type: 'money',
    key: false,
    show: false,
    order: 13
  },
  {
    name: 'congestion_surcharge',
    label: 'Congestion Surcharge',
    type: 'money',
    key: false,
    show: false,
    order: 14
  },
  {
    name: 'VendorID',
    label: 'Vendor ID',
    type: 'id',
    key: false,
    show: false,
    order: 15
  },
  {
    name: 'RatecodeID',
    label: 'Ratecode ID',
    type: 'id',
    key: false,
    show: false,
    order: 16
  },
  {
    name: 'passenger_count',
    label: 'Passenger count',
    type: 'number',
    key: false,
    show: true,
    order: 19
  }
];
