export default [
  {
    name: 'id',
    label: 'ID',
    type: 'number',
    key: true,
    show: true,
    order: 0
  },
  {
    name: 'description',
    label: 'Description',
    type: 'string',
    key: false,
    show: true,
    order: 3
  },
  {
    name: 'incident_datetime',
    label: 'Incident DateTime',
    type: 'datetime',
    key: false,
    show: true,
    order: 2
  },
  {
    name: 'location_id',
    label: 'Location ID',
    type: 'id',
    key: false,
    show: true,
    order: 1
  },
  {
    name: 'police_report',
    label: 'Police Report',
    type: 'string',
    key: false,
    show: true,
    order: 4
  },
  {
    name: 'ride_id',
    label: 'Ride ID',
    type: 'id',
    key: false,
    show: true,
    order: 5
  },
  {
    name: 'status',
    label: 'Active',
    type: 'string',
    key: false,
    show: false,
    order: 6
  },
  {
    name: 'type',
    label: 'Type',
    type: 'string',
    key: false,
    show: true,
    order: 7
  },
  {
    name: 'vehicle_id',
    label: 'Vehicle ID',
    type: 'id',
    key: false,
    show: true,
    order: 8
  }
];
