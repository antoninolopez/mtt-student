export default [
  {
    name: 'id',
    label: 'ID',
    type: 'number',
    key: true,
    show: false,
    order: 0
  },
  {
    name: 'incident_id',
    label: 'Incident ID',
    type: 'id',
    key: false,
    show: false,
    order: 1
  },
  {
    name: 'work_description',
    label: 'Description',
    type: 'number',
    key: false,
    show: true,
    order: 2
  },
  {
    name: 'material_cost',
    label: 'Material Cost',
    type: 'money',
    key: false,
    show: true,
    order: 3
  },
  {
    name: 'labor_cost',
    label: 'Labor Cost',
    type: 'money',
    key: false,
    show: true,
    order: 4
  },
  {
    name: 'final_total_costs',
    label: 'Total Cost',
    type: 'money',
    key: false,
    show: true,
    order: 5
  },
  {
    name: 'start_date',
    label: 'Start Date',
    type: 'date',
    key: false,
    show: true,
    order: 6
  },
  {
    name: 'end_date',
    label: 'End Date',
    type: 'date',
    key: false,
    show: true,
    order: 7
  },
  {
    name: 'additional_details',
    label: 'Additional Details',
    type: 'string',
    key: false,
    show: false,
    order: 8
  },
  {
    name: 'approval_status',
    label: 'Approval Status',
    type: 'String',
    key: false,
    show: true,
    order: 9
  },
  {
    name: 'approved_by',
    label: 'Approver',
    type: 'String',
    key: false,
    show: false,
    order: 9
  }
];
