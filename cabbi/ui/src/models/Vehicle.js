export default [
  {
    name: 'id',
    label: 'ID',
    type: 'number',
    key: true,
    show: true,
    order: 0
  },
  {
    name: 'vendor_id',
    label: 'Vendor ID',
    type: 'id',
    key: false,
    show: true,
    order: 3
  },
  {
    name: 'year',
    label: 'Year',
    type: 'number',
    key: false,
    show: true,
    order: 2
  },
  {
    name: 'make',
    label: 'Make',
    type: 'string',
    key: false,
    show: true,
    order: 1
  },
  {
    name: 'model',
    label: 'Model',
    type: 'string',
    key: false,
    show: true,
    order: 4
  },
  {
    name: 'license_plate',
    label: 'License Plate',
    type: 'string',
    key: false,
    show: true,
    order: 5
  },
  {
    name: 'max_passengers',
    label: 'Max Passengers',
    type: 'string',
    key: false,
    show: false,
    order: 6
  },
  {
    name: 'status',
    label: 'Active',
    type: 'number',
    key: false,
    show: true,
    order: 7
  }
];
