import Vue from 'vue';
import App from './App.vue';
import store from '@/store';
import router from './router';
import Axios from 'axios';

import Buefy from 'buefy';
import 'buefy/dist/buefy.css';
import '@fortawesome/fontawesome-free/js/all.js';

Vue.use(Buefy);

import VueKeyCloak from '@dsb-norge/vue-keycloak-js';

Vue.config.productionTip = false;

const ssoUrl = process.env.VUE_APP_KEYCLOAK_URI;
const ssoClientId = process.env.VUE_APP_KEYCLOAK_CLIENT_ID;
const ssoRealm = process.env.VUE_APP_KEYCLOAK_SSO_REALM;
const logoutUrl = process.env.VUE_APP_KEYCLOAK_LOGOUT_URL;

function tokenInterceptor() {
  Axios.interceptors.request.use(
    config => {
      const currentConfig = config;
      currentConfig.headers.Authorization = `Bearer ${Vue.prototype.$keycloak.token}`;
      return currentConfig;
    },
    error => Promise.reject(error)
  );
}

Vue.use(VueKeyCloak, {
  logout: {
    redirectUri: logoutUrl
  },
  init: {
    onLoad: 'login-required',
    checkLoginIframe: false
  },
  config: {
    realm: ssoRealm,
    url: ssoUrl,
    clientId: ssoClientId,
    logoutRedirectUri: logoutUrl
  },
  onReady: () => {
    tokenInterceptor();
    new Vue({
      store,
      router,
      render: h => h(App)
    }).$mount('#app');
  }
});
