import Vue from 'vue';
import VueRouter from 'vue-router';
import Dashboard from '../views/Dashboard.vue';

Vue.use(VueRouter);

const routes = [
  // {
  //   path: '/rides/:id/edit',
  //   name: 'RideDetailEdit',
  //   component: () =>
  //     import(
  //       /* webpackChunkName: "RideDetailEdit" */ '../views/RideDetailEdit.vue'
  //     ),
  //   meta: {
  //     requiredRole: ['admin']
  //   }
  // },
  {
    path: '/rides/:id',
    name: 'RideDetail',
    component: () =>
      import(/* webpackChunkName: "RideDetail" */ '../views/RideDetail.vue')
  },
  {
    path: '/rides',
    name: 'Rides',
    component: () =>
      import(/* webpackChunkName: "Rides" */ '../views/Rides.vue')
  },
  // {
  //   path: '/vehicles/:id/edit',
  //   name: 'VehicleDetailEdit',
  //   component: () =>
  //     import(
  //       /* webpackChunkName: "VehicleDetailEdit" */ '../views/VehicleDetailEdit.vue'
  //     ),
  //   meta: {
  //     requiredRole: ['admin']
  //   }
  // },
  {
    path: '/vehicles/:id/graphql',
    name: 'VehicleDetailGraphQL',
    component: () =>
      import(
        /* webpackChunkName: "VehicleDetailGraphQL" */ '../views/VehicleDetailGraphQL.vue'
      )
  },
  {
    path: '/vehicles/:id',
    name: 'VehicleDetail',
    component: () =>
      import(
        /* webpackChunkName: "VehicleDetail" */ '../views/VehicleDetail.vue'
      )
  },
  {
    path: '/vehicles',
    name: 'Vehicles',
    component: () =>
      import(/* webpackChunkName: "Vehicles" */ '../views/Vehicles.vue')
  },
  {
    path: '/incident-reports/:id/edit',
    name: 'IncidentReportEdit',
    component: () =>
      import(
        /* webpackChunkName: "IncidentReportEdit" */ '../views/IncidentReportEdit.vue'
      ),
    meta: {
      requiredRole: ['admin']
    }
  },
  {
    path: '/incident-reports/:id',
    name: 'IncidentReportDetail',
    component: () =>
      import(
        /* webpackChunkName: "IncidentReportDetail" */ '../views/IncidentReportDetail.vue'
      )
  },
  {
    path: '/incident-reports',
    name: 'IncidentReports',
    component: () =>
      import(
        /* webpackChunkName: "IncidentReports" */ '../views/IncidentReports.vue'
      )
  },
  {
    path: '/work-orders/:id/edit',
    name: 'WorkOrderDetailEdit',
    component: () =>
      import(
        /* webpackChunkName: "WorkOrderDetailEdit" */ '../views/WorkOrderDetailEdit.vue'
      ),
    meta: {
      requiredRole: ['admin']
    }
  },
  {
    path: '/work-orders/:id',
    name: 'WorkOrderDetail',
    component: () =>
      import(
        /* webpackChunkName: "WorkOrderDetail" */ '../views/WorkOrderDetail.vue'
      )
  },
  {
    path: '/work-orders',
    name: 'WorkOrders',
    component: () =>
      import(/* webpackChunkName: "WorkOrders" */ '../views/WorkOrders.vue')
  },
  {
    path: '/profile',
    name: 'Profile',
    component: () =>
      import(/* webpackChunkName: "Profile" */ '../views/Profile.vue')
  },
  {
    path: '/location/:id',
    name: 'LocationDetail',
    component: () =>
      import(
        /* webpackChunkName: "LocationDetail" */ '../views/LocationDetail.vue'
      )
  },
  {
    path: '/',
    name: 'Dashboard',
    component: Dashboard
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

router.beforeEach(async (to, from, next) => {
  // We wait for Keycloak init, then we can call all methods safely
  while (!router.app.$keycloak.ready) {
    await sleep(100);
  }

  const requiredRole = to.matched.some(el => el.meta.requiredRole);
  const currentRoles = router.app.$keycloak.tokenParsed.realm_access.roles;

  const roleList = to.matched[0].meta.requiredRole;

  if (requiredRole) {
    let hasMatch = hasMatchingRole(currentRoles, roleList);
    if (hasMatch) {
      next();
    } else {
      router.app.$buefy.toast.open({
        duration: 5000,
        message: `Only ${roleList.join(
          ' or '
        )} are allowed to access that page`,
        position: 'is-bottom',
        type: 'is-danger'
      });
      next('/');
    }
  } else {
    next();
  }
});

export default router;

function hasMatchingRole(userRoles, neededRoles) {
  const matchedArray = neededRoles.filter(value => userRoles.includes(value));

  if (matchedArray.length > 0) {
    return true;
  } else {
    return false;
  }
}
