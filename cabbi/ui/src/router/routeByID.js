export default function(that, trigger) {
  if (trigger.name === 'ride_id') {
    that.$router.push({
      name: 'RideDetail',
      params: { id: trigger.id }
    });
  }

  if (trigger.name === 'vehicle_id') {
    that.$router.push({
      name: 'VehicleDetail',
      params: { id: trigger.id }
    });
  }

  if (trigger.name === 'incident_id') {
    that.$router.push({
      name: 'IncidentReportDetail',
      params: { id: trigger.id }
    });
  }

  if (trigger.name.toLowerCase().includes('location')) {
    that.$router.push({
      name: 'LocationDetail',
      params: { id: trigger.id }
    });
  }
}
