import { Router } from "express";
import { AuthController } from "../controllers/auth.js";

const controller = new AuthController();
const router = new Router();

router.post("/register", async (req, res) => controller.register(req, res));
router.post("/login", async (req, res) => controller.login(req, res));

export const authRoutes = router;