import { Router } from 'express';
import { UserController } from '../controllers/user.js';
import jwt from 'jsonwebtoken';

const controller = new UserController();
const router = new Router();

const verifyToken = (req, res, next) => {
  const SECRET_KEY = 'secretkey123';
  const token = req.headers.authorization.split(' ')[1];

  if (!token) {
    return res.status(403).send('A token is required for authentication');
  }
  try {
    const decoded = jwt.verify(token, SECRET_KEY);
    req.user = decoded;
  } catch (err) {
    console.log(err);
    return res.status(401).send('Invalid Token');
  }
  return next();
};

router.get('/', verifyToken, async (req, res) => controller.getAll(req, res));
router.get('/:userID', async (req, res) => controller.getByID(req, res));
router.post('/', async (req, res) => controller.create(req, res));
router.put('/:userID', async (req, res) => controller.update(req, res));
router.delete('/:userID', async (req, res) => controller.delete(req, res));

export const userRoutes = router;
