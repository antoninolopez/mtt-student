import express from 'express';
import { userRoutes } from './routes/user.js';
import { authRoutes } from './routes/auth.js';
import { connectToDB } from './data/connection.js';
import jwt from 'express-jwt';

const startApp = (port, dbConnection) => {
  try {
    const app = express();

    app.use(express.json());

    app.use('/user', userRoutes);
    app.use('/auth', authRoutes);

    app.listen(port, () => {
      console.log(`Example app listening at http://localhost:${port}`);
    });
  } catch (error) {
    console.error(error.message);
    dbConnection.close();
  }
};

const port = 3000;
const dbConnection = connectToDB();

startApp(port, dbConnection);
