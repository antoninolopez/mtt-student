import { User } from '../data/user.js';

export class UserController {
  async getAll(req, res) {
    const users = await User.find({}).exec();
    res.json(users);
  }

  async getByID(req, res) {
    const userID = parseInt(req.params.userID);
    const user = await User.findOne({ id: userID }).exec();

    if (user) {
      res.json(user);
    } else {
      res.status(404).send('Unable to Find Resource');
    }
  }

  async create(req, res) {
    try {
      const user = new User(req.body);
      await user.save();
      res.status(201).json(user);
    } catch (err) {
      res.status(500).json({ error: err.message });
    }
  }

  async update(req, res) {
    const userID = parseInt(req.params.userID);
    const userData = req.body;
    if (userData) {
      try {
        const user = await User.findOneAndUpdate({ id: userID }, userData, {
          new: true,
        }).exec();
        res.json(user);
      } catch (err) {
        res.status(500).json({ error: err.message });
      }
    } else {
      res.status(400).json({ error: 'Must Provide User Data.' });
    }
  }

  async delete(req, res) {
    const userID = parseInt(req.params.userID);
    const user = await User.findOneAndDelete({ id: userID }).exec();

    if (user) {
      res.json(user);
    } else {
      res
        .status(404)
        .json({ success: success, message: 'Unable to Delete Resource' });
    }
  }
}
