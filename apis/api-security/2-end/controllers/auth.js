import jwt from 'jsonwebtoken';
import bcrypt from 'bcryptjs';
import { User } from '../data/user.js';

const SECRET_KEY = 'secretkey123';

export class AuthController {
  async register(req, res) {
    const { username, email, password } = req.body;
    const hashedPassword = bcrypt.hashSync(password);

    const exists = await User.findOne({ email }).exec();

    if (exists) {
      return res.status(409).json({
        message: 'User already exists',
      });
    }

    const newUser = new User({
      username: username,
      email: email,
      password: hashedPassword,
    });

    const user = await newUser.save();

    const expiresIn = 24 * 60 * 60; // one day in seconds

    const accessToken = jwt.sign({ id: user._id }, SECRET_KEY, {
      expiresIn: expiresIn,
    });

    res.status(200).send({
      user: { username: username, email: email },
      access_token: accessToken,
      expires_in: expiresIn,
    });
  }

  async login(req, res) {
    const { username, password } = req.body;

    const user = await User.findOne({ username }).exec();

    if (!user) return res.status(404).send('User not found!');

    const result = await bcrypt.compare(password, user.password);

    if (!result) return res.status(401).send('Password not valid!');

    const expiresIn = 24 * 60 * 60;

    const accessToken = jwt.sign({ id: user._id }, SECRET_KEY, {
      expiresIn: expiresIn,
    });

    res.status(200).send({
      user: { username: user.username, email: user.email },
      access_token: accessToken,
      expires_in: expiresIn,
    });
  }
}
