
import mongoose from 'mongoose';

const UserSchema = new mongoose.Schema(
  {
    fName: { type: String, required: true },
    lName: { type: String, required: true },
    username: { type: String, required: true },
  },
  {
    collection: 'Users',
    optimisticConcurrency: true
  }
);

export const User = mongoose.model('User', UserSchema);