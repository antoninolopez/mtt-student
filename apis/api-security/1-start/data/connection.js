import mongoose from 'mongoose';

export const connectToDB = () => {
  const dbConnectionURI = "mongodb://localhost:27017"
  mongoose.connect(dbConnectionURI);
  let dbConnection = mongoose.connection;

  dbConnection.on('error', (err) => console.log('Error connection to DB: ' + err));
  dbConnection.on('connected', () =>console.log(`Successfully Connected to DB: ${dbConnectionURI}`));

  return dbConnection;
};