
import { createNewUser, getAllUsers, getUserByID, deleteUser, updateUserByID } from '../user/user.js'

export class UserController {

    async getAll(req, res) {
        const users = getAll();
        res.json(users);
    }

    async getByID(req, res) {
        const userID = parseInt(req.params.userID);
        const user = getUserByID(userID);

        if (user) {
            res.json(user);
        } else {
            res.status(404).send("Unable to Find Resource");
        }
    }

    async create(req, res) {
        const user = createNewUser(req.body);
        res.status(201).json(user);
    }
}