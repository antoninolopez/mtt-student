import { Router } from "express";
import { UserController } from "../controllers/user.js";

const controller = new UserController();
const router = new Router();

router.get("/", async (req, res) => controller.getAll(req, res));
router.get("/:userID", async (req, res) => controller.getByID(req, res));
router.post("/", async (req, res) => controller.create(req, res));


export const userRoutes = router;