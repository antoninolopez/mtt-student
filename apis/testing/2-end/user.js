import fs from "fs";

export const readJSONFile = (targetPath) => {
  const userData = fs.readFileSync(targetPath, { encoding: "utf8" });
  return JSON.parse(userData);
}

export const writeJSONFile = (targetPath, targetJSON) => {
  fs.writeFileSync(targetPath, JSON.stringify(targetJSON, null, 2));
}

export const validateUser = (user) => {
  const idExists = 'id' in user;
  if (idExists) {
    return false;
  }
  const firstNameExists = 'firstName' in user;
  const lastNameExists = 'lastName' in user;
  const userNameExists = 'userName' in user;

  if (firstNameExists && lastNameExists && userNameExists) {
    return true;
  }
  return false;
   
}

export const getNextUserID = (userArr) => {
  const userIDs = userArr.map((user) => user.id);

  return Math.max(...userIDs) + 1;
}

export const getAllUsers = (targetPath) => {
  const parsedUserData = readJSONFile(targetPath);
  return parsedUserData.users;
}

export const getUserByID = (targetPath, targetID) => {
  const allUsers = readJSONFile(targetPath).users;
  const filteredUsers = allUsers.filter((user) => user.id === targetID);

  if (filteredUsers.length > 0) {
    return filteredUsers[0];
  } else {
    return null;
  }
}

export const createNewUser = (targetPath, newUserData) => {
  const allUsers = readJSONFile(targetPath).users;

  const nextUID = getNextUserID(allUsers);

  const newFullUser = {
    id: nextUID,
    ...newUserData,
  };

  allUsers.push(newFullUser);

  writeJSONFile(targetPath, { users: allUsers });

  return newFullUser;
}

export const deleteUser = (targetPath, targetID) => {
  const allUsers = readJSONFile(targetPath).users;

  const filteredUsers = allUsers.filter((user) => user.id !== targetID);

  if (filteredUsers.length === allUsers.length) {
    return false;
  } else {
    writeJSONFile(targetPath, { users: filteredUsers });
    return true;
  }
}