import { writeJSONFile, readJSONFile, createNewUser, deleteUser, getAllUsers, getNextUserID, getUserByID, validateUser } from "../user.js";
import path from "path";
import fs from "fs";

const __dirname = path.resolve();
const mockDataPath = path.join(__dirname, "tests", "mockData.json");

const createMockJson = () => {

    const mockUser = {
        "id": 1,
        "firstName": "John",
        "lastName": "Doe",
        "userName": "john.doe"
    };

  const mockJson = { users: [mockUser] }
  fs.writeFileSync(mockDataPath, JSON.stringify(mockJson, null, 2));
}

const destroyMockJson = () => {

    if (fs.existsSync(mockDataPath)) {
        fs.unlinkSync(mockDataPath);
    }
}

describe("Testing user.js", () => {

    test("reading json file", () => {
        createMockJson();

        const mockJsonData = readJSONFile(mockDataPath);
        expect(mockJsonData).toBeDefined();
        expect(mockJsonData).toHaveProperty("users");

        destroyMockJson();
    })

    test("writing json file", () => {
        const mockJsonData = { users: [{ id: 1, firstName: "jane", lastName: "doe", userName: "jane.doe"}] };

        writeJSONFile(mockDataPath, mockJsonData);

        const fileExists = fs.existsSync(mockDataPath);
        expect(fileExists).toBe(true);
        expect(mockJsonData).toHaveProperty("users");

        destroyMockJson(mockDataPath);
    })

    test("creating new user", () => {
        createMockJson();

        const userData = { id: 2, firstName: "jane", lastName: "doe", userName: "jane.doe" };
        const user = createNewUser(mockDataPath, userData);

        expect(user).toBeDefined();
        expect(user).toHaveProperty("id");
        expect(user).toHaveProperty("firstName");
        expect(user).toHaveProperty("lastName");
        expect(user).toHaveProperty("userName");

        destroyMockJson();
    })

    test("deleting user", () => {
        createMockJson();

        const userIdToDelete = 1;
        const successfullyDeleted = deleteUser(mockDataPath, userIdToDelete);
        expect(successfullyDeleted).toBe(true);

        const users = getAllUsers(mockDataPath);
        expect(users).toHaveLength(0);

        destroyMockJson();
    })

    test("getting next user id", () => {
        const userArr = [{ id: 1, firstName: "john", lastName: "doe", userName: "john.doe" }];
        const nextUserId = getNextUserID(userArr);
        expect(nextUserId).toBe(2);
    })

    test("getting user by id", () => {
        createMockJson();

        const userId = 1;
        const user = getUserByID(mockDataPath, userId);
        expect(user).toBeDefined();
        expect(user).toHaveProperty("id");
        expect(user).toHaveProperty("firstName");
        expect(user).toHaveProperty("lastName");
        expect(user).toHaveProperty("userName");

        expect(user.id).toBe(1);

        destroyMockJson();
    })

    test("validating user", () => {
        const user = { firstName: "john", lastName: "doe", userName: "john.doe" };
        const validUser = validateUser(user);
        expect(validUser).toBe(true);

        const userNoFirstName = { lastName: "doe", userName: "john.doe" };
        const invalidUser = validateUser(userNoFirstName);
        expect(invalidUser).toBe(false);
        
        const userWithID = { id: 1, firstName: "john", lastName: "doe", userName: "john.doe" };
        const invalidUserWithID = validateUser(userWithID);
        expect(invalidUserWithID).toBe(false);
    })
});