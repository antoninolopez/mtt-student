import { app } from '../app.js';
import { readJSONFile, writeJSONFile } from '../user.js'
import path from 'path';
import supertest from 'supertest';

const __dirname = path.resolve();
const userDataPath = path.join(__dirname, "data", "users.json");
const userJSONData = readJSONFile(userDataPath);

afterEach((done) => {
  writeJSONFile(userDataPath, userJSONData);
  done();
})


describe('test app routes', () => {
    test('GET /users', async () => {
        await supertest(app).get("/user")
        .expect(200)
        .then(res => {
            expect(Array.isArray(res.body)).toBeTruthy();
            expect(res.body).toHaveLength(1);
            expect(res.body[0].firstName).toBe('John');
            expect(res.body[0].lastName).toBe('Doe');
            expect(res.body[0].userName).toBe('john.doe')
        })
    })

    test('GET /users/:id', async () => {
        await supertest(app).get("/user/1")
        .expect(200)
        .then(res => {
            expect(res.body.firstName).toBe('John');
            expect(res.body.lastName).toBe('Doe');
            expect(res.body.userName).toBe('john.doe')
        })
    })

    test('POST /users', async () => {
        await supertest(app).post("/user")
        .send({
            firstName: 'Jane',
            lastName: 'Doe',
            userName: 'jane.doe'
        })
        .expect(200)
        .then(res => {
            expect(res.body.firstName).toBe('Jane');
            expect(res.body.lastName).toBe('Doe');
            expect(res.body.userName).toBe('jane.doe')
        })
    })

    test('DELETE /users/:id', async () => {
        await supertest(app).delete("/user/1")
        .expect(200)
        .then(res => {
            console.log(res.body);
            expect(res.body.deleted).toBe(true);
        })
    })
})