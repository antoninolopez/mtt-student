import express from "express";
import { getAllUsers, getUserByID, createNewUser, deleteUser, validateUser } from "./user.js"
import path from "path";

const __dirname = path.resolve();
const targetPath = path.join(__dirname, ".", "data", "users.json");

export const app = express();

app.use(express.json());

app.get('/user', (req, res) => {
    const users = getAllUsers(targetPath);
    res.json(users);
});

app.get('/user/:userID', (req, res) => {
    const userID = parseInt(req.params.userID);
    const user = getUserByID(targetPath, userID);
    res.json(user);
});

app.post('/user', (req, res) => {
    const newUser = req.body;
    const userIsValid = validateUser(newUser);
    if (userIsValid) {
        const createdUser = createNewUser(targetPath, newUser);
        res.json(createdUser).status(201);
    } else {
        res.status(400).send('User is not valid');
    }
});

app.delete('/user/:userID', (req, res) => {
    const userID = parseInt(req.params.userID);
    const successfullyDeleted = deleteUser(targetPath, userID);
    if (successfullyDeleted) {
         res.json({ deleted: successfullyDeleted });
    } else {
        res.status(404).json({ error: "User not found" });
    }
});