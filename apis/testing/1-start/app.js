import express from "express";
import { getAllUsers, getUserByID, createNewUser, deleteUser, validateUser } from "./user.js"
import path from "path";

const __dirname = path.resolve();
const targetPath = path.join(__dirname, ".", "data", "users.json");

export const app = express();

app.use(express.json());

