import { writeJSONFile, readJSONFile, createNewUser, deleteUser, getAllUsers, getNextUserID, getUserByID, validateUser } from "../user.js";
import path from "path";
import fs from "fs";

const __dirname = path.resolve();
const mockDataPath = path.join(__dirname, "tests", "mockData.json");

const createMockJson = () => {

    const mockUser = {
        "id": 1,
        "firstName": "John",
        "lastName": "Doe",
        "userName": "john.doe"
    };

  const mockJson = { users: [mockUser] }
  fs.writeFileSync(mockDataPath, JSON.stringify(mockJson, null, 2));
}

const destroyMockJson = () => {

    if (fs.existsSync(mockDataPath)) {
        fs.unlinkSync(mockDataPath);
    }
}