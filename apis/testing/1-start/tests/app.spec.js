import { app } from '../app.js';
import { readJSONFile, writeJSONFile } from '../user.js'
import path from 'path';
import supertest from 'supertest';

const __dirname = path.resolve();
const userDataPath = path.join(__dirname, "data", "users.json");
const userJSONData = readJSONFile(userDataPath);

afterEach((done) => {
  writeJSONFile(userDataPath, userJSONData);
  done();
})