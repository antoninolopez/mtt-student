const { ApolloServer, gql } = require('apollo-server');

const typeDefs = gql``;

const books = [
	{
		title: 'The Awakening',
		author: 'Kate Chopin',
	},
	{
		title: 'City of Glass',
		author: 'Paul Auster',
	},
];

const resolvers = {};

const server = new ApolloServer({ typeDefs, resolvers });

server.listen(3000).then(({ url }) => {
	console.log(`🚀  Server ready at ${url}`);
});
