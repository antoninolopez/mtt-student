const express = require("express");
const { getAllUsers, getUserByID, createNewUser } = require("./user/user");

const app = express();
const port = 3000;

app.use(express.json());

app.get("/", (req, res) => {
  res.send("Hello World!");
});

app.get("/users", (req, res) => {
  const users = getAllUsers();
  res.json(users);
});

app.post("/users", (req, res) => {
  const newUser = createNewUser(req.body);
  res.status(201).json(newUser);
});

app.get("/users/:userID", (req, res) => {
  const userID = parseInt(req.params.userID);

  const user = getUserByID(userID);

  if (user) {
    res.json(user);
  } else {
    res.status(404).send("Unable to Find Resource");
  }
});

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`);
});
