import fs from "fs";
import path from "path";

const __dirname = path.resolve();
const targetPath = path.join(__dirname, "data", "userData.json");

export function readJSONFile() {
  const userData = fs.readFileSync(targetPath, { encoding: "utf8" });

  return JSON.parse(userData);
}

export function writeJSONFile(targetJSON) {
  fs.writeFileSync(targetPath, JSON.stringify(targetJSON, null, 2));
}

export function getNextUserID(userArr) {
  const userIDs = userArr.map((user) => user.id);

  return Math.max(...userIDs) + 1;
}

export function getAllUsers() {
  const parsedUserData = readJSONFile();
  return parsedUserData.users;
}

export function getUserByID(targetID) {
  const allUsers = readJSONFile().users;

  const filteredUsers = allUsers.filter((user) => user.id === targetID);

  if (filteredUsers.length > 0) {
    return filteredUsers[0];
  } else {
    return null;
  }
}

export function createNewUser(newUserData) {
  const allUsers = readJSONFile().users;

  const nextUID = getNextUserID(allUsers);

  const newFullUser = {
    id: nextUID,
    ...newUserData,
  };

  allUsers.push(newFullUser);

  writeJSONFile({ users: allUsers });

  return newFullUser;
}

export function updateUserByID(targetID, update) {
  const allUsers = readJSONFile().users;

  const targetIndex = allUsers.map((user) => user.id).indexOf(targetID);

  if (targetIndex === -1) {
    return false;
  }

  const updatedUser = {
    ...allUsers[targetIndex],
    ...update,
  };

  allUsers.splice(targetIndex, 1, { ...updatedUser });

  writeJSONFile({ users: allUsers });
  return updatedUser;
}

export function deleteUser(targetID) {
  const allUsers = readJSONFile().users;

  const filteredUsers = allUsers.filter((user) => user.id !== targetID);

  if (filteredUsers.length === allUsers.length) {
    return false;
  } else {
    writeJSONFile({ users: filteredUsers });
    return true;
  }
}

