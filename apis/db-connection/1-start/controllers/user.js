
import { createNewUser, getAllUsers, getUserByID, deleteUser, updateUserByID } from '../helpers/user.js'

export class UserController {

    async getAll(req, res) {
        const users = getAllUsers();
        res.json(users);
    }

    async getByID(req, res) {
        const userID = parseInt(req.params.userID);
        const user = getUserByID(userID);

        if (user) {
            res.json(user);
        } else {
            res.status(404).send("Unable to Find Resource");
        }
    }

    async create(req, res) {
        const user = createNewUser(req.body);
        res.status(201).json(user);
    }

    async update(req, res) {
        const userID = parseInt(req.params.userID);
        const user = updateUserByID(userID, req.body);

        if (user) {
            res.json(user);
        } else {
            res.status(404).send("Unable to Find Resource");
        }
    }

    async delete(req, res) {
        const userID = parseInt(req.params.userID);
        const success = deleteUser(userID);

        if (success) {
            res.json({ success: success });
        } else {
            res.status(404).json({ success: success, message: "Unable to Find Resource" });
        }
    }
}