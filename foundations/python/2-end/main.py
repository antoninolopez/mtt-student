from random import randint
from lib.UserInput import get_numeric_input, get_range_input
from lib.CharacterMap import convert_to_char

def get_random_number_between_vals(val1, val2):
    return randint(val1, val2)

def get_random_number_between_multiple_ranges(range_choices):
    list_of_ranges = list()
    if 'a' in range_choices.lower():
        return randint(33,126)
    if 'u' in range_choices.lower():
        list_of_ranges.append(randint(65,90))
    if 'l' in range_choices.lower():
        list_of_ranges.append(randint(97,122))
    if 'n' in range_choices.lower():
        list_of_ranges.append(randint(49,57))
    if 's' in range_choices.lower():
        list_of_symbols = list()
        list_of_symbols.append(randint(33,47))
        list_of_symbols.append(randint(58,64))
        list_of_symbols.append(randint(91,96))
        list_of_symbols.append(randint(123,126))
        list_of_ranges.append(list_of_symbols[randint(0,len(list_of_symbols)-1)])
    return list_of_ranges[randint(0,len(list_of_ranges)-1)]
        
password_length = get_numeric_input("Please enter length of password (0-100): ")

# uncomment below for choice of ranges
# chars_to_use = get_range_input("Enter any combination of u,l,n,s,a for uppercase, lowercase, numbers, special characters, or all: ")

password_list = [None] * password_length

for i, item in enumerate(password_list):
    random_number = get_random_number_between_vals(33,126) # for random with range of all
    # random_number = get_random_number_between_multiple_ranges(chars_to_use) # for random with choice of range
    password_list[i] = convert_to_char(random_number)

password = "".join(password_list)
print(password)