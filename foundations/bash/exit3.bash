#!/bin/bash

HOST="googler.com"

ping -c 1 $HOST || echo "$HOST is unreachable"