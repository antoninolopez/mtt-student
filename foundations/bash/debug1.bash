#!/bin/bash 

MY_SHELL="sh"

if [ "$MY_SHELL" = "bash" ]
then
    echo "You might like learning Bash Shell Scripting"
elif [ "$MY_SHELL" = "zsh" ]
then
    echo "Why do you prefer the zsh shell?"
else
    echo "Maybe I can change your mind on Bash"
fi