#!/bin/bash

my_function() {
    echo "$GLOBAL_VAR - It's important where the VAR is defined"
}

# The value of GLOBAL_VAR is available to my_function since GLOBAL_VAR was defined before my_function was called.

GLOBAL_VAR=1

my_function


