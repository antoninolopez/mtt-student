#!/bin/bash

# Functions can accept parameters.
# the first parameter can be stored in $1
# the second parament can be stored in $2

function hello() {
    echo "Hello $1"
}

hello Andrew 
