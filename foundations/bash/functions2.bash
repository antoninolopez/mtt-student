#!/bin/bash

function hello() {
    echo "Hello Friend!"
    now
}

function now() {
    echo "It's $(date +%r)"
}

hello
