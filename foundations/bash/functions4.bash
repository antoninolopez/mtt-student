#!/bin/bash

# this script will loop through the each of the parameters 
# for the function 

# multiple parameters can be stored in $@

function hello() {
    for NAME in $@
    do
        echo "Hello $NAME"
    done
}

hello Andrew Tucker Justin Christine Dan Trenton John Aaron Thien

